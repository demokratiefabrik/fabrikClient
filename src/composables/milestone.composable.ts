/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import { computed, watch } from 'vue';
import {
  IMilestone,
  IMilestoneComposable,
  IMilestoneLibrary,
} from 'src/models/stage';
import { useStore } from 'vuex';
import useStoreService from 'src/services/store.service';
import { getStageService } from 'src/services';
// import getStageService from './stage.composable';

export function useMilestoneLibrary(): IMilestoneLibrary {
  const store = useStore();

  const stageMilestones = computed(
    (): string[] | undefined => store.getters['assemblystore/stageMilestones']
  );

  const isMilestoneSet = (key: string): boolean => {
    if (stageMilestones.value) {
      if (stageMilestones.value.includes(key)) {
        return true;
      }
    }
    return false;
  };

  return {
    isMilestoneSet,
  };
}

export function useMilestoneComposable(
  milestone: IMilestone
): IMilestoneComposable {
  const store = useStore();
  const { stageID } = useStoreService();
  const { isRoutedStageAlerted, markUnAlert } = getStageService();
  const { isMilestoneSet } = useMilestoneLibrary();

  // console.log(
  //   'DEBUG: loading',
  //   `LOADING ${milestone.loading} - ${milestone.loading?.value}`
  // );

  /** Returns null, when data are still loading */
  const priorsAchieved = computed((): boolean => {
    if (!milestone) {
      throw 'no milestone configured';
    }
    if (!milestone.priors?.length) {
      // console.log(!!milestone.loading?.value, 'DEBUGGGG');
      return true;
    }

    const notAchievedPrior = milestone.priors.find(
      (priorMilestone) => !priorMilestone.achieved?.value
    );

    return !notAchievedPrior;
  });

  // Enforce marking a Milestone as achieved (ignoring any achievment conditions...)
  const markAchieved = () => {
    // TODO: code-review: remove milestone inside function
    if (!isMilestoneSet(milestone.key)) {
      // first time achievement!
      // console.log('markAchieved ', milestone.key);
      store.dispatch('assemblystore/addMilestone', {
        key: milestone.key,
        stageID: stageID.value,
      });
    }

    // if (milestone.onAchieved) {
    //   milestone.onAchieved();
    // }
  };

  // Are all prior milestones achieved / or skipped?
  const priorsLoading = computed((): boolean => {
    if (!milestone.priors) {
      return false;
    }
    // is there any prior milestone, which is still loading.
    const prior = milestone.priors.find(
      (priorMilestone) => priorMilestone.loading?.value
    );
    return !!prior;
  });

  // does this milestone meet/or has once met the achievment conditions
  const achieved = computed((): boolean => {
    // Skipped milestones are always achieved...
    // if priors are not achieved, this is also not achieved...
    if (!priorsAchieved.value) {
      return false;
    }

    if (milestone.loading?.value) {
      return false;
    }
    if (milestone.skipped?.value) {
      return true;
    }

    if (isMilestoneSet(milestone.key)) {
      // console.log('MILESTONE SET true', milestone.key);
      return true;
    }

    // NEW: reveal allmilestones, when stage is alerted...
    // console.log(
    //   'isRoutedStageAlerted',
    //   isRoutedStageAlerted,
    //   isRoutedStageAlerted.value
    // );
    if (!isRoutedStageAlerted.value) {
      return true;
    }

    return !!milestone.achieved?.value;
  });

  const loading = computed((): boolean => {
    // if priors are not loaded, this is also not loaded...
    if (priorsLoading.value) {
      return true;
    }

    // console.log(
    //   'DEBUG: loading2',
    //   `LOADING ${milestone.loading} - ${milestone.loading?.value}`
    // );

    if (!milestone.loading) {
      return false;
    }

    return milestone.loading.value;
  });

  const skipped = computed((): boolean | undefined => {
    if (milestone.loading?.value) {
      return undefined;
    }

    if (!milestone.skipped) {
      // SKipped is not defined.
      return false;
    }

    return milestone.skipped?.value;
  });

  const visible = computed((): boolean => {
    // make sure all data are loaded
    if (loading.value || skipped.value) {
      return false;
    }

    // show content when all priors are achieved
    // console.log(key, priorsAchieved.value, 'DEBUGGGG');
    return priorsAchieved.value === true;
  });

  const hidden = computed((): boolean => {
    // make sure all data are loaded
    if (loading.value || skipped.value) {
      return true;
    }

    // show skeletong when all priors are achieved
    return !priorsAchieved.value;
  });

  watch(
    () => achieved.value,
    () => {
      if (achieved.value) {
        markAchieved();

        // LAST MILESTONE IN STAGE: (close stage, if achieved...)
        if (milestone.final) {
          if (isRoutedStageAlerted.value) markUnAlert();
        }
      }
    },
    { immediate: true }
  );

  if (milestone.initialize) {
    watch(
      () => priorsAchieved.value,
      (value) => {
        // console.log('ddddddd', value);
        if (value && milestone.initialize) {
          milestone.initialize();
        }
      },
      { immediate: true }
    );
  }

  return {
    markAchieved,
    key: milestone?.key,
    loading,
    hidden,
    visible,
    achieved,
    skipped,
  };
}
