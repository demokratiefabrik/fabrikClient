/** DEMOKRATIFABRIK: components for using qtree-discussions
This file contains all method that are specific to q-tree. 
i.e.  filtering and management of expanded content .
*/

import { scroll } from 'quasar';
const { getVerticalScrollPosition, setVerticalScrollPosition } = scroll;

import { ref, reactive, computed, watch } from 'vue';
import { useStore } from 'vuex';
import library from 'src/utils/library';
import constants from 'src/utils/constants';
// import getAppService from './app.service';
import { getAppService } from 'src/services';

import { INodeTuple } from 'src/models/content';
import useContentComposable from './content.composable';
import { IContentTreeTuple, ITreeNodeTuple } from 'src/models/contenttree';

export interface ITreeFilter {
  text: string | null;
  focus: INodeTuple | null;
  own: boolean | null;
  new: boolean | null;
  unreviewed: boolean | null; // used for manager review
}

const treeFilter = reactive<ITreeFilter>({
  text: '',
  own: null,
  new: null,
  unreviewed: null,
  focus: null,
});

const { getOffsetTop, pushSorted } = library;

const composeTreeNode = (
  id: number | null,
  rootNode: INodeTuple | null = null,
  flatEntries
): ITreeNodeTuple => {
  if (!flatEntries.value) {
    throw 'flatEntries could not be loaded..';
  }

  const entry = {
    id,
    key: `${id}`,
    label: `${id}`,
    progression: null,
    content: null,
    creator: null,
    children: [],
    level: null,
    path: undefined,
    nof_children: 0,
    nof_children_unread: 0,
    nof_children_unrated: 0,
    nof_children_alerted: 0,
    nof_descendants: 0,
    nof_descendants_unread: 0,
    nof_descendants_unrated: 0,
    nof_descendants_alerted: 0,
  } as ITreeNodeTuple;

  if (rootNode) {
    if (!rootNode.path) {
      // console.log(rootNode, 'rootNode');
      // throw 'no path defined for this entry!!';
      // Happens for instance, when one opens a unasigned peerreview...
      rootNode.path = rootNode.content?.id ? [rootNode.content?.id] : [];
    }
    // Notify descendants about state of every single node
    entry.progression = rootNode.progression ?? null;
    entry.content = rootNode.content;
    entry.level = rootNode.path.length - rootNode.path?.length;
    entry.path = rootNode.path;
    entry.creator = rootNode.creator;
  }

  return entry;
};

// SELECT X elements that are expanded initially..
const calculate_default_expanded_branches = (
  node: ITreeNodeTuple,
  QTREE_NUMBER_OF_INITIALLY_EXPANDED_NODES: number | null = null
): string[] => {
  if (!QTREE_NUMBER_OF_INITIALLY_EXPANDED_NODES) {
    QTREE_NUMBER_OF_INITIALLY_EXPANDED_NODES =
      constants.QTREE_NUMBER_OF_INITIALLY_EXPANDED_NODES;
  }

  if (!node?.content?.id) {
    throw 'invlalid TreeNodeTuple transmitted (1)...';
  }

  if (!node.children) {
    throw 'invlalid TreeNodeTuple transmitted (2)...';
  }

  const id: number | null = node.content.id ? node.content.id : node.id;

  const selected: string[] = id ? [`${id}`] : [];
  if (!node?.nof_descendants) {
    return selected;
  }

  let sample = node.children;
  function weighted_random(items, weights) {
    let i: number | null;
    for (i = 0; i < weights.length; i++) {
      weights[i] += weights[i - 1] || 0;
    }
    const random: number = Math.random() * weights[weights.length - 1];
    for (i = 0; i < weights.length; i++) {
      if (weights[i] > random) {
        break;
      }
    }
    return items[i];
  }
  while (
    selected.length <= QTREE_NUMBER_OF_INITIALLY_EXPANDED_NODES &&
    sample.length > 0
  ) {
    // add newSelected to selected / remove newSelected from sample / add their children to sample
    const weights = sample.map((child) => child.nof_descendants + 1);
    const newSelected = weighted_random(sample, weights);
    selected.push(`${newSelected.id}`);
    sample = sample.filter((child) => child.id !== newSelected.id);
    sample = sample.concat(newSelected.children);
  }
  return selected;
};

export default function useQtreeComposable(
  contenttreeTuple: IContentTreeTuple,
  alternativeRootNode: null | INodeTuple = null,
  filterTypes: null | string[] = null,
  doNotExpandNodesAtInitialization = false,
  accordion = false
) {
  // console.log('DEBUG: useQtreeComposable::SETUP');
  const store = useStore();

  // console.log('TODO: do not load useContenttree here.');
  const { markRead, isRead, isRated, isAlerted } = useContentComposable();
  const { headerOffset, scrollToAnchor } = getAppService();
  const tempBufferForJustReadContent = ref<any[]>([]);
  const animationDuration = ref<number>(150);
  const expanded = ref<string[] | null>(null);
  const selectedNodeTuple = ref<ITreeNodeTuple | null>(null);

  /* Returns null, if the full contenttree should be shown. 
  else, it returns content_id of the root content*/
  const rootId = computed((): number | null => {
    // AlternativeRootNode is the Root Entry!!
    if (alternativeRootNode?.content?.id) {
      return alternativeRootNode.content.id;
    }

    const identifierEls = contenttreeTuple.identifier.split('-');
    if (identifierEls.length > 1) {
      return parseInt(identifierEls[1]);
    } else {
      return null;
    }
  });

  const rootParentId = computed((): number | null => {
    if (alternativeRootNode?.content?.parent_id) {
      return alternativeRootNode.content.parent_id;
    }

    return null;
  });

  // COLLECT ALL Ids to include into this discussion tree...
  const entrieIds = computed((): number[] | null => {
    if (!contenttreeTuple?.entries) {
      console.error(contenttreeTuple);
      throw 'no entries within the contenttree';
    }

    // FILTER ENTRIES
    const ids = [] as number[];
    const paths = [] as number[][];
    const filteredBranches = [] as number[];
    for (const [id, tupleOrg] of Object.entries(contenttreeTuple.entries)) {
      const tuple = tupleOrg as INodeTuple;
      const withinPath =
        !alternativeRootNode ||
        !rootId.value ||
        (tuple.path &&
          tuple.path.includes(rootId.value) &&
          rootId.value !== tuple.content?.id);
      // console.log('withinPath', withinPath, rootId.value, tuple.path);
      const type = tuple.content?.type;
      const withinTypeFilter =
        !filterTypes || (type && filterTypes.includes(type));
      if (!withinTypeFilter && withinPath) {
        filteredBranches.push(parseInt(id));
      } else if (withinTypeFilter && withinPath) {
        if (tuple.path) {
          paths.push(tuple.path);
        }
      }
    }
    // remove all descendants of filtered elements...
    paths.map((path) => {
      if (path && !path.find((el) => filteredBranches.includes(el))) {
        const lastEl = path[path.length - 1] as number;
        ids.push(lastEl);
      }
    });

    return ids;
  });

  // collect all tree entries to include into this dicussion tree..
  const flatEntries = computed((): Record<string, INodeTuple> | null => {
    // console.log('DEBUG -  rerun TREE');

    if (!contenttreeTuple || !entrieIds.value) {
      return null;
    }

    if (filterTypes?.length || alternativeRootNode) {
      // fetch all content entries
      const entries = Object.keys(contenttreeTuple.entries)
        .filter((key) => entrieIds.value?.includes(parseInt(key)))
        .reduce((obj, key) => {
          obj[key] = contenttreeTuple?.entries[key];
          return obj;
        }, {});
      return entries;
    } else {
      // NO FILTER and root object: so take all entries...
      return contenttreeTuple.entries;
    }
  });

  // Finalize Discussion TREE STRUCTURE

  const tree = computed((): ITreeNodeTuple => {
    if (!flatEntries.value) {
      throw 'flatEntries not loaded...';
    }
    // console.log('tree 1', new Date());

    const rootNode: INodeTuple | null = rootId.value
      ? contenttreeTuple.entries[rootId.value]
      : null;

    // console.error('rootId.value', rootId.value, rootNode);

    const structure: ITreeNodeTuple = composeTreeNode(
      contenttreeTuple.id,
      rootNode,
      flatEntries
    );

    // console.log(structure, 'START');

    // console.log('INIT TREE METHOD');
    const entries: Record<string, ITreeNodeTuple> = {};

    // const rootLevel: number = alternativeRootNode?.path
    //   ? alternativeRootNode.path.length
    //   : 0;
    if (!flatEntries.value) {
      throw 'entries not loaded';
    }

    // console.log('tree 2', new Date());

    for (const [idStr, original2] of Object.entries(flatEntries.value)) {
      const id: number = parseInt(idStr);
      const original: INodeTuple = original2;
      if (!original.content) {
        throw 'invalid contentn node';
      }

      // console.log(id, rootId.value);
      if (id === alternativeRootNode?.id) {
        continue;
      }

      if (!original.path || !original.path.length) {
        // SKIP: can have temprarily an empty path
        // console.error('DEBUG: HMMM??');
        continue;
      }

      let entry: ITreeNodeTuple = entries[id];

      // is has been only provisorically inserted: the real content is comming right now
      if (entry && !entry.content) {
        entry = { ...entry, ...original };
        entries[id] = entry;
      }

      // provisorical insert
      if (!entry) {
        entry = composeTreeNode(id, original, flatEntries);
        entries[id] = entry;
      }

      original.path.forEach((descendant_id) => {
        if (descendant_id === id) {
          return;
        }
        if (!entries[descendant_id]) {
          entries[descendant_id] = composeTreeNode(
            descendant_id,
            null,
            flatEntries
          );
        }
        const descendant = entries[descendant_id];
        descendant.nof_descendants++;
        descendant.nof_descendants_unread += !isRead(entry) ? 1 : 0;
        descendant.nof_descendants_unrated += !isRated(entry) ? 1 : 0;
        descendant.nof_descendants_alerted += isAlerted(entry) ? 1 : 0;
      });

      // Notify root
      structure.nof_descendants++;
      structure.nof_descendants_unread += !isRead(entry) ? 1 : 0;
      structure.nof_descendants_unrated += !isRated(entry) ? 1 : 0;
      structure.nof_descendants_alerted += isAlerted(entry) ? 1 : 0;

      // Append children to parent
      // console.log('dddd', original.content.parent_id);
      if (original.content.parent_id) {
        let parentNode: null | ITreeNodeTuple = null;
        if (original.content.parent_id == structure.content?.id) {
          parentNode = structure;
        } else {
          parentNode = entries[original.content.parent_id];
        }

        if (parentNode) {
          pushSorted(parentNode.children, entry);
          parentNode.nof_children++;
          parentNode.nof_children_unread += !isRead(entry) ? 1 : 0;
          parentNode.nof_children_unrated += !isRated(entry) ? 1 : 0;
          parentNode.nof_children_alerted += isAlerted(entry) ? 1 : 0;
          // console.log('dddd', parentNode);
        }
      }

      // Attach the root element to the structure dict...
      if (
        rootId.value === original.content.id ||
        rootParentId.value === original.content.parent_id
      ) {
        // structure.children.push(entry);
        pushSorted(structure.children, entry);
        structure.nof_children++;
        structure.nof_children_unread += !isRead(entry) ? 1 : 0;
        structure.nof_children_unrated += !isRated(entry) ? 1 : 0;
        structure.nof_children_alerted += isAlerted(entry) ? 1 : 0;
      }
    }

    // console.log('tree 3', new Date());

    // console.log(structure, 'structure');
    return structure;
  });
  // -----------------------

  const highlightedNodeID = ref<number | null>(null);
  const expanded_filter = ref<any>(false);
  const public_profile = computed(() => store.getters['profilestore/profile']);
  const IsManager = computed(() => store.getters['assemblystore/IsManager']);
  const getExpandedBranches =
    store.getters['contenttreestore/getExpandedBranches'];

  /*
    Which <label> should be displayed on top of the ContentTree?
    SHould the Tree be displayed in <dense> layout?
    SHould the whole ContentTree be displayed or only a specific <childrenNodes>?

    <filterTypes>: You may additionaly limit the nodetypes to insert:...
    If nothing is indicated: every type is allowed, that is allowed for the given parent. */

  const total_nof_contents = () => {
    return tree.value.nof_descendants;
  };

  const is_currently_expanded = (node: INodeTuple | number): boolean | null => {
    if (typeof node === 'number') {
      if (expanded.value && node) {
        return expanded.value.includes(`${node}`);
      }
    } else {
      if (expanded.value && node.id) {
        return expanded.value.includes(`${node.id}`);
      }
    }

    return null;
  };

  const toggle_node = (node: ITreeNodeTuple): void => {
    if (!node.id) {
      return undefined;
    }
    if (expanded.value?.includes(`${node.id}`)) {
      collapse_node(node);
    } else {
      tempBufferForJustReadContent.value.push(node.id);
      expand_node(node);
    }
  };

  const collapse_node = (node): void => {
    if (!expanded.value) {
      return undefined;
    }
    expanded.value = expanded.value?.filter((x) => x != `${node.id}`);
    highlightNode(node);
    updateExpanded();
  };

  /* expand node and a few of its children */
  const expand_node = (node: ITreeNodeTuple): void => {
    if (!expanded.value || !node.content) {
      return undefined;
    }
    const isTopBranch =
      node.content.parent_id === null || node.content.parent_id == node.id;
    if (accordion && isTopBranch) {
      const newlyExpanded = expand_node_accordion(node);
      markAsReadByIDs(newlyExpanded);
    } else {
      // console.log('DEBUG EXPAND THIS', node, node?.nof_descendants);
      const newlyExpanded = calculate_default_expanded_branches(node, 3);
      expanded.value = expanded.value.concat(newlyExpanded);
      updateExpanded();
      markAsReadByIDs(newlyExpanded);
    }
    // mark newly Expanded as Read...
    highlightNode(node.content);
  };

  /* expand root-branch  and a few of its children, while closing all other branches */
  const expand_node_accordion = (node: ITreeNodeTuple) => {
    if (!expanded.value || !node.content || !node.children) {
      return undefined;
    }
    // let originOffsetTop = null
    const originalScrollPosition: number = getVerticalScrollPosition(window);
    // let originalScrollPosition: number | null = null

    const rootIds = node.children.map((x) => x.content?.id);
    const anchor = `NODE${node.content.id}`;
    const dom = document.getElementsByName(anchor);
    const ele = dom?.item(0);
    const originOffsetTop = getOffsetTop(ele);

    // dont use animations for this stepp:
    const originalAnimationDuration: null | number = animationDuration.value;
    animationDuration.value = 0;

    // close all branches
    expanded.value = expanded.value.filter(
      (x) => !rootIds.includes(parseInt(x))
    );

    // console.log('DEBUG 2: EXPAND THIS', node);
    const newlyExpanded = calculate_default_expanded_branches(node, 3);
    expanded.value = expanded.value.concat(newlyExpanded);
    updateExpanded();

    // Scroll to newly opened element... (WHEN Root-Branch changes...)
    setTimeout(() => {
      const offsetDiff = getOffsetTop(ele) - originOffsetTop;
      if (offsetDiff < 0) {
        setVerticalScrollPosition(
          window,
          originalScrollPosition + offsetDiff,
          0
        );

        // reset animations :
        animationDuration.value = originalAnimationDuration;
      }
    }, 60 * 5);

    return newlyExpanded;
  };

  const highlightNode = (node) => {
    highlightedNodeID.value = node ? node.id : null;
  };

  const markAsReadByIDs = (ids) => {
    // console.log(ids);
    ids.forEach((nodeId) => {
      if (!nodeId) {
        return;
      }
      const entry = contenttreeTuple?.entries[parseInt(nodeId)];
      if (entry && !isRead(entry)) {
        // console.log('not read...');
        markRead(entry);
      }
    });
  };

  const expand_none = () => {
    // console.log('expand_none');
    expanded.value = [];
    highlightNode(null);
    updateExpanded();
  };

  const focus_on_branch = (node) => {
    scrollToAnchor(`NODE${node.content.id}`, headerOffset.value, 0);
    expanded_filter.value = false;
    expand_node(node);
    console.assert(node.content.id);
  };

  /* Method store list of expanded array in localstorage */
  const updateExpanded = () => {
    // make list unique
    expanded.value = [...new Set(expanded.value)];
    store.dispatch('contenttreestore/update_expanded_branches', {
      contenttreeIdentifier: contenttreeTuple?.identifier,
      contenttreeRootNodeID: tree.value.path?.length
        ? tree.value.path[0]
        : null,
      expanded: expanded.value,
    });
  };

  // FILTER TREE
  const treeFilterMethod = (node: INodeTuple): boolean => {
    if (treeFilter.focus) {
      // False, if at least one path element is not in the current node' path.
      // console.log(treeFilter.path, node.path)
      if (treeFilter.focus?.path?.find((id) => !node.path?.includes(id))) {
        return false;
      }
    }

    if (!expanded_filter.value) {
      return true;
    }
    if (treeFilter.own) {
      if (node.creator.id !== public_profile.value?.id) {
        return false;
      }
    }
    if (treeFilter.new) {
      const contentID = node?.content?.id;
      if (
        contentID &&
        node.progression?.read &&
        !tempBufferForJustReadContent.value.includes(contentID)
      ) {
        // if (node.progression?.read && node !== last_expanded_node) {
        return false;
      }
    }
    if (treeFilter.unreviewed) {
      if (
        node.content?.reviewed !== false ||
        !node?.content?.private_property
      ) {
        return false;
      }
    }
    if (treeFilter.text?.length) {
      let searchable = '';
      searchable += `${node?.content?.title} ${node?.content?.text} #${node.content?.id}  @${node?.creator.FN} `;
      if (
        searchable.toLowerCase().indexOf(treeFilter.text.toLowerCase()) == -1
      ) {
        return false;
      }
    }

    return true;
  };

  const resetFilter = () => {
    // console.log('reset filter')
    treeFilter.text = '';
    treeFilter.focus = null;
    treeFilter.own = false;
    treeFilter.new = false;
    treeFilter.unreviewed = false; // used for manager review+
  };

  const resetFilterFocus = () => {
    treeFilter.focus = null;
  };

  const selectNodeTuple = (nodeTuple: ITreeNodeTuple): void => {
    // console.log(nodeTuple);
    selectedNodeTuple.value = nodeTuple;
  };

  // INITIALIZE
  // ---------------------
  watch(
    () => tree.value,
    (newTree, oldTree) => {
      if (
        newTree?.content?.id &&
        newTree?.content?.id !== oldTree?.content?.id
      ) {
        resetFilter();

        // INITIALIZE EXPANDED VALUE
        // choose random entries that are expanded... (if not previously changed expanded state...)
        expanded_filter.value = IsManager.value;
        expanded.value = getExpandedBranches({
          contenttreeIdentifier: contenttreeTuple?.identifier,
          contenttreeRootNodeID: tree.value.path?.length
            ? tree.value.path[0]
            : null,
        });

        if (expanded.value === null || expanded.value === undefined) {
          console.assert(contenttreeTuple);
          // Nothing expaneded here: so expand random branchens
          if (doNotExpandNodesAtInitialization) {
            expanded.value = [];
          } else {
            expanded.value = calculate_default_expanded_branches(tree.value);
          }
          store.dispatch('contenttreestore/update_expanded_branches', {
            contenttreeIdentifier: contenttreeTuple?.identifier,
            contenttreeRootNodeID: tree.value.path?.length
              ? tree.value.path[0]
              : null,
            expanded: expanded.value,
          });

          // mark newly Expanded as Read...
          markAsReadByIDs(expanded.value);
        }
      }
    },
    { immediate: true }
  );

  return {
    expanded,
    treeFilter,
    highlightNode,
    highlightedNodeID,
    expanded_filter,
    animationDuration,
    updateExpanded,
    total_nof_contents,
    is_currently_expanded,
    toggle_node,
    treeFilterMethod,
    resetFilter,
    resetFilterFocus,
    expand_none,
    tree,
    flatEntries,
    selectedNodeTuple,
    selectNodeTuple,
    focus_on_branch,
  };
}
