/**
 * Library of content methods and stuff...
 */
import constants from 'src/utils/constants';
import { computed } from 'vue';
import { INode, INodeData, INodeTuple } from 'src/models/content';
// import getMonitorService from './monitor.composable';
import { IContentTreeTuple } from 'src/models/contenttree';
import api from 'src/utils/api';
import useStoreService from 'src/services/store.service';
import library from 'src/utils/library';
import { getMonitorService, getStore } from 'src/services';
const { loaded } = library;

interface ISaveResponse {
  isAModification: boolean; // modification<true> | new insert <false>
  reviewNeeded: boolean;
}

const getTopicID = (
  node: INode | INodeData,
  contenttreeTuple: IContentTreeTuple
): number | null => {
  console.assert(node);
  console.assert(contenttreeTuple.id);

  const contentID = node.id ? node.id : node.parent_id;
  if (contentID) {
    const path = contenttreeTuple?.entries[contentID]?.path;
    if (path?.length) {
      return path[0];
    }
  }
  return null;
};

const getChildrenTuples = (
  node: INodeTuple,
  contenttreeTuple: IContentTreeTuple
): INodeTuple[] | null => {
  console.assert(node);

  if (!node?.content) {
    throw 'node_id not defined';
  }
  if (!contenttreeTuple) {
    // return null;
    throw 'contenttreeTuple not defined';
  }
  return Object.values(contenttreeTuple?.entries).filter(
    (c) =>
      c.path?.includes(node?.content?.id as number) &&
      node?.content?.id !== (c?.content?.id as number)
  );
};

const saveSalience = (salience: number, nodeTupe: INodeTuple) => {
  // console.log('SET new Salience value');
  const store = getStore();

  const data = {
    contentID: nodeTupe.content?.id,
    salience,
    topicID: nodeTupe.path ? nodeTupe.path[0] : null,
  };

  const { monitorLog } = getMonitorService();
  monitorLog(constants.MONITOR_SET_SALIENCE, data);

  // immediatly update saliences in vuex store (=> allows to update the dynamically generated charts)
  store.dispatch('contenttreestore/update_salience', {
    contenttreeIdentifier: nodeTupe.content?.contenttree_identifier,
    contentID: nodeTupe.content?.id,
    topicID: nodeTupe.path?.length ? nodeTupe.path[0] : null,
    salience,
  });
};

const saveCertainity = (certainity: number, nodeTupe: INodeTuple) => {
  // console.log('SET new Certainity value');
  const store = getStore();
  const data = {
    contentID: nodeTupe.content?.id,
    certainity,
    topicID: nodeTupe.path ? nodeTupe.path[0] : null,
  };

  const { monitorLog } = getMonitorService();
  monitorLog(constants.MONITOR_SET_CERTAINITY, data);

  // immediatly update saliences in vuex store (=> allows to update the dynamically generated charts)
  // console.log(store, store.dispatch);
  store.dispatch('contenttreestore/update_certainity', {
    contenttreeIdentifier: nodeTupe.content?.contenttree_identifier,
    contentID: nodeTupe.content?.id,
    topicID: nodeTupe.path?.length ? nodeTupe.path[0] : null,
    certainity,
  });
};

const saveRating = (rating: number, nodeTupe: INodeTuple) => {
  if (rating === null || rating === undefined) {
    return;
  }
  // console.log('SET new Rating value');
  const store = getStore();
  const data = {
    contentID: nodeTupe.content?.id,
    rating,
    topicID: nodeTupe.path ? nodeTupe.path[0] : null,
  };

  const { monitorLog } = getMonitorService();
  monitorLog(constants.MONITOR_SET_RATING, data);

  // immediatly update saliences in vuex store (=> allows to update the dynamically generated charts)
  store.dispatch('contenttreestore/update_rating', {
    contenttreeIdentifier: nodeTupe.content?.contenttree_identifier,
    contentID: nodeTupe.content?.id,
    topicID: nodeTupe.path?.length ? nodeTupe.path[0] : null,
    rating,
  });
};

const saveReview = (reviewed: boolean, nodeTupel: INodeTuple) => {
  const store = getStore();

  const data = {
    contentID: nodeTupel.content?.id,
    reviewed,
    topicID: nodeTupel.path ? nodeTupel.path[0] : null,
  };

  const { monitorFire } = getMonitorService();
  monitorFire(constants.MONITOR_SET_CONTENT_REVIEWED, data);

  // immediatly update saliences in vuex store (=> allows to update the dynamically generated charts)
  store.dispatch('contenttreestore/update_review', {
    contenttreeIdentifier: nodeTupel.content?.contenttree_identifier,
    contentID: nodeTupel.content?.id,
    reviewed,

    // contenttreeIdentifier,
    // contentID,
    // reviewed,
  });
};

/* Get all context types that are allowed at this position.
  There are a) general contenttreeTuple restrictions (See Configuration.CONTENT_TYPES),
  There are b) parentType restrictions (See Configuration.ONTOLOGY),
  and there are c) context restrictions (See QTree.filterTypes-Prop)),
    */
const doesContentChangeRequiresReviewProcess = (
  contenttreeIdentifier: string,
  originalNode: INodeData | null,
  modifiedNode: INodeData | null = null
): boolean | null => {
  console.assert(modifiedNode);

  const store = getStore();
  const isCommonPropertyByConentType =
    store.getters['contenttreestore/isCommonPropertyByConentType'];
  const IsManager = computed(
    (): boolean => store.getters['assemblystore/IsManager']
  );

  if (!contenttreeIdentifier) {
    throw 'no contenttreeIdentifier is present';
  }

  if (IsManager.value) {
    // Manager cannot add proposal...
    return null;
  }

  // per default: no review necessary
  let propertyIsCommon = false;

  // For Modification: check old and new content type...(if at least one is commont property, return true)
  if (originalNode?.type) {
    propertyIsCommon = isCommonPropertyByConentType({
      contenttreeIdentifier,
      type: originalNode.type,
    });
  }

  if (modifiedNode?.type) {
    propertyIsCommon =
      propertyIsCommon ||
      isCommonPropertyByConentType({
        contenttreeIdentifier,
        type: modifiedNode.type,
      });
  }

  return propertyIsCommon;
};

const saveContent = async (
  originalnode: INodeData | null,
  node: INodeData,
  contenttreeTuple: IContentTreeTuple
): Promise<ISaveResponse | undefined> => {
  const store = getStore();
  console.assert(!!store);

  const { assemblyIdentifier } = useStoreService();

  // // TODO: get rid of contenttreeTuple paramenter and read topic entry from vuex..?
  const contentreeId = contenttreeTuple?.id;
  if (!contentreeId) {
    return Promise.reject('Contenttree could not be found');
  }
  // Set topic as dicussed
  const topicID = getTopicID(node, contenttreeTuple);
  if (topicID) {
    markDiscussed(contenttreeTuple?.entries[topicID]);
  }
  let submitApiFunction = null as
    | null
    | ((
        assemblyIdentifier: string | null,
        contenttreeIdentifier: number,
        localmodel: INode | INodeData | null
      ) => Promise<any>);
  const reviewNeededClientSide = doesContentChangeRequiresReviewProcess(
    contenttreeTuple.identifier,
    originalnode,
    node
  );
  if (reviewNeededClientSide) {
    submitApiFunction = api.proposeContent;
  } else {
    submitApiFunction = api.saveContent;
  }

  const isAModification = !!node.id;

  // Send to API-Server
  return submitApiFunction(assemblyIdentifier.value, contentreeId, node).then(
    (response) => {
      // console.log('DEBUG: Content send to Server', response.data);

      // Check Response
      if (response.data.OK) {
        const reviewNeeded = 'peerreview' in response.data;
        if (!!reviewNeededClientSide !== !!reviewNeeded) {
          Promise.reject(
            `Something was wrong with reviewNeeded flag. Server's opinion differs to the client side's server: ${reviewNeeded} client: ${reviewNeededClientSide}`
          );
        }

        // TODO: merge with updateStore!!!

        // Update Vuex Store: CONTENT
        if ('content' in response.data) {
          // console.log('SEND TO VUEX: CONTENT, ', response.data.content);
          store.dispatch('contenttreestore/update_content', {
            contentTuple: response.data.content,
          });
        }

        // Update Vuex Store: COMMENT-CONTENT (store an additional comment content entry...)
        if ('comment' in response.data) {
          // console.log('SEND TO VUEX: COMMENT, ', response.data.comment);
          store.dispatch('contenttreestore/update_content', {
            contentTuple: response.data.comment,
          });
        }

        // Update Vuex Store: PEERREVIEW
        if ('peerreview' in response.data) {
          // console.log('SEND TO VUEX: PEERREVIEW, ', response.data.peerreview);
          store.dispatch('peerreviewstore/updatePeerreviewTuple', {
            peerreviewTuple: response.data.peerreview,
          });
        }

        // Update Vuex Store: NOTIFICATIONS
        if ('notifications' in response.data) {
          store.dispatch('profilestore/update_notifications', {
            notifications: response.data.notifications,
          });
        }

        // if ('certificates' in response.data) {
        //   console.log('RETRIEVED CERTIFICATES', response.data.peerreviews);
        //   store.dispatch('profilestore/update_certificates', {
        //     certificates: response.data.certificates,
        //   });
        // }

        // Update Vuex Store: activity counters....
        if (reviewNeeded) {
          store.dispatch('assemblystore/incrementAssemblyActivityCounter', {
            assemblyIdentifier: assemblyIdentifier.value,
            counterName: 'number_of_proposals_today',
          });
        } else {
          store.dispatch('assemblystore/incrementAssemblyActivityCounter', {
            assemblyIdentifier: assemblyIdentifier.value,
            counterName: 'number_of_comments_today',
          });
        }

        // Response
        const returnValue: ISaveResponse = {
          isAModification,
          reviewNeeded,
        };

        // console.log('DEBUG: submitApiFunction  => Promise.resolve (334)');

        return Promise.resolve(returnValue);
      }

      Promise.reject('Invalid data received from api server');
    }
  );
};

const isRead = (nodeTuple: INodeTuple): boolean => {
  // console.log('isRead ', nodeTuple, !!nodeTuple?.progression?.read);
  return !!nodeTuple?.progression?.read;
};

const isAlerted = (nodeTuple: INodeTuple): boolean => {
  return !!nodeTuple?.progression?.alerted;
};

const isSalienced = (nodeTuple: INodeTuple): boolean => {
  return loaded(nodeTuple.progression?.salience);
};

const isCertainitied = (nodeTuple: INodeTuple): boolean => {
  return loaded(nodeTuple.progression?.certainity);
};

const isRated = (nodeTuple: INodeTuple): boolean => {
  return loaded(nodeTuple.progression?.rating);
};

const markRead = (nodeTuple: INodeTuple): void => {
  if (!nodeTuple.content?.id) {
    throw 'Node or node.id is missing';
  }
  // const store = useStore();
  const store = getStore();

  const data = {
    contentID: nodeTuple.content.id,
  };

  const { monitorLog } = getMonitorService();
  monitorLog(constants.MONITOR_SET_CONTENT_READ, data);

  const contenttreeIdentifier: string | null =
    nodeTuple.content.contenttree_identifier;

  // immediatly update vuex store
  store.dispatch('contenttreestore/update_read', {
    contenttreeIdentifier: contenttreeIdentifier,
    contentID: nodeTuple.content.id,
  });
};

const markDiscussed = (nodeTuple: INodeTuple): void => {
  if (!nodeTuple.content?.id) {
    throw 'no node or node id transmitted';
  }
  const store = getStore();
  const contenttreeIdentifier: string | null =
    nodeTuple.content.contenttree_identifier;
  console.assert(contenttreeIdentifier);

  const data = {
    contentID: nodeTuple.content.id,
  };

  const { monitorLog } = getMonitorService();
  monitorLog(constants.MONITOR_SET_CONTENT_DISCUSSED, data);

  store.dispatch('contenttreestore/update_discussed', {
    contenttreeIdentifier: contenttreeIdentifier,
    contentID: nodeTuple.content.id,
  });
};

const getLabel = (nodeTuple: INodeTuple): string => {
  const contentContent = nodeTuple?.content;
  if (contentContent) {
    return contentContent.title;
  }

  throw 'content is empty...';
};

export default function useContentComposable() {
  return {
    saveSalience,
    saveRating,
    saveReview,
    saveCertainity,
    saveContent,
    markDiscussed,
    doesContentChangeRequiresReviewProcess,
    markRead,
    getLabel,
    isRated,
    isAlerted,
    isSalienced,
    isCertainitied,
    isRead,
    getChildrenTuples,
  };
}
