/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import useEmitter from 'src/utils/emitter';
import library from 'src/utils/library';
import { computed, Ref, watch } from 'vue';
import { useStore } from 'vuex';
import { INodeTuple } from 'src/models/content';
import { IContentTreeTuple, ITreeNodeTuple } from 'src/models/contenttree';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
import useStoreService from 'src/services/store.service';

const { userid } = usePKCEComposable();
const emitter = useEmitter();
const { loaded, pushSorted } = library;

const { assemblyIdentifier } = useStoreService();

/** Filter nodes by type (positive list) */
const filter_entries = (nodes, TYPES): INodeTuple[] | null => {
  if (!nodes) {
    return null;
  }
  const filtered = nodes.filter((item) => TYPES.includes(item.content.type));
  return filtered;
};

export default function useContenttreeComposable(
  customContentTreeIdentifier: Ref<string | null> | undefined = undefined,
  addWatcher = false
) {
  const store = useStore();

  const contenttreeIdentifier = computed((): string | null => {
    if (customContentTreeIdentifier?.value) {
      return customContentTreeIdentifier.value;
    }
    return null;
  });

  const contenttreeTuple = computed((): IContentTreeTuple | null => {
    if (!contenttreeIdentifier.value) {
      return null;
    }
    const get_contenttree = store.getters['contenttreestore/get_contenttree'];
    const contenttree = get_contenttree({
      contenttreeIdentifier: contenttreeIdentifier.value,
    });
    return contenttree;
  });

  const contenttreeEntries = computed((): Record<string, INodeTuple> | null => {
    if (!contenttreeTuple.value) {
      return null;
    }
    return contenttreeTuple.value?.entries;
  });

  const contenttreeRootNodeTuple = computed((): ITreeNodeTuple => {
    return {
      id: null,
      path: [],
      key: 'null',
      content: null,
      level: 0,
      nof_children: 0,
      nof_children_unread: 0,
      nof_children_unrated: 0,
      nof_children_alerted: 0,
      nof_descendants: 0,
      nof_descendants_unread: 0,
      nof_descendants_unrated: 0,
      nof_descendants_alerted: 0,
      // children: [],
      children: contenttreeRootNodeTuples.value,
    };
  });

  const contenttreeRootNodeTuples = computed((): ITreeNodeTuple[] | null => {
    if (!contenttreeTuple.value || !contenttreeTuple.value?.rootElementIds) {
      return null;
    }

    let elements: INodeTuple[] = [];
    contenttreeTuple.value.rootElementIds.forEach((x: any) => {
      const el = contenttreeTuple.value?.entries[x];
      if (el) {
        elements = pushSorted(elements, el);
      } else {
        // console.log('WARNING: content element not found', x);
      }
    });

    return elements as ITreeNodeTuple[];
  });

  const ready = computed(() => {
    const ready =
      loaded(contenttreeTuple.value) &&
      loaded(contenttreeTuple.value?.rootElementIds);
    if (ready) {
      emitter.emit('hideLoading');
    }
    return ready;
  });

  const numberOfUnsaliencedTopLevelEntries = computed((): number | null => {
    if (contenttreeTuple.value == null) {
      return null;
    }
    const unsalienced_children = contenttreeRootNodeTuples.value?.filter(
      (x) => !loaded(x.progression?.salience)
    );
    if (unsalienced_children) {
      return Object.values(unsalienced_children).length;
    }

    return null;
  });

  const numberOfUnratedTopLevelEntries = computed((): number | null => {
    if (contenttreeTuple.value == null) {
      return null;
    }
    const unrated_children = contenttreeRootNodeTuples.value?.filter(
      (x) => !loaded(x.progression?.rating)
    );
    if (!unrated_children) {
      return null;
    }
    return Object.values(unrated_children).length;
  });

  const salienceCompleted = computed((): boolean | null => {
    if (!loaded(numberOfUnsaliencedTopLevelEntries.value)) {
      return null;
    }

    return numberOfUnsaliencedTopLevelEntries.value == 0;
  });

  if (addWatcher) {
    // console.log(
    //   'DEBUG::: ADD WATCHER FOR CONTENTTREE ',
    //   contenttreeIdentifier.value
    // );
    watch(
      () => contenttreeIdentifier.value,
      () => {
        if (contenttreeIdentifier.value) {
          store.dispatch('contenttreestore/syncContenttree', {
            assemblyIdentifier: assemblyIdentifier.value,
            contenttreeIdentifier: contenttreeIdentifier.value,
            oauthUserID: userid.value,
          });
        }
      },
      { immediate: true }
    );
  }

  const getDescendantsOf = (node): INodeTuple[] => {
    console.assert(node);
    const nodePathLength = node.path.length;
    const nodeID = node.content.id;
    const entries = contenttreeTuple.value?.entries;
    if (!entries) {
      throw 'Contenttree entries could not be loaded';
    }
    return Object.values(entries).filter(
      (x) =>
        x.path &&
        x.path.length > nodePathLength &&
        x.path[nodePathLength - 1] == nodeID
    );
  };
  return {
    getDescendantsOf,
    contenttreeTuple,
    contenttreeEntries,
    ready,
    filter_entries,
    contenttreeRootNodeTuple,
    numberOfUnratedTopLevelEntries,
    numberOfUnsaliencedTopLevelEntries,
    salienceCompleted,
  };
}
