/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import { computed, watch } from 'vue';
import library from 'src/utils/library';
import { IPeerreviewTuple } from 'src/models/peerreview';
import { useStore } from 'vuex';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
// import useAssemblyService from './assembly.composable';
import useStoreService from 'src/services/store.service';
import { getAssemblyService } from 'src/services';
const { loaded } = library;

const { userid } = usePKCEComposable();

const isAssigned = (peerreview: IPeerreviewTuple): boolean => {
  return !!peerreview.progression;
};

const arePeerreviewsCompleted = (peerreviews: IPeerreviewTuple[]): boolean => {
  // ALL PROPOSAL RESPONDED?
  return !peerreviews.find(
    (byContent: IPeerreviewTuple) =>
      byContent.progression &&
      !loaded(byContent.progression?.response) &&
      !byContent.peerreview?.rejected &&
      !byContent.peerreview?.approved
    // It is not possible to have multiple parallel ongoing peerreview for each topic.
    // Hence, check only the first in the list.
  );
};

const { assemblyIdentifier } = useStoreService();

export default function usePeerreviewComposable(
  contenttreeID: number | null,
  addWatcher = false
) {
  const store = useStore();
  const dummy = false;

  const {
    overallLimitForAddingProposalsReached,
    limitForAddingProposalsReached,
  } = getAssemblyService();

  const get_peerreview_by_contenttree_id =
    store.getters['peerreviewstore/get_peerreview_by_contenttree_id'];

  const getAllLocalPeerreviews = computed(
    (): undefined | IPeerreviewTuple[] => {
      if (!store.state.peerreviewstore.peerreviews) {
        return undefined;
      }

      let peerreviews: IPeerreviewTuple[] = [];
      Object.values(store.state.peerreviewstore.peerreviews).forEach(
        (container: any) => {
          peerreviews = [
            ...peerreviews,
            ...Object.values(container.entries),
          ] as any;
        }
      );
      return peerreviews;
    }
  );

  const contentsToPeerreview = computed((): undefined | IPeerreviewTuple[] => {
    const loadedPeerreviewContententtrees =
      store.getters['peerreviewstore/loadedPeerreviewContententtrees'];
    if (!store.state.peerreviewstore.peerreviews) {
      return undefined;
    }
    if (!loadedPeerreviewContententtrees.includes(`${contenttreeID}`)) {
      return undefined;
    }

    const tempPeerreviews = get_peerreview_by_contenttree_id({
      contenttreeID: contenttreeID,
    });

    if (!tempPeerreviews) {
      return undefined;
    }

    // FILTER: return only assigned peerreviews...
    const listPeerreviews: IPeerreviewTuple[] = Object.values(tempPeerreviews);
    const assignedPeerreviews = listPeerreviews.filter((x: IPeerreviewTuple) =>
      isAssigned(x)
    );

    return assignedPeerreviews;
  });

  if (addWatcher) {
    watch(
      () => contenttreeID,
      () => {
        if (contenttreeID) {
          // console.log('SYNC Peerreviews');
          store.dispatch('peerreviewstore/syncPeerreviews', {
            assemblyIdentifier: assemblyIdentifier.value,
            contenttreeID: contenttreeID,
            oauthUserID: userid.value,
          });
        }
      },
      { immediate: true }
    );
  }

  return {
    arePeerreviewsCompleted,
    contentsToPeerreview,
    dummy,
    overallLimitForAddingProposalsReached,
    limitForAddingProposalsReached,
    getAllLocalPeerreviews,
  };
}
