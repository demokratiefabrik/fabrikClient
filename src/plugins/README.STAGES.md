# Stage Type Plugins

These plugins handle...

- stage intelligence
- stage structure

The main idea is that all stage-plugins can be used by distinct assembly types.
