const meta4AssemblyDelegates = { assemblyAcl: ['delegate', 'observer'] };
const meta4AssemblyManagers = { assemblyAcl: ['manager'] };
const meta4AssemblyPages = { topmenu: 'assemblies_ongoing_list' };
import { RouteRecordRaw } from 'vue-router';
const meta4AssemblyAuthenticated = {
  assemblyAcl: ['delegate', 'observer', 'manager', 'expert'],
};

const routes: RouteRecordRaw[] = [
  {
    name: 'AssemblyHome_CIR',
    path: '/:assemblyIdentifier/:assemblyType/',
    component: () => import('/src/plugins/CIR/Layout.vue'),
    children: [
      {
        path: '',
        name: 'CIR',
        component: () => import('pages/Index.vue'),
      },
      {
        path: 'report',
        name: 'report__CIR',
        component: () => import('/src/plugins/CIR/pages/report/Report.vue'),
        meta: {
          hideAssemblyMenu: true,
          //   ...meta4AssemblyAuthenticated,
        },
      },
      {
        path: 'start',
        name: 'start__CIR',
        component: () => import('/src/pages/Assembly/TOC/TOC.vue'),
        meta: {
          hideAssemblyMenu: true,
          ...meta4AssemblyAuthenticated,
        },
      },
      {
        path: 'assembly_manage',
        name: 'assembly_manage__CIR',
        component: () => import('/src/pages/Assembly/AssemblyManage.vue'),
        meta: {
          hideAssemblyMenu: true,
          ...meta4AssemblyManagers,
        },
      },
      {
        // TODO: do we need contenttreeID in path?
        path: 'textsheet/:stageID', // '/:contenttreeID/',
        name: 'TEXTSHEET__CIR',
        component: () => import('/src/plugins/CIR/stages/TEXTSHEET/Index.vue'),
        meta: { ...meta4AssemblyDelegates },
      },
      {
        // TODO: do we need contenttreeid in path?
        path: 'textsheet/:stageID', // /:contenttreeID/',
        name: 'TEXTSHEET',
        component: () => import('/src/plugins/CIR/stages/TEXTSHEET/Index.vue'),
        meta: meta4AssemblyPages,
      },
      // {
      //   path: '/:assemblyIdentifier/textsheet/:stageID', // /:contenttreeID/',
      //   name: 'TEXTSHEET_DEFAULT_LAYOUT',
      //   component: () => import('/src/plugins/CIR/stages/TEXTSHEET/Index.vue'),
      //   meta: meta4AssemblyPages,
      // },
      {
        path: 'profile/:stageID',
        name: 'PROFILEUPDATE',
        component: () =>
          import('/src/plugins/CIR/stages/PROFILEUPDATE/Index.vue'),
      },
      {
        path: ':stageID/information',
        name: 'INFORMATION__CIR',
        component: () =>
          import('/src/plugins/CIR/stages/INFORMATION/Information.vue'),
        meta: meta4AssemblyDelegates,
      },
      {
        path: ':stageID/voice',
        name: 'VOICE__CIR',
        component: () => import('/src/plugins/CIR/stages/VOICE/Voice.vue'),
        meta: meta4AssemblyDelegates,
      },

      {
        path: ':stageID/voice-fullscreen',
        name: 'VOICE_FULLSCREEN__CIR',
        component: () =>
          import('/src/plugins/CIR/stages/VOICE/VoiceFullscreen.vue'),
        meta: meta4AssemblyDelegates,
      },
      {
        path: ':stageID/arguments',
        name: 'PROS_AND_CONS__CIR',
        component: () =>
          import('/src/plugins/CIR/stages/PROS_AND_CONS/Argumentarium.vue'),
        meta: { ...meta4AssemblyDelegates },
      },
      {
        path: 'profile/:stageID',
        name: 'PROFILEUPDATE__CIR',
        component: () =>
          import('/src/plugins/CIR/stages/PROFILEUPDATE/Index.vue'),
      },

      // Note: Define a route for each stageGroup/=> /stageID/<stage.group>
      {
        path: ':stageID/conclusion',
        name: 'CONCLUSION__CIR',
        component: () =>
          import('/src/plugins/CIR/stages/CONCLUSION/Conclusion.vue'),
        meta: meta4AssemblyDelegates,
      },
    ],
  },
];

export default routes;
