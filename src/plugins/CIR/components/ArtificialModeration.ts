// import { IStageGroup } from 'src/composables/stage.composable';
// import { Ref} from 'vue';
import { INodeTuple } from 'src/models/content';
// import { IStageTuple } from 'src/models/stage';

export interface ICtx {
  // groupsScheduled: IStageGroup[];
  // assemblyMenuItems: IStageGroup[];
  // unratedStatement: boolean;
  statementContentTuple: INodeTuple;
  openedAsScheduledTask: boolean;
  // nextScheduledStageGroup: IStageGroup;
  // gotoStage: (IStageTuple) => void;
  // [x: string]: any,
}

const AMs = {
  statementRating: {
    id: 'statementRating',
    prosa: ' Direkt unter dem Statement.',
    loading: (ctx: any) => !ctx.statementContentTuple?.content,
    items: [
      {
        id: 1,
        prosa: ' ... Bitte bewerten sie das statment.',
        condition: (ctx: any) =>
          !ctx.ownCirProfile && ctx.openedAsScheduledTask,
        body: () =>
          'Danke, dass Sie diese Stellungnahme lesen. Danach können wir weiterfahren.',
        buttons: [
          {
            action: (ctx) => ctx.closeDialog(),
            label: () => 'Schliessen',
          },
        ],
      },
      {
        id: 2,
        prosa: ' ... Zur Gesprächsecke.',
        condition: (ctx: any) =>
          !ctx.ownCirProfile && !ctx.openedAsScheduledTask && ctx.IsDelegate,
        body: () =>
          'Wenn Sie möchten, können Sie dieser Person eine Nachricht schreiben.',
        buttons: [
          {
            action: (ctx) => ctx.tabChange('discussion'),
            label: () => 'Zur Gesprächsecke',
          },
        ],
      },

      {
        id: 3,
        prosa: ' ... Zur Gesprächsecke.',
        condition: (ctx: any) =>
          ctx.ownCirProfile && ctx.statementContentTuple?.content.text,
        body: () =>
          'Sie können hier Ihr öffentliches Profil ändern, wenn Sie möchten.',
        buttons: [
          {
            action: (ctx) => ctx.closeDialog(),
            label: () => 'Schliessen',
          },
        ],
      },
      {
        id: 3,
        prosa: ' ... Zur Gesprächsecke.',
        condition: (ctx: any) =>
          ctx.ownCirProfile && !ctx.statementContentTuple?.content.text,
        body: () =>
          'Mögen Sie nun ein Statement zur Abstimmungsvorlage abgeben?',
        // buttons: [
        //   {
        //     action: (ctx) => ctx.closeDialog(),
        //     label: () => 'Schliessen',
        //   },
        // ],
      },
    ],
  },

  medaillen: {
    id: 'medaillen',
    prosa: ' Zuoberst im Medaillen-Popup.',
    // loading: (ctx: ICtx) => !ctx.statementContentTuple?.content,
    items: [
      {
        id: 1,
        condition: (ctx) =>
          ctx.user?.CERTIFICATES?.length > 1 &&
          ctx.user?.CERTIFICATES?.length < ctx.nof_certificates,
        prosa: 'current state.',
        body: (ctx) =>
          `Sie haben sich schon ${ctx.user?.CERTIFICATES?.length} von ${ctx.nof_certificates} Medaillen verdient. Wenn Sie mögen, können Sie auf der Plattform nach weiteren Medaillen Ausschau halten.`,
      },
      {
        id: 3,
        condition: (ctx) =>
          ctx.user?.CERTIFICATES?.length >= ctx.nof_certificates,
        prosa: 'current state.',
        body: (ctx) =>
          `Sie haben sich schon ${ctx.user?.CERTIFICATES?.length} von ${ctx.nof_certificates} Medaillen verdient. Ganz herzliche Gratulation!`,
      },
      {
        id: 2,
        condition: (ctx) => ctx.user?.CERTIFICATES?.length == 1,
        prosa: 'explaining medals.',
        body: () =>
          'Sie haben sich bereits die erste Medaille verdient. Wir gratulieren!',
      },
    ],
    // buttons: [
    //   {
    //     action: (ctx) => ctx.closeDialog(),
    //     label: () => 'Schliessen',
    //   },
    // ],
  },
};

export default AMs;
