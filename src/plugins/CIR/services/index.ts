/** DEMOKRATIFABRIK SERVICE MANAGER */
// import { ref } from 'vue';
import useCIRMenuService from './menu.service';
// import useCIRStatementService from './statement.composable';
// import useCIRTopicComposable from './topic.composable';
// import useAppService from 'src/services/app.service';
// import useAssemblyService from 'src/services/assembly.service';
// import useAuthService from 'src/services/auth.service';
// import useMonitorService from 'src/services/monitor.service';
// import useStageService from 'src/services/stage.service';

// export let store: ref<any> = Ref(undefined);
const initializedServices = {};

export const getCIRMenuService = () => {
  if (!initializedServices['menuService']) {
    // console.log('INIT menuService');
    const menuService = useCIRMenuService();
    initializedServices['menuService'] = menuService;
  }
  return initializedServices['menuService'];
};

// export const getCIRStatementService = () => {
//   if (!initializedServices['statementService']) {
//     console.log('INIT statementService');
//     const statementService = useCIRStatementService();
//     initializedServices['statementService'] = statementService;
//   }
//   return initializedServices['monitorService'];
// };

// export const getCIRTopicService = () => {
//   if (!initializedServices['topicService']) {
//     console.log('INIT topicService');
//     const topicService = useCIRTopicComposable();
//     initializedServices['topicService'] = topicService;
//   }
//   return initializedServices['topicService'];
// };
