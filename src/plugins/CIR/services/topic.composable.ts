/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import { computed, readonly, watch, Ref } from 'vue';
import { useStore } from 'vuex';
import { INodeTuple } from 'src/models/content';
import { IAssemblyUserTuple } from 'src/models/assembly';
import { IContentTreeTuple } from 'src/models/contenttree';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
// import useAssemblyService from 'src/composables/assembly.composable';
import { getAssemblyService } from 'src/services';

export default function useCIRTopicComposable(
  userTuple: Ref<IAssemblyUserTuple | null> | undefined = undefined,
  addWatcher = false
) {
  const store = useStore();
  const get_contenttree = store.getters['contenttreestore/get_contenttree'];
  const public_profile = computed(() => store.getters['profilestore/profile']);
  const { assembly } = getAssemblyService();
  const { userid } = usePKCEComposable();

  const otherTopicContentTuple = computed((): INodeTuple | null | undefined => {
    if (userTuple?.value?.topic_tuple) {
      return userTuple.value.topic_tuple;
    }
    return null;
  });

  const topicContentId = computed((): number | null => {
    if (assembly.value?.custom_data.TOPIC_CONTENT_ID) {
      return assembly.value.custom_data.TOPIC_CONTENT_ID;
    }
    return null;
  });

  const topicContenttreeIdentifier = computed((): string | null => {
    if (!assembly.value?.custom_data) {
      return null;
    }

    if (assembly.value.custom_data.TOPIC_CONTENTTREE_ID) {
      return `${assembly.value.custom_data.TOPIC_CONTENTTREE_ID}`;
    }

    return null;
    // throw 'Stage is not correctly configured: custom_data must contain TOPIC_CONTENTTREE_ID';
  });

  const topicContenttreeTuple = computed((): IContentTreeTuple | null => {
    if (!topicContenttreeIdentifier.value) {
      return null;
    }

    const contenttree = get_contenttree({
      contenttreeIdentifier: topicContenttreeIdentifier.value,
    });
    return contenttree;
  });

  const ownTopicContentTuple = computed((): INodeTuple | null => {
    if (!topicContentId.value || !topicContenttreeTuple.value) {
      return null;
    }
    const entries = topicContenttreeTuple.value.entries;
    if (!entries || !(topicContentId.value in entries)) {
      return null;
    }
    return entries[topicContentId.value];
  });

  const ownCirProfile = computed(() => {
    if (!userTuple) {
      return true;
    }

    return public_profile.value?.id == userTuple.value?.user.id;
  });

  const topicContentTuple = computed((): INodeTuple | null | undefined => {
    if (ownCirProfile.value) {
      return ownTopicContentTuple.value;
    }
    return otherTopicContentTuple.value;
  });

  if (addWatcher) {
    watch(
      () => topicContenttreeIdentifier.value,
      () => {
        if (topicContenttreeIdentifier.value) {
          // console.log('SYNC CONTENTTREE (6)', topicContenttreeIdentifier.value);
          store.dispatch('contenttreestore/syncContenttree', {
            assemblyIdentifier: assembly.value.identifier,
            contenttreeIdentifier: topicContenttreeIdentifier.value,
            oauthUserID: userid.value,
          });
        }
      },
      { immediate: true }
    );
  }

  return {
    topicContentTuple,
    topicContenttreeIdentifier: readonly(topicContenttreeIdentifier),
    topicContenttreeTuple: readonly(topicContenttreeTuple),
  };
}
