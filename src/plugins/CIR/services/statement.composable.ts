/**
 * Every user has assigned one content entry for a single opinion statement, which can
 * be edited and discussed at different stages.
 */
import { computed, readonly, watch, Ref } from 'vue';
import { useStore } from 'vuex';
import { INodeTuple } from 'src/models/content';
import {
  // IAssembly,
  IAssemblyProgression,
  IAssemblyUserTuple,
} from 'src/models/assembly';

import { IContentTreeTuple } from 'src/models/contenttree';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
import useContentComposable from 'src/composables/content.composable';

import constants from 'src/utils/constants';
import { getAssemblyService, getMonitorService } from 'src/services';

// TODO Use as service

export default function useCIRStatementService(
  userTuple: Ref<IAssemblyUserTuple | null> | undefined = undefined,
  addWatcher = false,
  statementMarkRead = false
) {
  const store = useStore();
  console.assert(!!store);

  const { assembly } = getAssemblyService();
  // const assembly = computed(
  //   (): IAssembly | null => store.getters['assemblystore/assembly']
  // );

  const assemblyProgression = computed(
    (): IAssemblyProgression | null =>
      store.getters['assemblystore/assemblyProgression']
  );

  const { userid } = usePKCEComposable();
  const { monitorFire } = getMonitorService();
  const { markRead } = useContentComposable();
  const get_contenttree = store.getters['contenttreestore/get_contenttree'];
  // useContenttreeComposable;
  const IsManager = computed(
    (): boolean => store.getters['assemblystore/IsManager']
  );
  watch(
    () => assemblyProgression.value,
    () => {
      if (IsManager.value) {
        // No focused content for managers/moderators..
        return;
      }

      if (
        assemblyProgression.value &&
        !assemblyProgression.value?.custom_data?.FOCUSED_CONTENT_ID
      ) {
        monitorFire(constants.MONITOR_ASSEMBLY_AUTO_CREATE_FOCUSED_CONTENT);
      }

      // console.log(
      //   'assemblyProgression.FOCUSED_CONTENT_ID',
      //   assemblyProgression.value?.custom_data?.FOCUSED_CONTENT_ID
      // );
    },
    { immediate: true }
  );

  const assemblyStatementContentId = computed((): number | null => {
    if (assemblyProgression.value?.custom_data?.FOCUSED_CONTENT_ID) {
      return assemblyProgression.value?.custom_data?.FOCUSED_CONTENT_ID;
    }
    return null;
  });

  const statementContenttreeId = computed((): number | null => {
    // console.log('get STATEMENT CONTENTTREE_ID.lll');
    if (assembly.value?.custom_data.STATEMENT_CONTENTTREE_ID) {
      return assembly.value.custom_data.STATEMENT_CONTENTTREE_ID;
    }
    return null;
  });

  const assemblyStatementContenttreeIdentifier = computed((): string | null => {
    if (!assemblyStatementContentId.value) {
      return null;
    }
    return `${statementContenttreeId.value}-${assemblyStatementContentId.value}`;
  });

  const statementContentId = computed((): number | null => {
    if (!userTuple) {
      return assemblyStatementContentId.value;
    }

    if (!userTuple.value?.statement_content_id) {
      return null;
    }

    return userTuple.value?.statement_content_id;
  });

  const assemblyUserStatementContenttreeIdentifier = computed(
    (): string | null => {
      if (!userTuple) {
        throw 'userTuple must be transmitted';
      }
      if (!userTuple.value?.statement_contenttree_id) {
        return null;
      }
      if (!userTuple.value?.statement_content_id) {
        return null;
      }
      return `${userTuple.value.statement_contenttree_id}-${userTuple.value.statement_content_id}`;
    }
  );

  const statementContenttreeIdentifier = computed((): string | null => {
    if (userTuple) {
      return assemblyUserStatementContenttreeIdentifier.value;
    }
    return assemblyStatementContenttreeIdentifier.value;
  });

  const statementContenttreeTuple = computed((): IContentTreeTuple | null => {
    if (!statementContenttreeIdentifier.value) {
      return null;
    }
    const contenttree = get_contenttree({
      contenttreeIdentifier: statementContenttreeIdentifier.value,
    });
    console.log(
      'DEBUG 32342',
      statementContenttreeIdentifier.value,
      contenttree
    );

    if (contenttree && !contenttree.id) {
      console.log('ERROR', contenttree, statementContenttreeIdentifier.value);
      return null;
      // throw 'contenttree not correctly loaded...';
    }
    // console.log('DEBUG: new statementContenttreeTuple (111)', contenttree);

    return contenttree;
  });

  const contenttreeEntries = computed((): Record<string, INodeTuple> | null => {
    if (!statementContenttreeTuple.value) {
      return null;
    }
    return statementContenttreeTuple.value?.entries;
  });

  const statementContentTuple = computed((): INodeTuple | null => {
    if (!statementContenttreeTuple.value) {
      // not yet ready
      return null;
    }

    console.log('DEBUG ooooo');
    if (!contenttreeEntries.value) {
      return null;
      // throw 'invalid contenttree entries';
    }
    const id = statementContenttreeTuple.value?.rootElementIds[0];

    // console.log('sssssssssssssssssssssssss', id, contenttreeEntries.value);
    return contenttreeEntries.value[`${id}`];
  });

  // store.dispatch('contenttreestore/syncContenttree', {
  //   assemblyIdentifier: assemblyIdentifier.value,
  //   contenttreeIdentifier: contenttreeIdentifier.value,
  //   oauthUserID: userid.value,
  // });

  if (addWatcher) {
    watch(
      () => statementContenttreeIdentifier.value,
      () => {
        // console.log(
        //   'WATCHER',
        //   assembly.value,
        //   statementContenttreeIdentifier.value
        // );
        if (
          statementContenttreeIdentifier.value &&
          assembly.value?.identifier
        ) {
          store.dispatch('contenttreestore/syncContenttree', {
            assemblyIdentifier: assembly.value.identifier,
            contenttreeIdentifier: statementContenttreeIdentifier.value,
            oauthUserID: userid.value,
          });
        }
      },
      { immediate: true }
    );
  }

  if (statementMarkRead) {
    watch(
      () => statementContentTuple.value,
      () => {
        if (statementContentTuple.value) {
          markRead(statementContentTuple.value);
        }
      },
      { immediate: true }
    );
  }

  return {
    statementContentId: readonly(statementContentId),
    statementContentTuple: readonly(statementContentTuple),
    statementContenttreeTuple: readonly(statementContenttreeTuple),
    statementContenttreeIdentifier: readonly(statementContenttreeIdentifier),
  };
}
