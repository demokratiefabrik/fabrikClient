/** DEMOKRATIFABRIK RUNTIME VARIABLES */

import { shallowRef } from 'vue';
import { getAssemblyService } from 'src/services';
import { getStageService } from 'src/services';

import { IStageGroup } from 'src/models/stage';

// Footer bellow TOC
const tocFooterComponent = shallowRef<any>(null);
import('../components/TocIndexFooter.vue').then((comp) => {
  tocFooterComponent.value = comp.default;
});

const assemblyMenuAMs = shallowRef<any>(null);
import('../ArtificialModeration').then((comp) => {
  assemblyMenuAMs.value = comp.default;
});
export default function useCIRMenuService() {
  const { getStageRoute } = getAssemblyService();
  const { getFirstOrRoutedStageIDByGroup } = getStageService();

  const assemblyMenuData: Record<string, IStageGroup> = {
    information: {
      name: 'information',
      label: 'Vorlage',
      icon: 'mdi-sign-direction',
      // constants.ICONS.VAA_TOPIC,
      description:
        'Erfahren Sie, worum es beim Bundesgesetz über die Ziele im Klimaschutz, die Innovation und die Stärkung der Energiesicherheit geht.',
      // actionText:
      //   'Ihre Aufgabe: Hier können Sie sich über den Inhalt der Vorlage informieren und ihre eigene Position dazu darlegen.',
      tooltip:
        'Erfahren Sie, worum es beim Bundesgesetz über die Ziele im Klimaschutz, die Innovation und die Stärkung der Energiesicherheit geht.',
      expanded: () => false,
      expandable: false,
      to: () => {
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('information');
        return getStageRoute(firstStageTuple);
      },
    },

    voice: {
      name: 'voice',
      label: 'Teilnehmerschaft', // MEINUNGSRAUM
      icon: 'mdi-chat-outline',
      description:
        'Erfahren Sie mehr über die anderen Teilnehmenden. Wie kommt die Abstimmungsvorlage im Teilnehmendenkreis an?',
      // actionText:
      //   'Ihre Aufgabe: Hier können Sie sich über die Meinungen und Positionen der anderen Teilnehmenden informieren und mit diesen diskutieren.',
      tooltip:
        'Erfahren Sie mehr über die anderen Teilnehmenden. Wie kommt die Abstimmungsvorlage im Teilnehmendenkreis an?',
      expanded: () => false,
      expandable: false,
      // manual_expanded: false,
      to: () => {
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('voice');
        return getStageRoute(firstStageTuple);
      },
    },

    arguments: {
      name: 'arguments',
      label: 'Argumentarium',
      description:
        'Nun wird es konkret: Was sind für Sie die wichtigsten Argumente für oder gegen die Vorlage? Für welche Argumente bringen Sie am meisten Verständnis auf?',
      // actionText:
      //   'Ihre Aufgabe: Hier können Sie sich vertieft mit den Argumenten von anderen Teilnehmenden auseinandersetzen und diese bewerten. Allenfalls werden Sie auch zufällig eingeladen, ein Gutachten zu einem dieser Argumente zu erstellen. Zudem können Sie eigene, neue Argumente vorschlagen.',
      icon: 'mdi-plus-minus-variant',
      expanded: () => false,
      tooltip:
        'Nun wird es konkret: Was sind für Sie die wichtigsten Argumente für oder gegen die Vorlage? Für welche Argumente bringen Sie am meisten Verständnis auf?',
      to: () => {
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('arguments');
        return getStageRoute(firstStageTuple);
      },
    },

    conclusion: {
      name: 'conclusion',
      label: 'Zwischenstand',

      description:
        'Welche Argumente kommen bei den anderen Teilnehmenden am besten an? Hier stellen wir Ihnen einige erste, provisorische Resultate zusammen und geben Ihnen einen Ausblick, wie es in der Demokratiefabrik weitergeht.',
      // actionText:
      //   'Ihre Aufgabe: Hier können Sie Ihre Position zur Vorlage nochmals anpassen, wenn Sie möchten.',
      tooltip:
        'Welche Argumente kommen bei den anderen Teilnehmenden am besten an? Hier stellen wir Ihnen einige erste, provisorische Resultate zusammen und geben Ihnen einen Ausblick, wie es in der Demokratiefabrik weitergeht. ',
      // disabled: this.groupsAccessible?.includes("conclusion"),
      icon: 'mdi-flag-checkered',
      expanded: () => false,
      to: () => {
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('conclusion');
        return getStageRoute(firstStageTuple);
      },
    },
  };
  // console.log('DEBUG === ', assemblyMenuAMs.value);
  return {
    assemblyMenuData,
    assemblyMenuAMs,
    tocFooterComponent,
  };
}
