import { IStageGroup, IStageTuple } from 'src/models/stage';

export interface IAmToc {
  groupsScheduled: IStageGroup[];
  assemblyMenuItems: IStageGroup[];
  stages_by_groups: Record<string, IStageTuple[]>;
  nextScheduledStage: IStageTuple;
  enabled_certificates: boolean;
  nextScheduledStageGroup: IStageGroup;
  gotoStage: (IStageTuple) => void;
  // [x: string]: any,
}

const AMs = {
  toc: {
    id: 'toc',
    prosa: ' Wird bei der Tagesübersicht angzeigt.',
    loading: (ctx: IAmToc) => {
      // console.log('LOADING', ctx.nextScheduledStageGroup?.name);
      // console.log('TEST', !ctx.groupsScheduled.length);
      return (
        !ctx.assemblyMenuItems?.length ||
        !ctx.stages_by_groups ||
        ctx.groupsScheduled === undefined
      );
    },
    items: [
      {
        id: 3,
        prosa:
          ' ... INFORMATION: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStage.stage.group == 'information',
        body: (ctx) => {
          // const chapter = ctx.nextScheduledStage.stage.title;
          return [
            ctx.enabled_certificates
              ? 'Sie haben in der Demokratiefabrik die Möglichkeit, fünf Medaillen für die Erledigung verschiedener Aufgaben zu gewinnen.'
              : '',
            'In einem ersten Schritt möchten wir, dass Sie sich das Kapitel «Abstimmungsvorlage» ansehen. Machen Sie mit?',
          ];
        },
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ich komme mit!',
          },
        ],
      },
      {
        id: 21,
        prosa: ' OK... TEILNEHMERSCHAFT (Spätere Sessions)',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStageGroup?.name == 'voice' &&
          ctx.nextScheduledStage.progression,
        body: (ctx) => {
          // const chapter = ctx.nextScheduledStageGroup.toc_label ? ctx.nextScheduledStageGroup.toc_label : ctx.nextScheduledStageGroup.label
          return ctx.nextScheduledStage.progression.number_of_day_sessions > 1
            ? 'Kommen Sie doch nochmals mit zur Übersicht über die Teilnehmerschaft.'
            : 'Kommen Sie doch mit zur Übersicht über die Teilnehmerschaft.'; // MEINUNGSRAUM
        },
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ja, gern.',
          },
        ],
      },

      {
        id: 22,
        prosa:
          ' ... ARGUMENTARIUM: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStageGroup?.name == 'arguments',
        body: () => 'Nun brauchen wir Ihre Hilfe beim Argumentarium.',
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ja, ich komme mit!',
          },
        ],
      },
      {
        id: 2,
        prosa: ' ... CONCLUSION.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStageGroup?.name == 'conclusion',
        body: () =>
          'Sehr schön! Sie haben unsere Fragen alle beantwortet. Vielen Dank. Sie können sich nun noch den Zwischenstand ansehen.',
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Zum Zwischenstand',
          },
        ],
      },
      {
        id: 1,
        prosa: ' ... gibt nichts mehr zu tun',
        condition: (ctx: IAmToc) => !ctx.groupsScheduled.length,
        body: () =>
          'Sie haben für heute die wichtigsten Traktanden erledigt. Morgen früh halten wir wieder neue Aufgaben für Sie bereit. ',
      },
    ],
  },
};

export default AMs;
