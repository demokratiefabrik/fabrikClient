import useXHR from 'src/utils/xhr';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
import { IPolarBeeRequest } from '../stages/VOICE/models/plots';
import { IAssemblyUserTuple } from 'src/models/assembly';

interface ISVGResponse {
  data: ISVGResponseData;
}

interface IResponseProfilesToChallenge {
  data: IAssemblyUserTuple[];
}

const xhr = useXHR();

interface ISVGResponseData {
  svg: string;
  totalUserCount: number;
  plottedUserCount: number;
  // plottedUsers: IAssemblyUser[];
}

export default {
  // CIR
  // *********************************
  /**
   * Get CIR plot svg..
   */
  polarbee: async (
    assemblyIdentifier: string,
    request: IPolarBeeRequest
  ): Promise<ISVGResponse> => {
    // content/${contentId}
    const url = `/assembly/${assemblyIdentifier}/cirplot?distribution=${request.distribution}&marker-color-map=${request.markerColorMap}&maxNumber=${request.maxNumber}&marker-size-factor=${request.markerSizeFactor}`;

    const { refresh_token_if_required } = usePKCEComposable();
    await refresh_token_if_required();

    const data = {
      method: 'get',
      url: url,
    };
    return xhr.customRequest(data) as Promise<ISVGResponse>;
  },

  /**
   * Get CIR plot svg..
   */
  profilesToChallenge: async (
    assemblyIdentifier: string,
    stageId: number
  ): Promise<IResponseProfilesToChallenge> => {
    const url = `/assembly/${assemblyIdentifier}/stage/${stageId}/profilesToChallenge`;
    const { refresh_token_if_required } = usePKCEComposable();
    await refresh_token_if_required();

    const data = {
      method: 'get',
      url: url,
    };
    return xhr.customRequest(data);
  },

  /**
   * Manage Voices
   */
  manageVoices: async (
    assemblyIdentifier: string,
    filter: string,
    filterToggle: string
  ): Promise<any> => {
    const url = `/assembly/${assemblyIdentifier}/manage_voices`;
    const { refresh_token_if_required } = usePKCEComposable();
    await refresh_token_if_required();

    const data = {
      method: 'post',
      data: { filter, filterToggle },
      url: url,
      headers: {
        'content-type': 'application/json',
      },
    };
    return xhr.customRequest(data);
  },

  /**
   * Manage Voices
   */
  manageArguments: async (
    assemblyIdentifier: string,
    filter: string
  ): Promise<any> => {
    const url = `/assembly/${assemblyIdentifier}/manage_arguments`;
    const { refresh_token_if_required } = usePKCEComposable();
    await refresh_token_if_required();

    const data = {
      method: 'post',
      data: { filter },
      url: url,
      headers: {
        'content-type': 'application/json',
      },
    };
    return xhr.customRequest(data);
  },
};
