import mitt from 'mitt';
// import { IDialog, INotificationBanner } from 'src/models/layout';
// import { IStageTuple } from 'src/models/stage';

type Events = {
  notifyPublicCirProfileUpdate: undefined; // either rating or statement has been modified...
};

const emitter_CIR = mitt<Events>();

export default function useEmitter() {
  return emitter_CIR;
}
