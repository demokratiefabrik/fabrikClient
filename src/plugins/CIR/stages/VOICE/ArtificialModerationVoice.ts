// import { IStageGroup } from 'src/composables/stage.composable';
// import { IStageGroup, IStageTuple } from 'src/models/stage';

// import { Ref } from 'vue';
import { INodeTuple } from 'src/models/content';
import { IMilestoneComposable, IStageTuple } from 'src/models/stage';

export interface ICtx {
  nofReadFeedback: number;
  beePlotUserCount: number;
  markUnAlert: (stage: null | IStageTuple) => void;
  topicContentTuple: INodeTuple;
  nextScheduledStage: IStageTuple;
  scrollToTop: () => void;
  msChallengeOfOthers: IMilestoneComposable;
  // [x: string]: any,
}

const AMs = {
  AMmsVoiceIntroduction: {
    id: 'AMmsInitialPreference',
    items: [
      {
        id: 1,
        prosa: 'what to do on voice page.',
        body: () => [
          'Hier können Sie sich über die Meinungen und Positionen der anderen Teilnehmenden informieren und mit diesen diskutieren.',
        ],
      },
    ],
  },

  AMmsChallengeOfOthers: {
    id: 'AMmsChallengeOfOthers',
    items: [
      {
        id: 1,
        prosa: 'give feedback to other users.',
        body: (ctx) => [
          // 'Haben Sie Verständnis für diese Stellungnahmen? Welchen Aspekt der Vorlage sollen sich die Teilnehmenden nochmals durch den Kopf gehen lassen.',
          ctx.profilesToChallenge?.length == 1
            ? 'Wir bitten Sie, sich eine Stellungnahme näher anzuschauen. Klicken Sie dazu auf den Benutzernamen.'
            : `Wir bitten Sie, sich ${ctx.profilesToChallenge?.length} Stellungnahmen näher anzuschauen. Klicken Sie dazu auf die Benutzernamen.`,
          // 'Bitte bewerten Sie die Stellungnahmen.',
        ],
        // buttons: [
        //   {
        //     action: (ctx) => ctx.msChallengeOfOthers.markAchieved(),
        //     label: () => 'Gerne weiterfahren...',
        //   },
        // ],
      },
    ],
  },

  AMmsPolarBeePlot: {
    id: 'msPolarBeePlot',
    items: [
      {
        id: 101,
        prosa: 'below the polar-bee-plot (middle of the round).',
        condition: (ctx: ICtx) =>
          ctx.beePlotUserCount > 1 &&
          ctx.nextScheduledStage &&
          (ctx.nextScheduledStage.stage.group === 'voice' ||
            ctx.nextScheduledStage.stage.group === 'arguments'),
        body: (ctx) => [
          ctx.beePlotUserCount > 10
            ? 'Und wer sind die anderen Teilnehmenden? Hier erhalten Sie einen Überblick über diejenigen, die sich aktuell schon beteiligt haben.'
            : '',
          !ctx.q?.platform?.is?.mobile
            ? 'Wenn Sie auf einen Punkt klicken, erhalten Sie mehr Informationen zu dieser Person.'
            : '',
        ],
      },
    ],
  },

  endOfPage: {
    id: 'endOfPage',
    items: [
      {
        id: 101,
        prosa: 'Continue.',
        condition: (ctx: ICtx) =>
          ctx.nextScheduledStage &&
          ctx.msChallengeOfOthers.achieved.value &&
          (ctx.nextScheduledStage.stage.group === 'voice' ||
            ctx.nextScheduledStage.stage.group === 'arguments'),
        body: () => [
          'Sobald Sie bereit sind, gehen wir weiter zum nächsten Abschnitt.',
        ],
        buttons: [
          {
            action: (ctx) => {
              console.log(
                'DEBUG: ',
                ctx.isRoutedStageAlerted,
                ctx.nextScheduledStage
              );
              if (ctx.isRoutedStageAlerted) {
                ctx.markUnAlert(ctx.nextScheduledStage);
              }
              ctx.gotoScheduledStage();
            },
            label: () => 'Weiter zum Argumentarium...',
          },
        ],
      },
      {
        id: 102,
        prosa: 'final message (after the round).',
        condition: (ctx: ICtx) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStage.stage.group === 'conclusion',
        body: () => [
          'Nun denn, sobald Sie bereit sind können wir zum Zwischenfazit gelangen.',
        ],
        buttons: [
          {
            action: (ctx) => {
              // if (ctx.msChallengeByOthers) {
              // //   ctx.msChallengeByOthers.markAchieved();
              // // }
              ctx.markUnAlert();
              ctx.gotoScheduledStage();
            },
            label: () => 'Weiter zum Zwischenfazit...',
          },
        ],
      },
      {
        id: 103,
        prosa: 'final message - Free discussion...',
        condition: (ctx: ICtx) => !ctx.nextScheduledStage,
        body: () => ['Und hier gelangen Sie wieder ganz nach oben.'],
        buttons: [
          {
            action: (ctx) => {
              ctx.scrollToTop();
            },
            label: () => 'Nach oben, bitte!',
          },
        ],
      },
    ],
  },
};

export default AMs;
