export interface IPolarBeeRequest {
  distribution: string;
  markerColorMap: string;
  markerSizeFactor: number;
  maxNumber: number;
  // divisor: number;
}

export type IPosition = {
  x: number;
  y: number;
};

export type IOffsets = {
  w: number;
  h: number;
};

export type IViewBox = {
  x: number;
  y: number;
  w: number;
  h: number;
};

export interface IHighlightEvent {
  id: number;
  ev: MouseEvent;
  label: string;
  pos: number;
  elementOffsets: IPosition;
}
