/** DEMOKRATIFABRIK Polar Bee Plot VARIABLES */
import { ref, reactive, readonly } from 'vue';
import { IPolarBeeRequest } from '../models/plots';
import cirapi from '../../../utils/api';
import { getAppService } from 'src/services';
import useStoreService from 'src/services/store.service';

const storedPolarBeePlot = ref<null | string>(null);
const requestParams = reactive<IPolarBeeRequest>({
  distribution: 'default',
  markerColorMap: 'winter',
  markerSizeFactor: 1,
  maxNumber: 300,
  // divisor: 1,
});

export default function usePolarBeePlotComposable(maxNumber) {
  const { showServiceError } = getAppService();
  const { assemblyIdentifier } = useStoreService();

  const loading = ref<null | boolean>(null);
  const error = ref<null | boolean>(null);
  const postProcessed = ref<null | boolean>(null);
  const totalUserCount = ref<null | number>(null);
  const plottedUserCount = ref<null | number>(null);

  if (maxNumber) {
    requestParams.maxNumber = maxNumber;
  }
  const retrievePolarBeePlot = async (): Promise<void> => {
    loading.value = true;
    postProcessed.value = false;

    if (!assemblyIdentifier.value) {
      throw 'assemblyIdentifier not found';
    }

    return cirapi
      .polarbee(assemblyIdentifier.value, requestParams)
      .then((response) => {
        storedPolarBeePlot.value = `${response.data.svg}<span id='svgLoaded'></span>`;
        plottedUserCount.value = response.data.plottedUserCount;
        totalUserCount.value = response.data.totalUserCount;
      })
      .catch((error) => {
        showServiceError(error);
        console.warn(error); // timeout!
        return Promise.reject();
      });
  };

  return {
    storedPolarBeePlot: readonly(storedPolarBeePlot),
    requestParams,
    loading,
    error,
    postProcessed,
    retrievePolarBeePlot,
    totalUserCount,
    plottedUserCount,
  };
}
