import { IAssemblyTuple } from 'src/models/assembly';
import { INodeTuple } from 'src/models/content';
import { IStageTuple } from 'src/models/stage';
import {
  IArtificialModeration,
  IArtificialModerationGroup,
} from 'src/pages/components/artificial_moderation/model';

import library from 'src/utils/library';
const { loaded } = library;

export interface ICtx {
  nofReadFeedback: number;
  markUnAlert: (stage: null | IStageTuple) => void;
  topicContentTuple: INodeTuple;
  nextScheduledStage: IStageTuple;
  scrollToTop: () => void;
  routedStageTuple: IStageTuple | null;
  isRoutedStageAlerted: boolean;
  gotoAssemblyStart: (IAssemblyTuple) => void;
  assembly: IAssemblyTuple;
  isFirstText: boolean;
  toSubmitInitialPreference: boolean;
  commpletedProfile: boolean;
  gotoStage: (IStageTuple) => void;
  markCompleted: () => void;
  loaded: (any) => boolean;
  node: INodeTuple;
  showCIRProfileForm: () => void;
  // [x: string]: any,
}

const AMs = {
  index_top: {
    id: 'index_top',
    prosa: ' Leitet eine Text-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 10,
        prosa: 'Irgendwann, nachdem rundgang zu Ende ist.',
        body: () =>
          'Hier können Sie sich über den Inhalt der Vorlage informieren.',
      },
    ] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  statement_instruction: {
    id: 'statement_instruction',
    prosa: ' Anweisung zur Eingabe des Statements...',
    items: [
      {
        id: 1,
        body: () => [
          'Bitte geben Sie für die anderen Teilnehmenden an, wie Sie momentan zur Vorlage stehen.',
          'Die Teilnehmenden wären Ihnen dankbar, wenn Sie Ihre Position in wenigen Sätzen begründen.',
        ],
      },
    ] as IArtificialModeration[],
  },

  ask_for_statement_: {
    id: 'ask_for_statement_',
    prosa: ' Erster Besuch / mit leerem Profile Statement!.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 1,
        prosa: ' Erster Besuch / mit leerem Profile Statement!.',
        condition: (ctx: ICtx) => !ctx.commpletedProfile,
        body: () =>
          'Sie haben den Text zu Ende gelesen? Dann folgen Sie mir bitte.',
        buttons: [
          {
            action: (ctx) => ctx.showCIRProfileForm(),
            label: () => 'Ja, bitte!',
          },
        ],
      },
    ],
  },

  index_bottom: {
    id: 'index_bottom',
    prosa: ' Schliesst die Text-Stage ab.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 2,
        prosa: ' Zweiter Besuch (ohne Statement Submitting)',
        condition: (ctx: ICtx) =>
          ctx.nextScheduledStage &&
          ctx.commpletedProfile &&
          !ctx.toSubmitInitialPreference,
        body: () => 'Möchten Sie fortfahren?',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert(null);
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, gerne fortfahren!',
          },
          {
            action: (ctx: ICtx) => {
              ctx.showCIRProfileForm();
            },
            label: () => 'Stellungnahme nochmals bearbeiten',
          },
        ],
      },
      {
        id: 3,
        prosa: ' Nach Statement Submitting',
        condition: (ctx: ICtx) =>
          ctx.nextScheduledStage &&
          ctx.commpletedProfile &&
          ctx.toSubmitInitialPreference,
        body: () => 'Vielen Dank! Dann gehen wir doch gleich weiter.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert(null);
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte weiterfahren!',
          },
          {
            action: (ctx: ICtx) => {
              ctx.showCIRProfileForm();
            },
            label: () => 'Stellungnahme nochmals bearbeiten',
          },
        ],
      },
      {
        id: 10,
        prosa: 'Nachdem Rundgang zu Ende ist.',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && !ctx.nextScheduledStage,
        body: () => 'Hier geht es zurück nach oben.',
        buttons: [
          {
            action: (ctx) => {
              ctx.scrollToTop();
            },
            label: () => 'Nach oben, bitte!',
          },
        ],
      },
    ] as IArtificialModeration[],
  },

  faq: {
    id: 'discussion_index_top',
    loading: (ctx: ICtx) => !loaded(ctx.node),
    items: [
      {
        id: 2,
        body: () =>
          'Sie können hier eine Frage stellen. Die anderen Teilnehmenden oder die Organisatoren werden sie sehr bald beantworten.',
      },
    ] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  msInitialPreference: {
    id: 'msInitialPreference',
    items: [
      {
        id: 1,
        condition: (ctx: ICtx) =>
          !ctx.topicContentTuple ||
          !loaded(ctx.topicContentTuple.progression?.rating),
        prosa: 'please complete the public cir Profile.',
        body: () => [
          'Als erstes möchten wir mehr über Ihre aktuelle Position zur Vorlage erfahren.',
        ],
      },
    ],
  },

  msChallengeByOthers: {
    id: 'msChallengeByOthers',
    items: [
      {
        id: 1,
        prosa: 'first feedbacks, User must see the feedbacks to progress.',
        condition: (ctx: ICtx) => ctx.nofReadFeedback == 0,
        body: () => [
          'Sie haben von anderen Teilnehmenden bereits einen Kommentar zu Ihrer Stellungnahme bekommen.',
          'Würden Sie diesen bitte kurz lesen und allenfalls darauf reagieren?',
        ],
        buttons: [
          {
            action: (ctx) => ctx.openPublicCirProfile(),
            label: () => 'Stellungnahme öffnen',
          },
        ],
      },
      {
        id: 2,
        prosa: 'further feedbacks, User must see the feedbacks to progress.',
        condition: (ctx: ICtx) => ctx.nofReadFeedback > 0,
        body: () => [
          'Sie haben von anderen Teilnehmenden erneut einen Kommentar zu Ihrer Stellungnahme bekommen.',
          'Möchten Sie diesen nun lesen?',
        ],
        buttons: [
          {
            action: (ctx) => ctx.msChallengeByOthers.markAchieved(),
            label: () => 'Gerne weiterfahren...',
          },
        ],
      },
    ],
  },
};

export default AMs;
