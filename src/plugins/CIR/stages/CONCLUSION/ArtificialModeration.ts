import { IStageGroup, IStageTuple } from 'src/models/stage';

export interface IAmToc {
  groupsScheduled: IStageGroup[];
  assemblyMenuItems: IStageGroup[];
  stages_by_groups: Record<string, IStageTuple[]>;
  nextScheduledStage: IStageTuple;
  nextScheduledStageGroup: IStageGroup;
  gotoStage: (IStageTuple) => void;
  // [x: string]: any,
}

const AMs = {
  conclusionIntroA: {
    id: 'conclusionIntroA',
    items: [
      {
        id: 1,
        body: () => [
          'Wir danken Ihnen im Namen des ganzen Demokratiefabrik-Teams für Ihre wertvolle Mitarbeit!',
          // `${actorPartnerReference} und ich machen nun Pause. Wir stehen Ihnen ab morgen wieder zur Verfügung!`,
        ],
      },
    ],
  },

  conclusionIntroB: {
    id: 'conclusionIntroB',
    items: [
      {
        id: 2,
        body: (ctx) => [
          ctx.linkRedirectionDisabled
            ? 'Nun folgt noch eine kurze Übersicht zum aktuellen Stand der Demokratiefabrik.'
            : 'Sie haben es fast geschafft! Es folgt noch eine kurze Übersicht zum aktuellen Stand der Demokratiefabrik.',
          ctx.surveyFinalized
            ? 'Am Ende erfahren Sie auch, wie es mit der Demokratiefabrik weitergeht.'
            : ctx.linkRedirectionDisabled
            ? ''
            : 'Dann leiten wir Sie zurück zu LINK.',
          // 'Sie haben es heute fast geschafft. Es folgt nun noch eine kleine Übersicht über den aktuellen Stand des Argumentariums. Am Ende erfahren Sie noch, wie es mit der Demokratiefabrik weitergeht.',
        ],
      },
    ],
  },

  conclusionA: {
    id: 'conclusionA',
    items: [
      {
        id: 1,
        body: (_ctx, { actorPartnerReference }) => [
          // 'Wir danken Ihnen im Namen des ganzen Demokratiefabrik-Teams für Ihre wertvolle Mitarbeit!',
          `${actorPartnerReference} und ich machen nun Pause. Wir stehen Ihnen ab morgen wieder zur Verfügung!`,
        ],
      },
    ],
  },

  conclusionB: {
    id: 'conclusionB',
    items: [
      {
        id: 2,
        body: () => [
          'Wenn Sie Lust haben, können Sie sich gerne selbstständig auf eine Entdeckungstour durch die Demokratiefabrik begeben.',
          // 'Unten folgt noch eine kleine Übersicht über den aktuellen Stand des Argumentariums.',
        ],
      },
    ],
  },

  conclusionC: {
    id: 'conclusionC',
    items: [
      {
        id: 3,
        body: () => 'EMPTY',
      },
    ],
    buttons: [
      {
        action: (ctx) => {
          ctx.setSurveyFinalized();
        },
        label: () => 'Weiter zu LINK',
      },
    ],
  },
  msRevisePreference: {
    id: 'msRevisePreference',
    condition: (ctx: any) => ctx.statementContentTuple?.content,
    items: [
      {
        id: 1,
        prosa: 'please revise the public cir Profile.',
        condition: (ctx: any) => ctx.statementContentTuple?.content?.text,
        body: () => [
          'Sind Sie mit Ihrer Stellungnahme zur Abstimmungsvorlage noch zufrieden? Sie können hier noch Änderungen vornehmen.',
        ],
      },
      {
        id: 2,
        prosa: 'please add public cir Profile.',
        condition: (ctx: any) => !ctx.statementContentTuple?.content?.text,
        body: () => [
          'Sie haben noch keine Stellungnahme zur Abstimmungsvorlage abgegeben. Es wäre schön, wenn Sie dies noch tun könnten.',
        ],
      },
    ],
  },
};

export default AMs;
