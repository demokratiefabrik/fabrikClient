import { IAssemblyTuple } from 'src/models/assembly';
import { INodeTuple } from 'src/models/content';
import { IStageTuple } from 'src/models/stage';
import {
  IArtificialModeration,
  IArtificialModerationFile,
  IArtificialModerationGroup,
} from 'src/pages/components/artificial_moderation/model';
// import constants from 'src/utils/constants';
import { Ref } from 'vue';

export interface ICtx {
  $q: any;
  routedStageTuple: IStageTuple | null;
  nextScheduledStage: Ref<IStageTuple>;
  firstSide: 'PRO' | 'CONTRA';
  isRoutedStageAlerted: boolean;
  assembly: IAssemblyTuple;
  isFirstText: boolean;
  gotoStage: (IStageTuple) => void;
  scrollToTop: () => void;
  markUnAlert: () => void;
  loaded: (any) => boolean;
  node: INodeTuple;
  nofArgumentsToRate: number;
  [propName: string]: any;
}

export const AMs = {
  index_top_one: {
    id: 'index_top_one',
    prosa: ' Leitet eine Pros-cons-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 11,
        prosa: 'Erster Besuch. (erste Seite)',
        // condition: (ctx: ICtx) => ctx.isRoutedStageAlerted,
        body: () => [
          'Dies ist unser zentrales Anliegen. Hier werden die wichtigsten Argumente für oder gegen die Abstimmungsvorlage zusammengetragen und bewertet.',
          'Sie selbst können neue Argumente vorschlagen, Änderungsvorschläge einreichen und von Zeit zu Zeit die Anträge der anderen Teilnehmenden prüfen.',
        ],
      },
    ],
  },
  index_top: {
    id: 'index_top',
    prosa: ' Leitet eine Pros-cons-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 3,
        prosa: ' Zweiter Besuch',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () =>
          'Heute haben wir hier keine konkreten Fragen mehr an Sie. Soweit vielen Dank für Ihr Engagement.',
      },
      {
        id: 10,
        prosa: 'Irgendwann, nachdem rundgang zu Ende ist.',
        condition: (ctx: ICtx) => !ctx.nextScheduledStage,
        body: () => 'Möchten Sie am Argumentarium noch etwas ändern? Nur zu!',
      },
    ] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  index_middle: {
    id: 'index_middle',
    prosa: ' Zweite Staffel von Argumenten.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    condition: (ctx: ICtx) => ctx.isRoutedStageAlerted,
    items: [
      {
        id: 1,
        prosa: 'Erster Besuch.',
        body: (ctx: ICtx) => [
          // 'Nun kommen wir zu der anderen Seite.',
          ctx.secondSide === 'PRO'
            ? 'Wir kommen nun zu den Pro-Argumenten.'
            : 'Wir kommen nun zu den Contra-Argumenten.',
        ],
      },
    ] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  index_bottom_left: {
    id: 'index_bottom_left',
    prosa: ' Frage zum Argumentarium.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 1,
        prosa: ' Erster Besuch.',
        condition: (ctx: ICtx) =>
          ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () => [
          'Widerspiegelt diese Grafik Ihre Position zur Abstimmungsvorlage?',
          'Sie können die Bewertung der Argumente oben jederzeit wieder ändern.',
        ],
      },
    ] as IArtificialModeration[],
  },

  index_bottom_right: {
    id: 'index_bottom',
    prosa: ' Schliesst die Text-Stage ab.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 1,
        prosa: ' Erster Besuch.',
        condition: (ctx: ICtx) =>
          ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () =>
          'Möchten Sie die Argumente vorerst so stehen lassen und den Rundgang für heute abschliessen? Dann folgen Sie mir bitte.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              if (ctx.nextScheduledStage) {
                ctx.gotoStage(ctx.nextScheduledStage);
              } else {
                ctx.gotoNextStageNr(ctx.routedStageTuple);
              }
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },
      {
        id: 2,
        prosa: ' Zweiter Besuch',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () =>
          'Heute haben wir hier keine konkreten Fragen mehr an Sie. Kommen Sie doch mit zum Zwischenstand.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },

      {
        id: 10,
        prosa: 'Irgendwann, nachdem Rundgang zu Ende ist.',
        condition: (ctx: ICtx) => !ctx.nextScheduledStage,
        body: () => 'Hier geht es zurück nach oben.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.scrollToTop();
              // ctx.gotoNextStageNr(ctx.routedStageTuple);
            },
            label: () => 'Nach Oben',
          },
        ],
      },
    ] as IArtificialModeration[],
  },

  arguments_rating: {
    id: 'arguments_rating',
    prosa: ' Erläuterungen zum aktuellen Stand einer Argumentarium Seite.',
    items: [
      {
        id: 1,
        condition: (ctx) => ctx.nofArgumentsToRate <= 6 && ctx.firstSide,
        body: (ctx) =>
          // TODO: SECOND ROUND: "Bewerten Sie bitte auch diese Argumente.""
          ctx.nofArgumentsToRate === 1
            ? [
                'Wie relevant ist das gelb markierte Argument für Ihren persönlichen Stimmentscheid?',
              ]
            : [
                'Wie relevant sind folgende Argumente für Ihren persönlichen Stimmentscheid?',
                'Öffnen Sie die gelb markierten Argumente und bewerten Sie sie.',
              ],
      },
      {
        id: 2,
        condition: (ctx) => ctx.nofArgumentsToRate <= 6 && !ctx.firstSide,
        body: () => 'Bewerten Sie bitte alle gelb markierten Argumente.',
      },
      {
        id: 104,
        condition: (ctx) => ctx.nofArgumentsToRate > 6,
        body: () => [
          'Können Sie die gelb markierten Argumente bewerten? Wie relevant finden Sie sie?',
        ],
      },
    ],

    // {
    //   id: 2,
    //   condition: (ctx) => ctx.milestoneSALIENCE && !ctx.milestonePEERREVIEW,
    //   body: () => 'Gerne würden wir nun mit den Gutachten fortfahren.',
    //   buttons: [
    //     {
    //       action: (ctx) => {
    //         ctx.milestone('SALIENCE', 3);
    //         ctx.$root.scrollToAnchor('PEERREVIEW');
    //       },
    //       label: () => 'Zu den Gutachten',
    //     },
    //   ],
    // },
    // {
    //   id: 3,
    //   condition: (ctx) => ctx.milestonePEERREVIEW && !ctx.milestonePROPOSE,
    //   body: () => 'Weiter unten haben wir eine Frage an Sie.',
    //   buttons: [
    //     {
    //       action: (ctx) => {
    //         ctx.$root.scrollToAnchor('PROPOSE');
    //       },
    //       label: () => 'Ich komme mit!',
    //     },
    //   ],
    // },
    // {
    //   id: 3,
    //   condition: (ctx) => ctx.milestonePROPOSE && !ctx.milestoneCHARTS,
    //   body: () => 'Weiter unten haben wir eine Frage an Sie.',
    //   // buttons: [
    //   //   {
    //   //     action: (ctx) => { ctx.$root.scrollToAnchor('CHARTS') },
    //   //     label: (ctx) => 'Zur Statistik.'
    //   //   }
    //   // ]
    // },
    // {
    //   id: 4,
    //   prosa: 'finish with this topic => other stage is still open..',
    //   condition: (ctx) =>
    //     ctx.milestoneCHARTS &&
    //     ctx.milestonePROPOSE &&
    //     ctx.next_scheduled_stage &&
    //     ctx.VAAQuestionStagesAlerted.length,
    //   body: () =>
    //     `Zum Thema '${ctx.VAAQuestionStageRandomFocusTopic.content.title}' haben wir gerade keine Frage. Wenn Sie mögen können wir zum nächten Thema wechseln.`,
    //   buttons: [
    //     {
    //       action: (ctx) => ctx.gotoStage(ctx.next_scheduled_stage),
    //       label: () => 'Zum nächsten Thema',
    //     },
    //   ],
    // },
    // {
    //   id: 4,
    //   prosa: "finish with all topics..",
    //   condition: (ctx) => ctx.next_scheduled_stage && !ctx.VAAQuestionStagesAlerted.length,
    //   body: (ctx) => `Sehr schön. Kommen Sie doch mit. Jetzt muss ich Ihnen was zeigen.`,
    // },

    // {
    //   id: 5,
    //   prosa: "finish with everything",
    //   condition: (ctx) => ctx.milestonePROPOSE && ctx.next_scheduled_stage &&  ctx.VAAQuestionStagesAlerted.length,
    //   body: (ctx) => `Sie haben das Thema '${ctx.VAAQuestionStageRandomFocusTopic.content.title}' ausgewählt. Nun können Sie das Thema nach belieben wechseln.`
    // }
    // ],
  },

  // // TODO: only when there are less than five questions (incl. proposal) and when user has free-proposals left...
  // questions_peerreview: {
  //   id: 'questions_peerreview',
  //   loading: (ctx) => !ctx.routedStageTuple,
  //   items: [
  //     {
  //       id: 1,
  //       condition: (ctx) =>
  //         !ctx.milestonePEERREVIEW &&
  //         Object.keys(ctx.contentsToPeerreview).length == 1,
  //       body: () =>
  //         'Das ist uns besonders wichtig: Wir brauchen Ihr Urteil bei einem Gutachten.',
  //     },
  //     {
  //       id: 2,
  //       condition: (ctx) =>
  //         !ctx.milestonePEERREVIEW &&
  //         Object.keys(ctx.contentsToPeerreview).length > 1,
  //       body: (ctx) =>
  //         `Das ist uns besonders wichtig: Nun brauchen wir Ihr Urteil bei ${
  //           Object.keys(ctx.contentsToPeerreview).length
  //         }  Gutachten.`,
  //     },
  //   ],
  // },

  // // TODO: only when there are less than five questions (incl. proposal) and when user has free-proposals left...
  propose_argument: {
    id: 'propose_argument',
    condition: (ctx) =>
      !ctx.isShownProposeForm && (ctx.encourageToAdd || ctx.encourageToImprove),
    items: [
      {
        id: 11,
        condition: (ctx) => ctx.encourageToAdd && ctx.side === 'CONTRA',
        body: () => 'Haben Sie einen Vorschlag für ein neues Gegen-Argument?',
      },
      {
        id: 12,
        condition: (ctx) => ctx.encourageToAdd && ctx.side === 'PRO',
        body: () => 'Haben Sie einen Vorschlag für ein neues Pro-Argument?',
      },
      {
        id: 13,
        condition: (ctx) => ctx.encourageToImprove,
        body: () =>
          'Haben Sie einen Vorschlag, wie eines der obigen Argumente verbessert werden kann? Wir wären sehr froh über Ihre Mithilfe!',
      },
    ],
    buttons: [
      {
        condition: (ctx) => ctx.encourageToAdd,
        action: (ctx) => {
          ctx.toggleProposeForm(true);
        },
        label: () => 'Ja, ich weiss da etwas.',
      },
      {
        action: (ctx) => {
          ctx.toggleProposeForm(false);
          // ctx.scrollToAnchor(ctx.firstSide ? 'SECOND' : 'FINAL', 300, 300);
        },
        label: () => 'Nein, gerade nicht.',
      },
    ],
  },

  // TODO: only when there are less than five questions (incl. proposal) and when user has free-proposals left...
  arguments_peerreview: {
    id: 'arguments_peerreview',
    loading: (ctx) => !ctx.routedStageTuple && !ctx.peerreviews,
    items: [
      {
        id: 1,
        condition: (ctx) => ctx.peerreviews?.length == 1,
        body: () =>
          'Das ist uns besonders wichtig: Wir brauchen Ihr Urteil bei einem Gutachten.',
      },
      {
        id: 2,
        condition: (ctx) => ctx.peerreviews?.length > 1,
        body: (ctx) =>
          `Das ist uns besonders wichtig: Nun brauchen wir Ihr Urteil bei ${ctx.peerreviews.length}  Gutachten.`,
      },
    ],
  },

  // questions_after_charts: {
  //   id: 'topics_after_charts',
  //   loading: (ctx) => !ctx.routedStageTuple,
  //   items: [
  //     {
  //       id: 1,
  //       body: (ctx) =>
  //         ctx.daySessions <= 1
  //           ? 'Sind Sie mit Ihrer Priorisierung einverstanden?'
  //           : 'Sind Sie mit Ihrer Priorisierung einverstanden?',
  //       condition: (ctx) => !ctx.milestoneCHARTS,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.milestone('CHARTS', 4);
  //             ctx.$root.scrollToAnchor('FINAL', 300, 300);
  //           },
  //           label: () => 'Ja, es stimmt so!',
  //         },
  //         {
  //           action: (ctx) => {
  //             ctx.$root.scrollToAnchor('SALIENCE');
  //           },
  //           label: () => 'Ich möchte noch etwas ändern.',
  //         },
  //       ],
  //     },
  //     {
  //       id: 2,
  //       body: () =>
  //         'Sie sehen in diesem Bild Ihre persönliche Prioritätenliste der smartvote-Fragen. Sind Sie zufrieden damit?',
  //       condition: (ctx) => ctx.milestoneSALIENCE && !ctx.milestoneCHARTS,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.milestone('CHARTS', 4);
  //             ctx.$root.scrollToAnchor('FINAL', 300, 300);
  //           },
  //           label: () => 'Ja, es stimmt so!',
  //         },
  //         {
  //           action: (ctx) => {
  //             ctx.$root.scrollToAnchor('SALIENCE');
  //           },
  //           label: () => 'Ich möchte noch etwas ändern.',
  //         },
  //       ],
  //     },
  //   ],
  // },

  // questions_end_of_page: {
  //   id: 'topics_end_of_page',
  //   // condition: (ctx) => ctx.next_scheduled_stage,
  //   loading: (ctx) => !ctx.routedStageTuple,
  //   items: [
  //     {
  //       id: 1,
  //       prosa: 'Fortfahren mit zweiter Fragen-Stage.',
  //       body: () => [
  //         'Vielen Dank! Wir möchten nun, dass Sie sich zum Schluss noch ein zweites Thema ansehen.',
  //         'Das zweite Thema können Sie frei wählen.',
  //       ],
  //       condition: (ctx) =>
  //         ctx.next_scheduled_stage &&
  //         ctx.isRandomFocusStage &&
  //         ctx.VAAQuestionStagesAlerted.length,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.gotoStage(ctx.next_scheduled_stage);
  //           },
  //           label: () => 'Okay!',
  //         },
  //         {
  //           action: (ctx) => {
  //             ctx.monitorLog(constants.MONITOR_STAGE_OPTOUT);
  //             ctx.markUnAlert(ctx.next_scheduled_stage);
  //             ctx.gotoStage(ctx.next_scheduled_stage);
  //           },
  //           label: () => 'Ich habe wirklich keine Lust mehr.',
  //         },
  //       ],
  //     },
  //     {
  //       id: 2,
  //       body: () => [
  //         'Ich verstehe gut, dass Sie müde sind und die smartvote-Fragen zu diesem Thema grad nicht mehr bewerten wollen. Schauen Sie doch noch kurz beim Zwischenstand vorbei?',
  //       ],
  //       condition: (ctx) =>
  //         ctx.next_scheduled_stage &&
  //         !ctx.VAAQuestionStagesAlerted.length &&
  //         !ctx.milestoneSALIENCE,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.milestone('CHARTS', 4);
  //             ctx.$root.scrollToAnchor('SALIENCE');
  //           },
  //           label: () => 'Zur Bewertung der Fragen.',
  //         },
  //         {
  //           action: (ctx) => {
  //             ctx.milestone('CHARTS', 4);
  //             ctx.gotoStage(ctx.next_scheduled_stage);
  //           },
  //           label: () => 'Zum Zwischenstand',
  //         },
  //       ],
  //     },
  //     {
  //       id: 199,
  //       body: () => [
  //         'Haben Sie noch Lust, Gutachten zu diesem Thema zu beantworten? Es ist uns ein Anliegen, dass möglichst viele Personen dabei mithelfen.',
  //       ],
  //       condition: (ctx) =>
  //         !ctx.VAAQuestionStagesAlerted.length &&
  //         ctx.milestoneSALIENCE &&
  //         !ctx.peerreviewCompleted,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.$root.scrollToAnchor('PEERREVIEW');
  //           },
  //           label: () => 'Zu den Gutachten.',
  //         },
  //         {
  //           action: (ctx) => ctx.$root.scrollToAnchor('TOPICSELECTION'),
  //           label: () => 'Nein, danke. Es reicht jetzt wirklich!',
  //         },
  //       ],
  //     },
  //     {
  //       id: 200,
  //       body: () => [
  //         "Wunderbar! Sie können sich nun noch die weiteren Themen anschauen oder direkt zum 'Zwischenstand' wechseln.",
  //       ],
  //       condition: (ctx) =>
  //         ctx.next_scheduled_stage &&
  //         !ctx.VAAQuestionStagesAlerted.length &&
  //         ctx.milestoneSALIENCE &&
  //         ctx.peerreviewCompleted,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.$root.scrollToAnchor('TOPICSELECTION');
  //           },
  //           label: () => 'Erneut zur Themenwahl',
  //         },
  //         {
  //           action: (ctx) => {
  //             ctx.gotoStage(ctx.next_scheduled_stage);
  //           },
  //           label: () => 'Zum Zwischenstand',
  //         },
  //       ],
  //     },
  //     {
  //       id: 201,
  //       body: () => ['Hier geht es wieder nach oben zur Themenwahl'],
  //       condition: (ctx) =>
  //         !ctx.next_scheduled_stage &&
  //         ctx.milestoneSALIENCE &&
  //         ctx.peerreviewCompleted,
  //       buttons: [
  //         {
  //           action: (ctx) => {
  //             ctx.$root.scrollToAnchor('TOPICSELECTION');
  //           },
  //           label: () => 'Erneut zur Themenwahl',
  //         },
  //       ],
  //     },
  //   ],
  // },
} as IArtificialModerationFile;
