/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import { computed } from 'vue';
import { INodeTuple } from 'src/models/content';
import { IAssemblyUserTuple } from 'src/models/assembly';
import { getStageService } from 'src/services';
import useContenttreeComposable from 'src/composables/contenttree.composable';
import { useStore } from 'vuex';
import { getAssemblyService } from 'src/services';
const { pushSorted } = library;
import library from 'src/utils/library';

function compare_rating(a: INodeTuple, b: INodeTuple) {
  if (a?.progression?.rating === null || a?.progression?.rating === undefined) {
    throw 'no rating available (a)';
  }
  if (b?.progression?.rating === null || b?.progression?.rating === undefined) {
    throw 'no rating available (b)';
  }

  if (a.progression.rating < b.progression.rating) {
    return 1;
  }
  if (a.progression.rating > b.progression.rating) {
    return -1;
  }

  return 0;
}

function compare_rating_community(a: INodeTuple, b: INodeTuple) {
  if (a?.content?.S?.RA === null || a?.content?.S?.RA === undefined) {
    throw 'no rating available (a)';
  }
  if (b?.content?.S?.RA === null || b?.content?.S?.RA === undefined) {
    throw 'no rating available (b)';
  }

  if (a.content.S.RA < b.content.S.RA) {
    return 1;
  }
  if (a.content.S.RA > b.content.S.RA) {
    return -1;
  }

  return 0;
}

export default function useArgumentsComposable(
  userTuple: IAssemblyUserTuple | undefined = undefined,
  addWatcher = false
) {
  // console.log(userTuple);
  const {
    routedStageTuple,
    nextScheduledStage,
    markUnAlert,
    isRoutedStageAlerted,
  } = getStageService();
  const { assembly, gotoStage, gotoNextStageNr } = getAssemblyService();

  const store = useStore();

  const contenttreeID = computed((): number | null => {
    // console.log('get PROS_AND_CONS_CONTENTTREE_ID');
    if (assembly.value?.custom_data.PROS_AND_CONS_CONTENTTREE_ID) {
      return assembly.value.custom_data.PROS_AND_CONS_CONTENTTREE_ID;
    }
    return null;
  });

  const contenttreeIdentifier = computed((): string | null => {
    if (!contenttreeID.value) {
      return null;
    }
    return `${contenttreeID.value}`;
  });

  const firstSide = computed((): 'PRO' | 'CONTRA' | null => {
    const assemblyProgression =
      store.getters['assemblystore/assemblyProgression'];
    if (!assemblyProgression) {
      return null;
    }
    const day = assemblyProgression.number_of_day_sessions;
    const gettrandomLocalStorageSeed =
      store.getters['profilestore/randomLocalStorageSeed'];
    if (day === null || day === undefined) {
      throw 'error while reading day session';
    }
    if (
      gettrandomLocalStorageSeed === null ||
      gettrandomLocalStorageSeed === undefined
    ) {
      throw 'error while reading localStorage Seed';
    }

    const rand = (gettrandomLocalStorageSeed + day) % 2;
    if (rand === 1) {
      return 'PRO';
    }

    return 'CONTRA';
  });

  const secondSide = computed((): 'PRO' | 'CONTRA' | null => {
    if (firstSide.value === 'PRO') {
      return 'CONTRA';
    }

    if (firstSide.value === 'CONTRA') {
      return 'PRO';
    }
    return null;
  });

  // const firstSide: Ref<'PRO' | 'CONTRA'> = whichSideFirst();
  const { getDescendantsOf, contenttreeTuple } = useContenttreeComposable(
    contenttreeIdentifier,
    addWatcher
  );

  // Retieve all arguments (either as param of the transmitted IAsemblyUserTuple or from local copy of the contenttree)
  const allArguments = computed((): INodeTuple[] | undefined => {
    if (userTuple?.arguments) {
      return userTuple.arguments;
    }
    // TODO: random ORDER!!!!
    if (!contenttreeTuple.value?.entries) {
      return undefined;
    }

    // FILTER
    const all_args = Object.values(contenttreeTuple.value.entries).filter(
      (arg) => arg?.content?.parent_id === null
    );
    const sorted_args = [];
    all_args.forEach((arg) => {
      pushSorted(sorted_args, arg);
    });

    return sorted_args;
  });

  const mainArgumentsPro = computed((): INodeTuple[] | undefined => {
    if (!allArguments.value) {
      return undefined;
    }

    const pros = allArguments.value.filter(
      (pro) => pro?.content?.parent_id === null && pro?.content.type === 'PRO'
    );
    return pros;
  });

  const mainArgumentsContra = computed((): INodeTuple[] | undefined => {
    if (!allArguments.value) {
      return undefined;
    }

    const cons = allArguments.value.filter(
      (con) =>
        con?.content?.parent_id === null && con?.content.type === 'CONTRA'
    );

    return cons;
  });

  const emptyArguments = computed(() => {
    return allArguments.value && !allArguments.value.length;
  });

  const argumentsBySide = computed(
    (): Record<'PRO' | 'CONTRA', INodeTuple[]> | undefined => {
      if (!mainArgumentsContra.value) {
        return undefined;
      }
      if (!mainArgumentsPro.value) {
        return undefined;
      }

      return {
        PRO: mainArgumentsPro.value,
        CONTRA: mainArgumentsContra.value,
      };
    }
  );

  const top3ArgumentsBySide = computed(
    (): Record<'PRO' | 'CONTRA', INodeTuple[]> | undefined => {
      if (!mainArgumentsContra.value) {
        return undefined;
      }
      if (!mainArgumentsPro.value) {
        return undefined;
      }

      let top3Pro: INodeTuple[] = [...mainArgumentsPro.value];
      let top3Contra: INodeTuple[] = [...mainArgumentsContra.value];

      // only rated arguments
      top3Pro = top3Pro.filter(
        (a) => !!a.progression?.rating && a.progression?.rating > 10
      );
      top3Contra = top3Contra.filter(
        (a) => !!a.progression?.rating && a.progression?.rating > 10
      );

      // Order by ranking..and limit to 3....
      top3Pro.sort(compare_rating);
      top3Contra.sort(compare_rating);

      // limit to 3
      top3Pro = top3Pro.slice(0, 3);
      top3Contra = top3Contra.slice(0, 3);

      return {
        PRO: top3Pro,
        CONTRA: top3Contra,
      };
    }
  );

  const top3ArgumentsBySideForCommunity = computed(
    (): Record<'PRO' | 'CONTRA', INodeTuple[]> | undefined => {
      if (!mainArgumentsContra.value) {
        return undefined;
      }
      if (!mainArgumentsPro.value) {
        return undefined;
      }

      let top3Pro: INodeTuple[] = [...mainArgumentsPro.value];
      let top3Contra: INodeTuple[] = [...mainArgumentsContra.value];

      // only rated arguments
      top3Pro = top3Pro.filter(
        (a) => !!a.content?.S?.RA && a.content.S.RA > 10
      );
      top3Contra = top3Contra.filter(
        (a) => !!a.content?.S?.RA && a.content.S.RA > 10
      );

      // Order by ranking..and limit to 3....
      top3Pro.sort(compare_rating_community);
      top3Contra.sort(compare_rating_community);

      // limit to 3
      top3Pro = top3Pro.slice(0, 3);
      top3Contra = top3Contra.slice(0, 3);

      return {
        PRO: top3Pro,
        CONTRA: top3Contra,
      };
    }
  );

  return {
    argumentsBySide,
    top3ArgumentsBySideForCommunity,
    top3ArgumentsBySide,
    assembly,
    getDescendantsOf,
    contenttreeID,
    contenttreeIdentifier,
    firstSide,
    secondSide,
    routedStageTuple,
    nextScheduledStage,
    isRoutedStageAlerted,
    markUnAlert,
    gotoStage,
    gotoNextStageNr,
    contenttreeTuple,
    emptyArguments,
  };
}
