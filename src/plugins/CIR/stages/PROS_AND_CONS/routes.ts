const meta4AssemblyPages = { topmenu: 'assemblies_ongoing_list' };
import { RouteRecordRaw } from 'vue-router';
const routes: RouteRecordRaw[] = [
  {
    // TODO: do we need contenttreeid in path?
    path: '/:assemblyIdentifier/:assemblyType/argumentarium/:stageID', // /:contenttreeID/',
    name: 'PROS_AND_CONS',
    component: () =>
      import('/src/plugins/CIR/stages/PROS_AND_CONS/Argumentarium.vue'),
    meta: meta4AssemblyPages,
  },
  {
    path: '/:assemblyIdentifier/argumentarium/:stageID/:contenttreeID/',
    name: 'PROS_AND_CONS_DEFAULT_LAYOUT',
    component: () => import('/src/plugins/CIR/stages/PROS_AND_CONS/Index.vue'),
    meta: meta4AssemblyPages,
  },
];

export default routes;
