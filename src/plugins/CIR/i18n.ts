// import { i18n } from 'src/boot/i18n';
export default {
  'de-de': {},
  'de-ch': {
    auth: {
      authentication_warning_body:
        'Sie sind aktuell nicht bei der Demokratiefabrik angemeldet. Für den Zugriff auf diesen Bereich ist dies jedoch erforderlich. Bitte verwenden Sie den Login-Link, der Ihnen zugesandt wurde.',
    },
    common_properties: {
      insert: {
        criteria_accept: {
          '1': 'Das vorgeschlagene Argument ist – soviel ich weiss – sachlich korrekt.', // {topic}
          '2': 'Der Beitrag beschreibt ein {typeLabel} der Vorlage.',
          '3': 'Das vorgeschlagene Argument fokussiert auf einen einzigen Sachverhalt. (Es ist keine Sammlung mehrerer verschiedener Argumente oder Überlegungen).',
          '4': 'Das vorgeschlagene Argument ist – so oder ähnlich – noch nicht vorhanden. (Sehen Sie sich dazu bitte die bereits vorhandenen {typeLabel}e an).',
          '5': 'Das vorgeschlagene Argument ist verständlich formuliert und beinhaltet ausschliesslich Ausführungen, welche für die Argumentation notwendig sind. ',
        },
      },
      update: {
        criteria_accept: {
          '1': 'Das vorgeschlagene Argument ist – soviel ich weiss – sachlich korrekt.', // {topic}
          '2': 'Die ursprüngliche Idee des Beitrags bleibt erhalten (oder wird präzisiert).',
          '3': 'Der Beitrag fokussiert auf einen einzigen Sachverhalt. (Er ist keine Sammlung mehrerer verschiedener Argumente oder Überlegungen).',
          '4': 'Die Änderung stellt eine Verbesserung des Beitrags dar.',
          '5': 'Der Beitrag ist verständlich formuliert und beinhaltet ausschliesslich Ausführungen, welche für die Argumentation notwendig sind.',
          // '6': 'Der verbesserte Beitrag ist nicht zu lang. Er beinhaltet ausschliesslich Ausführungen, welche für die Argumentation notwendig sind.',
        },
      },

      contribution_text_hint: '',
      contribution_title_hint: '',
      contribution_title_label: 'Einen kurzen Titel',
      contribution_criteria_html: `<p class="text-caption">
        Sie finden nachfolgend einige wichtige Hinweise zur Formulierung der Argumente.
        </p>
        <ol>
          <li>
          Formulieren Sie bitte nur Argumente, die einen <b>Bezug zur Abstimmungsvorlage</b> aufweisen. 
          </li>
          <li>
          Halten Sie ihr Argument also <b>möglichst kurz</b>. Bitte beschränken Sie sich auf die Ausführungen, die für Ihr Argument notwendig sind.
          </li>
          <li>
          Formulieren Sie <b>pro Beitrag</b> bitte nur <b>ein Argument</b>.
          </li>
          <li>
          Kontrollieren Sie bitte, dass dieses Argument <b>nicht bereits 
          in ähnlicher Weise</b> vorliegt.
          </li>
          <li>
          Formulieren Sie bitte an dieser Stelle <b>nur  {thisSideLabel} Argumente</b>, die {otherSideLabel} Argumente werden an einer anderen Stelle diskutiert.
          </li>
        </ol>`,
    },

    contenttree: {
      types: {
        COMMENT: 'Kommentar',
        INVITED_COMMENT: 'Kommentar*',
        QUESTION: 'Frage',
        ANSWER: 'Antwort',
        PARAGRAPH: 'Absatz',
        FOLDER: 'Ordner ',
        SECTION: 'Kapitel',
        SUBSECTION: 'Unterkapitel',
        CIR_TOPIC: 'Abstimmungsvorlage',
        PRO: 'Pro-Argument',
        CONTRA: 'Contra-Argument',
      },
    },
  },
};
