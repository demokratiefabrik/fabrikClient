# Assembly Type Plugins

These plugins handle...

- routes
- assembly menu
- i18n / translations
- artificial-moderators / AM instructions...

The main idea is that all stage-plugins can be used by distinct assembly types. (and are customizable due to this assembly plugins)
