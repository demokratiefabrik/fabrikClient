import mitt from 'mitt';
import { IAccessTokenPartial, IPayload } from './pkce.composable';

type Events = {
  PKCEAuthenticationError;
  AfterLogin: IPayload;
  RecycleLogin: IPayload;
  AccessTokenChanges: IAccessTokenPartial | null;
  AfterLogout;
  showAuthorizationInvalidToken;
};

//   oauthEmitter.emit('AccessTokenChanged', null);
//   if (!silent) {
//     oauthEmitter.emit('AfterLogout');

const emitter = mitt<Events>();

export default function useOAuthEmitter() {
  return emitter;
}
