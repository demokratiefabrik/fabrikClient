import mitt from 'mitt';
import { IDialog, INotificationBanner } from 'src/models/layout';
import { IStageTuple } from 'src/models/stage';

type Events = {
  newDaySession: number;
  loadingGifStackChange: any;
  EventStageLoaded: IStageTuple;
  notificationBannerChange?: INotificationBanner;
  notificationAction: any;
  receiveBackendFeedback: any;
  showDialog: IDialog;
  routeChange: any; // TODO ROUTE OBJECT
  // newUserProfileLoaded: undefined;
  userProfileIsInSync: undefined;
  showTooManyRequestsError: undefined;
  showNetworkError: undefined;
  showAuthorizationError: undefined;
  showAuthenticationWarning: undefined;
  showServiceError: undefined | { nobuttons: boolean };
  AssemblyLoaded: undefined; // assembly is loaded
  hideLoading: undefined; // hide loading elements in layout...
  showLoading: undefined; // show loading elements in layout...
  reload: undefined; // reload page (TODO: implement)...
};

const emitter = mitt<Events>();

export default function useEmitter() {
  return emitter;
}
