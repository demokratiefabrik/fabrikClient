/** DEMOKRATIFABRIK standalone Method Libraryies */

import { scroll } from 'quasar';
const { setVerticalScrollPosition } = scroll;
// import { dom } from 'quasar';
// const { offset } = dom;

// export default function useLibraryComposable() {
// export default library {

/* Add Object filter: helper...
  * <var>.filter only works for lists but not for objects...
  * Returns length of a object/list, while handling null as 0. 
    TODO: same as object?.length rigth?
  */
const filter = (obj, predicate) =>
  Object.keys(obj)
    .filter((key) => predicate(obj[key]))
    .reduce((res, key) => ((res[key] = obj[key]), res), {});

const removeItem = (arr: any[], value: any): any[] => {
  return arr.filter(function (ele) {
    return ele != value;
  });
};

const nLength = (object1) => {
  if (object1 === null) {
    return 0;
  }
  return object1.length;
};

const loaded = (object1) => {
  return object1 !== null && object1 !== undefined;
};

// randomly choose an entry from array...
const sample = (array) => {
  return array[Math.floor(Math.random() * array.length)];
};

/* Push sorted content array */
const pushSorted = (children, toAdd): any[] => {
  if (!toAdd.content.order_position) {
    children.push(toAdd);
    return children;
  }
  function getIndex(children, toAdd) {
    const idx = children.findIndex(
      (x) => x.content.order_position > toAdd.content.order_position
    );
    return idx === -1 ? children.length : idx;
  }

  const ix = getIndex(children, toAdd);
  children.splice(ix, 0, toAdd);
  return children;
};

const getRandomInt = (max) => {
  return Math.floor(Math.random() * max);
};

const getOffsetTop = (
  element: HTMLElement,
  container: null | HTMLElement = null
) => {
  let offsetTop = 0;
  let el: HTMLElement | null = element;
  while (el) {
    offsetTop += el.offsetTop;
    if (el.offsetParent) {
      el = el.offsetParent as HTMLElement;
      if (container && (container === el || container.offsetParent === el)) {
        el = null;
      }
    } else {
      el = null;
    }
  }
  return offsetTop;
};

const timestamp = (): number => {
  const ts = Date.now();
  return Math.round(ts / 1000);
};

const scrollToAnchor = (anchor, headerOffset, duration = 300, lag = 0) => {
  const dom = document.getElementsByName(anchor);
  const ele = dom?.item(0);
  // console.log('dddd', ele, dom, getOffsetTop(ele), headerOffset);
  const scrollFnc = () => {
    const elOffset = getOffsetTop(ele) - headerOffset;
    // console.log(elOffset, 'll');
    setVerticalScrollPosition(window, elOffset, duration);
  };
  if (lag) {
    setTimeout(scrollFnc, lag);
  } else {
    scrollFnc();
  }
};

const scrollToTop = (duration = 300, lag = 0) => {
  const scrollFnc = () => {
    setVerticalScrollPosition(window, 0, duration);
  };
  if (lag) {
    setTimeout(scrollFnc, lag);
  } else {
    scrollFnc();
  }
};

const addArrayItemAtIndex = (array, index, newItem) => {
  return [...array.slice(0, index), newItem, ...array.slice(index)];
};

export default {
  timestamp,
  // scrollToAnchorIfNecessary,
  scrollToAnchor,
  scrollToTop,
  removeItem,
  getOffsetTop,
  pushSorted,
  sample,
  loaded,
  getRandomInt,
  nLength,
  filter,
  addArrayItemAtIndex,
};
