/** DEMOKRATIFABRIK RUNTIME VARIABLES */
import { RouteLocationRaw } from 'vue-router';
import useOAuthEmitter from 'src/utils/VueOAuth2PKCE/oauthEmitter';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
import { computed, shallowRef, ref, Ref, watch } from 'vue';
import { useStore } from 'vuex';
import useRouterService from './router.service';
import useEmitter from 'src/utils/emitter';
import library from 'src/utils/library';
import { IStage, IStageGroup, IStageTuple } from 'src/models/stage';
import { IContributionLimits } from 'src/models/layout';
import useStoreService from './store.service';
import { IAssembly } from 'src/models/assembly';
import { useI18n } from 'vue-i18n';
import { getCurrentRoute, getStore } from 'src/services';
// import { IArtificialModeration } from 'src/pages/components/artificial_moderation/model';
// import comp from 'src/plugins/CIR/components/IndexFooter.vue';

export default function useAssemblyService(caller = '') {
  // caller as first param
  void caller;

  const store = useStore();

  const oauthEmitter = useOAuthEmitter();
  const { userid } = usePKCEComposable();
  const emitter = useEmitter();
  const { loaded } = library;
  const assemblyMenuData = ref<Record<string, IStageGroup> | null>(null);
  const tocFooterComponent = shallowRef<null | any>(null);
  const assemblyMenuAMs = shallowRef<null | any>(null);

  // import AMs from 'src/pages/Assembly/ArtificialModeration';

  const showIndexInAssemblyMenu = ref<boolean>(false);
  const { assemblyIdentifier, stageID } = useStoreService();
  const { pushR, clearSession } = useRouterService();

  // TODO: instead use a watcher, (when on routes' assembly_identifier ...)
  const initializePlugin = (
    localAssemblyType: string,
    localAssemblyMenuData: Record<string, IStageGroup>,
    localI18n: any,
    _showIndexInAssemblyMenu: boolean,
    localTocFooterComponent: Ref<any> | null = null,
    localAssemblyMenuAMs: Ref<any | null> | null = null
  ) => {
    const { mergeLocaleMessage, locale } = useI18n();
    // console.log('DEBUGII', localAssemblyMenuAMs);
    assemblyMenuData.value = localAssemblyMenuData;
    // assemblyMenuAMs.value = localAssemblyMenuAMs;
    showIndexInAssemblyMenu.value = _showIndexInAssemblyMenu;
    // tocFooterComponent.value = localTocFooterComponentPath;

    if (localTocFooterComponent) {
      watch(
        () => localTocFooterComponent.value,
        () => {
          if (localTocFooterComponent.value) {
            tocFooterComponent.value = localTocFooterComponent.value;
          }
        }
      );
    }

    if (localAssemblyMenuAMs) {
      watch(
        () => localAssemblyMenuAMs.value,
        () => {
          if (localAssemblyMenuAMs.value) {
            assemblyMenuAMs.value = localAssemblyMenuAMs.value;
          }
        }
      );
    }

    // /// Local I18n
    if (localI18n) {
      // TODO: add watcher on language change... (However, only nice to have)
      if (!localI18n[locale.value.toLowerCase()]) {
        throw `language ${locale.value} is not available for this assembly module`;
      }
      mergeLocaleMessage(
        locale.value.toLowerCase(),
        localI18n[locale.value.toLowerCase()]
      );
    }
  };

  // const setStageIDByStageNr = (stageNr) => {
  //   if (stageNr === null || stageNr === undefined) {
  //     setStageID(null);
  //   } else {
  //     if (!assembly_sorted_stages.value) {
  //       return undefined;
  //     }
  //     const stageID = assembly_sorted_stages.value[stageNr as number].stage.id;
  //     setStageID(stageID);
  //   }
  // };

  const syncPublicAssembly = () => {
    const store = getStore();
    store.dispatch('publicindexstore/syncPublicIndex');
  };
  const syncUserAssembly = () => {
    // sync public assembly...
    const store = getStore();
    if (assemblyIdentifier.value && userid.value) {
      store.dispatch('assemblystore/syncAssembly', {
        oauthUserID: userid.value,
        assemblyIdentifier: assemblyIdentifier.value,
      });
    }
  };

  const setSyncIntervall = () => {
    // CREATE INTERVALL TO KEEP IN SYNC ASSEMBLY
    const intervallString: string = process.env
      .ENV_APISERVER_MONITOR_INTERVAL_SECONDS
      ? process.env.ENV_APISERVER_MONITOR_INTERVAL_SECONDS
      : '60';
    const intervall = (parseInt(intervallString) + 5) * 1000;
    // console.log(intervall);
    setInterval(() => {
      // INTERVALL SYNC
      syncPublicAssembly();
      syncUserAssembly();
    }, intervall * 1000);
  };

  const getStageRoute = (stage: IStageTuple): RouteLocationRaw => {
    console.assert(stage);
    const store = getStore();
    const assembly = computed(
      (): IAssembly => store.getters['assemblystore/assembly']
    );
    const params = {
      assemblyIdentifier: assemblyIdentifier.value,
      assemblyType: assembly.value.type,
      stageID: stage.stage.id,
    };
    return {
      name: `${stage.stage.type}__${assembly.value.type}`,
      params,
    };
  };

  const gotoStage = (stage): void => {
    console.assert(stage);
    pushR(getStageRoute(stage));
  };

  const gotoNextStageNr = (stage): void => {
    console.assert(stage);

    const store = getStore();
    const currentStageGroup = stage.stage.group;
    const find_next_accessible_stage =
      store.getters['assemblystore/find_next_accessible_stage'];
    // const get_stage_number_by_stage =
    //   store.getters['assemblystore/get_stage_number_by_stage'];

    const nextStage = find_next_accessible_stage(stage);
    if (!nextStage) {
      return;
    }
    const nextStageGroup = stage.stage.group;
    if (nextStageGroup !== currentStageGroup) {
      // different group: so make a new route...
      pushR(`${nextStage.stage.id}/${nextStage.stage.group}`);
    } else {
      gotoStage(nextStage);
    }
  };

  // TODO: what is that for?
  const gotoDefaultStageTeaser = (): void => {
    // console.log('goto default stage teaser');
    const store = getStore();
    const get_stage_number_by_stage =
      store.getters['assemblystore/get_stage_number_by_stage'];
    const get_stage_number_by_stage_id =
      store.getters['assemblystore/get_stage_number_by_stage_id'];
    const last_accessible_stage = computed(
      (): IStage => store.getters['assemblystore/last_accessible_stage']
    );
    if (stageID.value !== null && stageID.value !== undefined) {
      get_stage_number_by_stage(get_stage_number_by_stage_id(stageID.value));
    } else if (last_accessible_stage.value) {
      get_stage_number_by_stage(last_accessible_stage.value);
    } else {
      get_stage_number_by_stage(null);
    }
  };

  const getDailyContributionLimits = (): IContributionLimits => {
    const store = getStore();
    const progression = store.getters['assemblystore/assemblyProgression'];
    const configuration = store.getters['assemblystore/assemblyConfiguration'];

    if (!configuration) {
      // TODO: make sure to route away before logouting. (progression will be empty after logout)
      // throw 'assembly is not configured, correctly';
      syncPublicAssembly();
      return {
        number_of_proposals: {
          daylimit: 0,
          overalllimit: 0,
          overallCurrent: 0,
          current: 0,
        },
        number_of_comments: {
          daylimit: 0,
          current: 0,
        },
      };
    }
    if (!progression) {
      // TODO: make sure to route away before logouting. (progression will be empty after logout)
      throw 'assembly is not configured, correctly II';
    }

    return {
      number_of_proposals: {
        daylimit: configuration.MAX_DAILY_USER_PROPOSALS,
        overalllimit: configuration.MAX_OVERALL_USER_PROPOSALS,
        overallCurrent:
          progression.number_of_proposals === null
            ? 0
            : progression.number_of_proposals,
        current:
          progression.number_of_proposals_today === null
            ? 0
            : progression.number_of_proposals_today,
      },
      number_of_comments: {
        daylimit: configuration.MAX_DAILY_USER_COMMENTS,
        current:
          progression.number_of_comments_today === null
            ? 0
            : progression.number_of_comments_today,
      },
    };
  };

  const initialize = () => {
    oauthEmitter.on('AfterLogin', () => {
      clearSession();
      syncUserAssembly();
      setSyncIntervall();
    });
    oauthEmitter.on('RecycleLogin', () => {
      syncUserAssembly();
      setSyncIntervall();
    });

    syncPublicAssembly();
    syncUserAssembly();
  };

  // const get_stage_number_by_stage_id =
  //   store.getters['assemblystore/get_stage_number_by_stage_id'];

  const assemblyStages = computed(
    (): Record<number, IStageTuple> =>
      store.getters['assemblystore/assemblyStages']
  );
  const IsManager = computed((): boolean => {
    return store.getters['assemblystore/IsManager'];
  });
  const assembly_sorted_stages = computed(
    (): IStageTuple[] | null =>
      store.getters['assemblystore/assembly_sorted_stages']
  );
  const assembly = computed((): IAssembly | null => {
    // public users only look in public index
    const assemblyAcls = store.getters['assemblystore/assemblyAcls'];
    // console.log(assemblyAcls, 'll');
    if (assemblyAcls && !assemblyAcls?.length) {
      const { assemblyIdentifier } = useStoreService();
      const published = store.getters['publicindexstore/published_assemblies'];
      if (published) {
        const publicAssembly = published.find(
          (x) => x.identifier == assemblyIdentifier.value
        );
        return publicAssembly;
      }
    }

    return store.getters['assemblystore/assembly'];
  });
  const showAssemblyMenu = computed((): boolean => {
    const currentRoute = getCurrentRoute();
    const assembly = store.getters['assemblystore/assembly'];
    return !!assembly && !currentRoute.meta.hideAssemblyMenu;
  });

  const get_order_pos = (group: IStageGroup): number => {
    const tuple: IStageTuple | undefined = assembly_sorted_stages.value?.find(
      (x) => x.stage.group === group.name
    );
    if (!assembly_sorted_stages.value || !tuple) {
      return 0;
    }
    return tuple.stage.extended_order_position;
  };

  const sortFunc = (a: IStageGroup, b: IStageGroup) => {
    const aPos = get_order_pos(a);
    const bPos = get_order_pos(b);
    return aPos < bPos ? -1 : aPos > bPos ? 1 : 0;
  };

  // TODO: put to Vuex
  const orderedAssemblyMenuItems = computed((): IStageGroup[] => {
    if (!assemblyMenuData.value) {
      return [];
    }
    const stageGroupItems = Object.values(assemblyMenuData.value);
    // console.log(
    //   'DEBUG: orderedAssemblyMenuItems',
    //   assembly_sorted_stages?.value?.length
    // );
    if (!assembly_sorted_stages?.value?.length) {
      return [];
    }

    stageGroupItems.sort((a, b) => sortFunc(a, b));
    return stageGroupItems;
  });

  // const stage_nr_last_visited: Ref<number | null> = computed({
  //   get() {
  //     if (stageID.value === null || isNaN(stageID.value)) {
  //       return null;
  //     }
  //     const get_stage_number_by_stage_id =
  //       store.getters['assemblystore/get_stage_number_by_stage_id'];
  //     return get_stage_number_by_stage_id(stageID.value) as number;
  //   },
  //   set(stageNr: number | null) {
  //     setStageIDByStageNr(stageNr);
  //   },
  // });

  // STAGES
  // const daySessions = computed((): number => {
  //   const progression = store.getters['assemblystore/assemblyProgression'];
  //   return progression?.number_of_day_sessions;
  // });

  const ready = computed((): boolean => {
    // const assembly = store.getters['assemblystore/assembly'];
    const ready = loaded(assembly.value);
    if (ready) {
      emitter.emit('hideLoading');
    }
    return ready;
  });

  // const user_id = computed((): string | null => {
  //   return store.state.profilestore?.profile?.user?.id;
  // });
  const external_id = computed(() => store.getters['profilestore/external_id']);

  const outgoingURL = computed((): string | undefined => {
    const assembly = store.getters['assemblystore/assembly'];
    if (assembly?.custom_data?.OUTGOING_URL) {
      return assembly.custom_data?.OUTGOING_URL;
    }
    return undefined;
  });

  const redirectToOutgoingURL = () => {
    if (outgoingURL.value) {
      // user_id
      if (!external_id.value) {
        throw 'No external_id available...';
      }

      // const templ =
      //   'https://linkpanel.ch/?m=1001&return=complete&i_survey=EXTERNAL_ID';
      const templ = outgoingURL.value;
      const re = new RegExp('\\bEXTERNAL_ID\\b', 'gi');
      const url = templ.replace(re, external_id.value);
      // linkpanel.ch/?m=6006&return=complete&i_survey={EXTERNAL_ID}
      // const url = `${outgoingURL.value}&EXTERNAL=${external_id.value}&DFID=${user_id.value}`;
      window.open(url);
    }
  };

  const stage_last_visited = computed((): IStageTuple | null => {
    const get_stage_number_by_stage_id =
      store.getters['assemblystore/get_stage_number_by_stage_id'];
    const stage_nr_last_visited = get_stage_number_by_stage_id(
      stageID.value
    ) as number;
    if (stage_nr_last_visited === null) {
      return null;
    }
    return assembly_sorted_stages[stage_nr_last_visited];
  });

  const overallLimitForAddingProposalsReached = computed((): boolean | null => {
    const dailyContributionLimits = getDailyContributionLimits();
    if (!dailyContributionLimits) {
      return null;
    }
    const numberOfProposalLimits = dailyContributionLimits.number_of_proposals;
    const limitReached =
      numberOfProposalLimits.overallCurrent >=
      numberOfProposalLimits.overalllimit;
    return limitReached;
  });

  const limitForAddingProposalsReached = computed((): boolean | null => {
    const dailyAddingLimits = getDailyContributionLimits();
    if (!dailyAddingLimits) {
      return null;
    }
    const numberOfProposalLimits = dailyAddingLimits.number_of_proposals;
    const limitReached =
      numberOfProposalLimits.current >= numberOfProposalLimits.daylimit;
    return limitReached;
  });

  // TODO: put to Vuex
  const limitForAddingCommentsReached = computed((): boolean | null => {
    if (IsManager.value) {
      return false;
    }

    const dailyAddingLimits = getDailyContributionLimits();
    if (!dailyAddingLimits) {
      return null;
    }
    const numberOfCommentsLimits = dailyAddingLimits.number_of_comments;
    const limitReached =
      numberOfCommentsLimits.current >= numberOfCommentsLimits.daylimit;
    return limitReached;
  });

  return {
    assembly,
    assembly_sorted_stages,
    assemblyStages,
    ready,
    orderedAssemblyMenuItems,
    assemblyMenuAMs,
    stage_last_visited,
    overallLimitForAddingProposalsReached,
    limitForAddingProposalsReached,
    limitForAddingCommentsReached,
    syncUserAssembly,
    initialize,
    initializePlugin,
    gotoDefaultStageTeaser,
    getDailyContributionLimits,
    gotoNextStageNr,
    gotoStage,
    getStageRoute,
    showAssemblyMenu, // should assembly menu be shown?
    showIndexInAssemblyMenu,
    tocFooterComponent,
    assemblyMenuData,
    redirectToOutgoingURL,
    outgoingURL,

    // daySessions,
    IsManager,
  };
}
