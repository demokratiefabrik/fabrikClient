/** DEMOKRATIFABRIK RUNTIME VARIABLES */
import { ref, readonly, computed } from 'vue';
import Constants from 'src/utils/constants';
import usePKCEComposable, {
  IPayload,
} from 'src/utils/VueOAuth2PKCE/pkce.composable';
import { useStore } from 'vuex';
import useEmitter from 'src/utils/emitter';
import useOAuthEmitter from 'src/utils/VueOAuth2PKCE/oauthEmitter';
import useRouterService from './router.service';
import constants from 'src/utils/constants';
import { watch } from 'vue';
import { IAssemblyUser } from 'src/models/assembly';
import {
  getCurrentRoute,
  getMonitorService,
  getRouter,
  getStore,
  getT,
} from 'src/services';
// import getMonitorService from './monitor.composable';
import { useI18n } from 'vue-i18n';
import { useQuasar } from 'quasar';

export default function useAuthService() {
  const store = useStore();

  const pkce = usePKCEComposable();
  const logoutState = ref<boolean>(false);
  const _emailIsAvailable = ref<boolean>(false);
  const emitter = useEmitter();
  const oauthEmitter = useOAuthEmitter();
  const { monitorLog, monitorContextData } = getMonitorService();

  const getUsername = (profile: IAssemblyUser) =>
    profile ? profile.U : 'Anonymous';

  const getUsernameDerivation = (
    profile: IAssemblyUser,
    shortversion = false,
    thirdPerson = true
  ) => {
    const t = getT();

    if (!profile) {
      return '';
    }
    const fullname = profile.FN;
    const var1 = profile.VAR1;
    const var2 = profile.VAR2;
    const var3 = profile.VAR3;

    if (profile.ROLE === 'EXPERT') {
      if (thirdPerson) {
        return t('auth.expert_name_derivation_3rd_party', {
          fullname: fullname,
          var1,
          var2,
          var3,
        });
        // return 'Dieser User wurden als Expertin respektive Experte zu dieser Demokratiefabrik-Veranstaltung eingeladen.';
      } else {
        return t('auth.expert_name_derivation', {
          fullname: fullname,
          var1,
          var2,
          var3,
        });
      }
    }

    if (profile.ROLE === 'MANAGER') {
      if (thirdPerson) {
        return t('auth.manager_name_derivation_3rd_party', {
          fullname: fullname,
          var1,
          var2,
          var3,
        });
        // return 'Dieser User gehört zum Organsiationsteam dieser Demokratiefabrik-Veranstaltung.';
      } else {
        return t('auth.manager_name_derivation', {
          fullname: fullname,
          var1,
          var2,
          var3,
        });
        // return 'Sie gehören zum Organsiationsteam dieser Demokratiefabrik-Veranstaltung.';
      }
    }

    // DELEGATE
    if (thirdPerson) {
      return t(
        `auth.name_derivation_3rd_party${shortversion ? '_short' : ''}`,
        {
          fullname,
          var1,
          var2,
          var3,
        }
      );
    } else {
      return t('auth.name_derivation', {
        fullname: fullname,
        var1,
        var2,
        var3,
      });
    }
  };
  const markIndicatedEmail = (): void => {
    _emailIsAvailable.value = true;
  };

  const loadProfile = async () => {
    const store = getStore();
    store.dispatch('profilestore/touchRandomSeed');
    // console.log('BEFORE SYNC USER..');
    // SYNC USER PROFILE
    if (pkce.userid.value) {
      store.dispatch('profilestore/keepInSyncProfile', {
        oauthUserID: pkce.userid.value,
      });
    }
  };

  const setLocale = (lang: string | null = null, locale: any = null): void => {
    const store = getStore();
    const profile = store.state.profilestore?.profile?.user;
    if (!locale) {
      const { locale: localeNew } = useI18n();
      locale = localeNew;
    }

    if (!lang) {
      lang = profile?.LANG;
    }

    if (!lang) {
      // language unknown...
      return undefined;
    }

    if (lang === locale.value) {
      // already set
      return undefined;
    }

    locale.value = lang;
    // console.log('DEBUG: SET NEW LANGUAGE', locale.value);
  };

  const initialize = async (): Promise<void> => {
    // console.log('*** INIT OAUTH ***');

    const { locale } = useI18n();
    const $q = useQuasar();

    // set user language at first app -load...
    setLocale();

    oauthEmitter.on('AfterLogin', (payload: IPayload) => {
      console.log('AFTER LOGIN', payload);
      if (payload?.lang) {
        setLocale(payload.lang, locale);
      }
      loadProfile();
      afterLogin($q);
    });
    oauthEmitter.on('RecycleLogin', () => {
      loadProfile();
    });

    addRoutePermisionWatcher();
    const response = await pkce.initialize();
    return response;
  };
  // oauthAcls
  const afterLogin = ($q) => {
    const { pushR, gotoHome } = useRouterService();

    const store = getStore();
    console.log('AFTER LOGIN 2', $q);

    // Notify Backend
    store.dispatch('appstore/monitorSetup');
    monitorLog(constants.MONITOR_LOGIN);
    monitorContextData($q);

    // CHECK FOR REDIRECTION URL in local storage (During Login)
    const desitionationRouteJson = localStorage.getItem(
      'oauth2authcodepkce-destination'
    );
    console.log('AFTER LOGIN 3', desitionationRouteJson);
    if (desitionationRouteJson) {
      const destination_route = JSON.parse(desitionationRouteJson);
      if (destination_route) {
        localStorage.removeItem('oauth2authcodepkce-destination');
        pushR(destination_route);
        return;
      }
    }

    console.log('AFTER LOGIN 4');
    gotoHome();
    console.log('AFTER LOGIN 5');
    setLogoutState(false);
    console.log('AFTER LOGIN 6');
  };
  // -------

  // PKCE METHODS
  // ------------------------
  const setLogoutState = (state: boolean) => (logoutState.value = state);
  const logout = async (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _eventString: string | null = null,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _extra = {},
    silent = false
  ) => {
    const { pushR } = useRouterService();
    const store = getStore();

    setLogoutState(true);

    await store.dispatch('appstore/monitorFire', {
      eventString: Constants.MONITOR_LOGOUT,
      data: {},
      onlyWhenTokenValid: true,
    });

    store.dispatch('appstore/clearUserData');
    pkce.logout(silent);
    pushR('/logout');
  };

  const loginToCurrentPage = (): void => {
    const currentRoute = getCurrentRoute();
    const destination_route = {
      name: currentRoute.name,
      params: currentRoute.params,
    };
    pkce.login(destination_route);
  };
  /* Page Permission */
  const checkPagePermission = (currentRoute): void => {
    const router = getRouter();

    // allow for all general pages.
    if (!currentRoute.meta.assemblyAcl) {
      return;
    }

    // enforce Array format
    if (!Array.isArray(currentRoute.meta.assemblyAcl)) {
      currentRoute.meta.assemblyAcl = [currentRoute.meta.assemblyAcl];
    }

    // Wrongly configured....
    if (!currentRoute.params.assemblyIdentifier) {
      console.error('assemblyIdentifier is missing in the route params');
      throw new Error('MISCONFIGURATION');
    }

    // All other pages require login
    if (!pkce.payload.value) {
      // NOt logged in
      throw new Error('LOGIN_REQUIRED');
    }

    // find first permission that is allowed.
    const allowed = currentRoute.meta.assemblyAcl.find((assemblyAcl) => {
      const role = `${assemblyAcl}@${currentRoute.params.assemblyIdentifier}`;
      return pkce.payload.value.roles.includes(role);
    });

    if (!allowed) {
      router.push({ name: 'home' });
      throw new Error('AUTHORIZATION_FAILED');
    }
  };

  const addRoutePermisionWatcher = async () => {
    const currentRoute = getCurrentRoute();
    const router = getRouter();

    watch(
      () => currentRoute,
      (currentRoute) => {
        // Add Watcher for Authorization Check (client-side)
        try {
          checkPagePermission(currentRoute);

          // MONITOR ALL ROUTE CHANGES... (Add lag, so that no integrity errors happens)
          setTimeout(() => {
            monitorLog(constants.MONITOR_ROUTE_CHANGE);
          }, 3000);
        } catch (error) {
          router.push({ name: 'home' });

          // Show error message...
          switch ((error as any).message) {
            case 'ErrorInvalidGrant':
              oauthEmitter.emit('showAuthorizationInvalidToken');
              break;
            default:
              console.error(error);
              emitter.emit('showServiceError', { nobuttons: true });
              break;
          }
        }
      },
      { deep: true }
    );
  };

  const profile = computed((): IAssemblyUser => {
    return store.state.profilestore?.profile?.user;
  });

  const currentUsernameDerivation = computed((): string => {
    return getUsernameDerivation(profile.value, false, false);
  });

  const currentUsername = computed((): string => {
    return getUsername(profile.value);
  });

  const emailIsAvailable = computed(() => {
    return _emailIsAvailable.value || !!pkce.payload.value.userEmail;
  });

  return {
    initialize,
    logoutState: readonly(logoutState),
    setLogoutState,
    logout,
    loginToCurrentPage,
    checkPagePermission,
    refresh_token_if_required: pkce.refresh_token_if_required,
    authorized: readonly(pkce.authorized),
    jwt: readonly(pkce.jwt),
    payload: pkce.payload,
    userid: readonly(pkce.userid),
    pkce,
    setLocale,
    profile,
    getUsernameDerivation,
    currentUsernameDerivation,
    currentUsername,
    getUsername,
    is_in_testing_phase: store.getters['profilestore/is_in_testing_phase'],
    emailIsAvailable,
    markIndicatedEmail,
  };
}
