/** DEMOKRATIFABRIK RUNTIME VARIABLES */
import useEmitter from 'src/utils/emitter';
import { getCurrentRoute, getRouter } from 'src/services';
import { RouteLocationRaw, RouteLocationNormalizedLoaded } from 'vue-router';
import { ref } from 'vue';
import useStoreService from './store.service';
const { setAssemblyIdentifier, setStageID } = useStoreService();

const emitter = useEmitter();
const instanceNr = ref<number>(0);
const lastRouteString = ref<RouteLocationRaw | null>(null); // the last visited url
const currentRouteString = ref<RouteLocationRaw | null>(null); // the last visited url

/* Reload the page when redirecting to the same page */
const pushR = (route: RouteLocationRaw) => {
  const router = getRouter();
  const currentRoute = getCurrentRoute();
  const target = router.resolve(route).href;
  const current = router.resolve(currentRoute).href;
  if (target === current) {
    emitter.emit('reload');
  } else {
    router.push(route);
  }
};

// to enforce reload of page container!
const reload = () => {
  instanceNr.value += 1;
};

/* pushI: - Analog to push method: 
However: It does nothing when redirecting to the same page.
Default push method raises an error. 
*/
const pushI = (route: RouteLocationRaw) => {
  const router = getRouter();
  const currentRoute = getCurrentRoute();
  const target = router.resolve(route).href;
  const current = router.resolve(currentRoute).href;
  if (target !== current) {
    router.push(route);
  }
};

const gotoHome = () => {
  console.log('AFTER LOGIN Home 5b');

  pushR({ name: 'home' } as RouteLocationRaw);
};

const gotoProfile = () => {
  pushR({ name: 'profile' } as RouteLocationRaw);
};

const setLastRoute = (route: RouteLocationNormalizedLoaded | null) => {
  if (route) {
    const newRoute = {
      name: route.name,
      params: route.params,
    } as RouteLocationRaw;

    lastRouteString.value = currentRouteString.value;
    if (route) {
      currentRouteString.value = newRoute;
    }
  }
};

const getAssemblyHomeRoute = (assembly): RouteLocationRaw => {
  if (!assembly) {
    return {
      name: 'home',
    } as RouteLocationRaw;
  }

  return {
    name: assembly.type,
    params: { assemblyIdentifier: assembly.identifier },
  } as RouteLocationRaw;
};

const getAssemblyStartRoute = (assembly): RouteLocationRaw => {
  if (!assembly) {
    return {
      name: 'home',
    } as RouteLocationRaw;
  }
  return {
    name: `start__${assembly.type}`,
    params: {
      assemblyIdentifier: assembly.identifier,
      assemblyType: assembly.type,
    },
  } as RouteLocationRaw;
};

const getAssemblyManageRoute = (assembly): RouteLocationRaw => {
  if (!assembly) {
    return {
      name: 'home',
    } as RouteLocationRaw;
  }

  return {
    name: `assembly_manage__${assembly.type}`,
    params: {
      assemblyIdentifier: assembly.identifier,
      assemblyType: assembly.type,
    },
  } as RouteLocationRaw;
};

const gotoAssemblyHome = (assembly) => {
  const route = getAssemblyHomeRoute(assembly);
  pushR(route);
};

const gotoAssemblyStart = (assembly) => {
  const route = getAssemblyStartRoute(assembly);
  pushR(route);
};

const gotoAssemblyManage = (assembly) => {
  const route = getAssemblyManageRoute(assembly);
  pushR(route);
};

const clearSession = () => {
  setStageID(null);
  setAssemblyIdentifier(null);
};

export default function useRouterService() {
  // const currentRoute = getCurrentRoute();
  // const currentRouteMeta = computed(() => currentRoute?.meta);

  return {
    pushR,
    pushI,
    gotoProfile,
    gotoHome,
    reload,
    clearSession,
    setLastRoute,
    // currentRoute, // TODO: not used, right?
    gotoAssemblyManage,
    gotoAssemblyHome,
    gotoAssemblyStart,
    getAssemblyHomeRoute,
    getAssemblyManageRoute,
    instanceNr,
    lastRouteString,
    // currentRouteMeta,
  };
}
