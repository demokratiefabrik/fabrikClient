/** DEMOKRATIFABRIK SERVICE MANAGER */
import { ref } from 'vue';
import useAppService from 'src/services/app.service';
import useAssemblyService from 'src/services/assembly.service';
import useAuthService from 'src/services/auth.service';
import useMonitorService from 'src/services/monitor.service';
import useStageService from 'src/services/stage.service';

// export let store: ref<any> = Ref(undefined);
export const serviceSetup = (
  localStore,
  localRouter,
  localCurrentRoute,
  localT,
  localLocale
) => {
  t.value = localT;
  locale.value = localLocale;
  store.value = localStore;
  router.value = localRouter;
  currentRoute.value = localCurrentRoute;
};

// ENVIRONMENT VARIABLE
const store = ref<any>(undefined);
const router = ref<any>(undefined);
const currentRoute = ref<any>(undefined);
const t = ref<any>(undefined);
const locale = ref<any>(undefined);
const initializedServices = {};

export const getStore = () => {
  return store.value;
};
export const getRouter = () => {
  return router.value;
};
export const getCurrentRoute = () => {
  return currentRoute.value;
};
export const getT = () => {
  return t.value;
};
export const getLocale = () => {
  return locale.value;
};

export const getAppService = () => {
  if (!initializedServices['appService']) {
    // console.log('INIT appService');
    const appService = useAppService();
    initializedServices['appService'] = appService;
  }
  return initializedServices['appService'];
};

export const getAssemblyService = () => {
  if (!initializedServices['assemblyService']) {
    // console.log('INIT assemblyService');
    const assemblyService = useAssemblyService();
    initializedServices['assemblyService'] = assemblyService;
  }
  return initializedServices['assemblyService'];
};

export const getAuthService = () => {
  if (!initializedServices['authService']) {
    // console.log('INIT authService');
    const authService = useAuthService();
    initializedServices['authService'] = authService;
  }
  return initializedServices['authService'];
};

export const getMonitorService = () => {
  if (!initializedServices['monitorService']) {
    // console.log('INIT monitorService');
    const monitorService = useMonitorService();
    initializedServices['monitorService'] = monitorService;
  }
  return initializedServices['monitorService'];
};

export const getStageService = () => {
  if (!initializedServices['stageService']) {
    // console.log('INIT stageService');
    const stageService = useStageService();
    initializedServices['stageService'] = stageService;
  }
  return initializedServices['stageService'];
};
