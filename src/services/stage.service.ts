import { watch, computed, ref } from 'vue';
// import getMonitorService from './monitor.composable';
import constants from 'src/utils/constants';
import useRouterService from './router.service';
import useEmitter from 'src/utils/emitter';
// import useAssemblyService from './assembly.composable';
import library from 'src/utils/library';
import { IStageTuple } from 'src/models/stage';
import { IAssembly } from 'src/models/assembly';
import useStoreService from './store.service';
import { getAssemblyService, getMonitorService, getStore } from 'src/services';

const emitter = useEmitter();
const { loaded } = library;
const initialized = ref<boolean>(false);

const { stageID } = useStoreService();

export default function useStageService(addWatcher = true) {
  const store = getStore();
  console.assert(!!store);

  const { gotoStage } = getAssemblyService();
  const { monitorLog } = getMonitorService();
  const { gotoAssemblyStart } = useRouterService();

  const assembly = computed(
    (): IAssembly => store.getters['assemblystore/assembly']
  );
  const assembly_sorted_stages = computed(
    (): IStageTuple[] | null =>
      store.getters['assemblystore/assembly_sorted_stages']
  );
  const assemblyStages = computed(
    (): IStageTuple[] | null => store.getters['assemblystore/assemblyStages']
  );
  const assembly_accessible_stages = computed(
    (): IStageTuple[] | null =>
      store.getters['assemblystore/assembly_accessible_stages']
  );
  const assembly_scheduled_stages = computed(
    (): IStageTuple[] | null =>
      store.getters['assemblystore/assembly_scheduled_stages']
  );
  const IsManager = computed(
    (): boolean => store.getters['assemblystore/IsManager']
  );

  const is_stage_accessible =
    store.getters['assemblystore/is_stage_accessible'];

  const is_stage_alerted = store.getters['assemblystore/is_stage_alerted'];
  const is_stage_completed = store.getters['assemblystore/is_stage_completed'];

  const isRoutedStageAlerted = computed((): boolean | null => {
    if (!routedStageTuple.value) {
      return null;
    }
    // console.log(
    //   'DEBUG is_stage_alerted',
    //   is_stage_alerted(routedStageTuple.value),
    //   routedStageTuple.value
    // );
    return is_stage_alerted(routedStageTuple.value);
  });
  const isRoutedStageCompleted = computed((): boolean | null => {
    if (!routedStageTuple.value) {
      return null;
    }
    return is_stage_completed(routedStageTuple.value as IStageTuple);
  });

  const nextScheduledStage = computed(
    (): IStageTuple | null => store.getters['assemblystore/nextScheduledStage']
  );

  const routedStageTuple = computed((): IStageTuple | null => {
    if (!stageID.value) {
      return null;
    }
    if (!assemblyStages.value) {
      return null;
    }
    return assemblyStages.value[stageID.value];
  });

  const ready = computed((): boolean => {
    const ready = loaded(assemblyStages);
    if (ready) {
      emitter.emit('hideLoading');
    }
    return ready;
  });

  const routedStageContenttreeID = computed((): null | number => {
    if (!routedStageTuple.value?.stage?.contenttree_id) {
      return null;
    }
    return routedStageTuple.value?.stage?.contenttree_id;
  });

  const routedStageContenttreeIdentifier = computed((): null | string => {
    if (!routedStageTuple.value?.stage?.contenttree_id) {
      return null;
    }
    return `${routedStageTuple.value?.stage?.contenttree_id}`;
  });

  // --- TODO: move all below to assembly COMPO?
  const isFirstText = computed((): null | boolean => {
    if (assembly_sorted_stages.value?.length) {
      const firstTextStage = assembly_sorted_stages.value?.find(
        (stage) => stage?.stage.type === 'TEXTSHEET'
      );
      return routedStageTuple.value == firstTextStage;
    }
    return null;
  });

  // TODO: TO IMPLEMENT
  // const highlightedItem = computed((): null | boolean => {
  //   return this.sideMenuItems.find(
  //     (item) => {
  //       if (item.customHightlighting) {
  //         return item.customHightlighting()
  //       }
  //       const weights = stageMilestoneWeigths[item.anchor];
  //       return !weights || weights < 3
  //     }
  //   );
  // },

  const markUnAlert = (stageTuple: IStageTuple | null = null) => {
    // Notify stage as completed
    // if (!stage) {
    //   stage = routedStageTuple.value;
    // }
    // console.log('DEBUG!!', stageTuple);
    if (!stageTuple) {
      // console.log('DEBUG!!', routedStageTuple, routedStageTuple.value);
      stageTuple = routedStageTuple.value;
    }

    if (!stageTuple) {
      return;
    }
    // console.log('DEBUG markUnAlert', stageTuple, stageTuple.stage.id);
    store.dispatch('assemblystore/storeStageProgressionAlertFlag', {
      stageID: stageTuple.stage.id,
      alerted: false,
    });
    // console.log('DEBUG!!', stageTuple);

    // Fire (immediately), immediately
    const data = { stageID: stageTuple.stage.id };
    monitorLog(constants.MONITOR_STAGE_UNALERT, data);
  };

  // const markUnAlertById = (stageId: number) => {
  //   // Notify stage as completed
  //   console.log('DEBUG!!', stageId);
  //   store.dispatch('assemblystore/storeStageProgressionAlertFlag', {
  //     stageID: stageId,
  //     alerted: false,
  //   });

  //   // Fire (immediately), immediately
  //   const data = { stageID: stageId };
  //   monitorLog(constants.MONITOR_STAGE_UNALERT, data);
  // };

  const markCompleted = (stage: IStageTuple | null = null) => {
    // Notify stage as completed
    if (!stage) {
      stage = routedStageTuple.value;
    }

    if (!stage) {
      return;
    }
    if (stage && !stage?.progression?.completed) {
      monitorLog(constants.MONITOR_STAGE_COMPLETED);
    }

    store.dispatch('assemblystore/storeStageProgressionAlertFlag', {
      stageID: stage.stage.id,
      alerted: false,
    });
    store.dispatch('assemblystore/storeStageProgressionCompleted', {
      stageID: stage.stage.id,
      completed: true,
    });
  };

  const groups = computed((): string[] => {
    return Object.keys(stages_by_groups);
  });

  const stages_by_groups = computed(
    (): Record<string, IStageTuple[]> | null => {
      const stages_by_groups = {};
      if (!assembly_sorted_stages.value) {
        return null;
      }
      assembly_sorted_stages.value.forEach((stage: IStageTuple) => {
        if (!stages_by_groups[stage.stage.group]) {
          stages_by_groups[stage.stage.group] = [];
        }
        stages_by_groups[stage.stage.group].push(stage);
      });

      return stages_by_groups;
    }
  );

  const groupsAccessible = computed((): string[] | undefined => {
    // console.log('assembly_accessible_stages', assembly_accessible_stages.value);
    if (!assembly_accessible_stages.value) {
      return;
    }
    const groups = assembly_accessible_stages.value?.map(
      (stage) => stage.stage.group
    );
    return groups;
  });

  const groupsScheduled = computed((): string[] | undefined => {
    // console.log('DEBUGGER, ', assembly_scheduled_stages.value);
    if (!assembly_scheduled_stages.value) {
      return;
    }
    const groups = assembly_scheduled_stages.value.map(
      (stage) => stage.stage.group
    );
    // console.log('DEBUGGER, ', assembly_scheduled_stages.value, groups);
    return groups;
  });

  const routedStageGroup = computed((): string => {
    if (!stageID.value || !routedStageTuple.value) {
      return 'preparation';
    }
    return routedStageTuple.value.stage.group;
  });

  const is_stage_first_shown = (stage): boolean | undefined => {
    console.assert(stage);
    console.assert(assembly_sorted_stages);
    if (!assembly_sorted_stages.value) {
      return undefined;
    }
    const len = assembly_sorted_stages.value.length - 1;
    return stage === assembly_sorted_stages[len];
  };

  const is_stage_last_shown = (stage): boolean => {
    console.assert(stage);
    if (!assembly_sorted_stages.value) {
      return false;
    }
    return stage === assembly_sorted_stages.value[0];
  };

  const getFirstOrRoutedStageIDByGroup = (group): IStageTuple => {
    // console.assert(stages_by_groups);
    if (!stages_by_groups.value) {
      throw Error(
        'Could not find stage: stage groups are not yet loaded. refresh?'
      );
    }

    // console.log('group', group, stages_by_groups.value);
    if (!Object.keys(stages_by_groups.value).includes(group)) {
      throw Error('Could not find stage: invalid stage group');
    }

    const localStageGroups: IStageTuple[] = stages_by_groups.value[group];
    if (!localStageGroups.length) {
      throw Error('Could not find stage: no stage found in given group');
    }
    if (routedStageTuple.value) {
      if (
        localStageGroups.find(
          (x) => routedStageTuple.value?.stage.id === x.stage.id
        )
      ) {
        return routedStageTuple.value;
      }
    }

    // Get first stage wihtin group
    // TODO: Test!
    const stage: IStageTuple = localStageGroups[0];
    return stage;
  };

  const gotoScheduledStage = (): void => {
    if (!nextScheduledStage.value) {
      return;
    }

    gotoStage(nextScheduledStage.value);
  };

  // MOUNTED
  //TODO add to initialize function ()
  /* Reset Notifications when routing...Ensure that all (error) messages disappear, when route changes.. */
  if (!initialized.value) {
    initialized.value = true;
  }

  if (addWatcher) {
    watch(
      () => routedStageTuple.value,
      () => {
        if (routedStageTuple.value) {
          if (!IsManager.value) {
            if (
              routedStageTuple.value &&
              !is_stage_accessible(routedStageTuple.value)
            ) {
              // TODO: emit event...(and get reluctant of this method here...?)
              // console.log(assembly, assembly.value);
              gotoAssemblyStart(assembly.value);
            }
          }
        }
      },
      { immediate: true }
    );
  }

  return {
    ready,
    assembly_sorted_stages,
    routedStageTuple,
    isRoutedStageAlerted,
    gotoScheduledStage,
    isFirstText,
    routedStageContenttreeID,
    routedStageContenttreeIdentifier,
    markUnAlert,
    // markUnAlertById,
    markCompleted,
    routedStageGroup,
    groups,
    assemblyStages,
    nextScheduledStage,
    groupsScheduled,
    groupsAccessible,
    stages_by_groups,
    getFirstOrRoutedStageIDByGroup,
    is_stage_first_shown,
    is_stage_last_shown,
    isRoutedStageCompleted,
  };
}
