/** DEMOKRATIFABRIK RUNTIME VARIABLES */
import { ref } from 'vue';

const assemblyIdentifier = ref<string | null>(null);
const stageID = ref<number | null>(null);
const setAssemblyIdentifier = (identifier: string | null) => {
  // console.log('SEET ASSEMBLY IDENTIFIER...', identifier);
  assemblyIdentifier.value = identifier;
};
const setStageID = (id: number | null) => (stageID.value = id);

export default function useStoreService() {
  return {
    assemblyIdentifier,
    setAssemblyIdentifier,
    setStageID,
    stageID,
  };
}
