/** DEMOKRATIFABRIK RUNTIME VARIABLES */
import { watch, ref, readonly } from 'vue';
import { useRoute, useRouter } from 'vue-router';
import { useI18n } from 'vue-i18n';
import library from 'src/utils/library';
import useEmitter from 'src/utils/emitter';
import useRouterService from './router.service';
// import useAuthService from './auth.composable';
// import getMonitorService from 'src/composables/monitor.composable';
import constants from 'src/utils/constants';
import { useQuasar } from 'quasar';
import { useStore } from 'vuex';
import { INotificationBanner } from 'src/models/layout';
import useStoreService from './store.service';
import useOAuthEmitter from 'src/utils/VueOAuth2PKCE/oauthEmitter';
import {
  getAssemblyService,
  getAuthService,
  getMonitorService,
  getStore,
  getT,
  serviceSetup,
} from 'src/services';

export interface INotificationConfig {
  settimer?: boolean;
  nobuttons?: boolean;
}

const { removeItem, scrollToAnchor: scrollToAnchorLibrary } = library;
const headerOffset = ref<number>(150); // default header is minimized within assembly sections
const setHeaderOffset = (offset: number) => (headerOffset.value = offset);

// APP State
// const appExitState = ref<boolean>(false);
const emitter = useEmitter();
const oAuthEmitter = useOAuthEmitter();

const { setStageID, setAssemblyIdentifier, assemblyIdentifier, stageID } =
  useStoreService();

// Notifications
const notificationBanner = ref<INotificationBanner | undefined>(undefined);
const loadingGifStack = ref<string[]>([]);

export default function useAppService() {
  // INIT SERVICE
  // SETUP CUSTOM SERVICES
  const store = useStore();
  const router = useRouter();
  const currentRoute = useRoute();
  const assemblyService = getAssemblyService();
  const {
    initialize: assemblyInitialize,
    syncUserAssembly,
    // assembly,
  } = assemblyService;
  const {
    authorized,
    logoutState,
    initialize: authInitialize,
    logout,
    is_in_testing_phase,
  } = getAuthService();

  const { initialize: monitorInitialize } = getMonitorService();
  const { setLastRoute } = useRouterService();
  const $q = useQuasar();
  const { t, locale } = useI18n();

  const initialize = () => {
    // console.log('DEBUG: INITIALIZE APP.COMPO');
    // FIRST: install plugins
    // installedAssemblyPlugins
    // assemblyService.installAssemblyPlugin('CIR')

    // const appService = useAppService();

    serviceSetup(store, router, currentRoute, t, locale);

    // serviceSetup(store);
    // console.log(IsManager2.value, '11111');

    authInitialize();

    // START MONITOR ENGINE
    monitorInitialize();
    // START ASSEMBLY ENGINE
    assemblyInitialize();
    // LISTENING ON ERRORS
    emitter.on('showNetworkError', () => {
      showNetworkError();
    });
    emitter.on(
      'showServiceError',
      (options: undefined | INotificationConfig = undefined) => {
        showServiceError(options);
      }
    );
    emitter.on('showAuthorizationError', () => {
      showAuthorizationError();
    });
    emitter.on('showTooManyRequestsError', () => {
      showTooManyRequestsError();
    });
    emitter.on('showAuthenticationWarning', () => {
      showAuthenticationWarning();
    });

    oAuthEmitter.on('PKCEAuthenticationError', () => {
      showAuthenticationError();
    });

    emitter.on('receiveBackendFeedback', async (data: any) => {
      // RECEIVE MESSAGES FROM FROMBACKEND
      // console.log('RESPONSE MONITORED');

      // Shortcuts
      if (!data.ok) {
        return null;
      }
      if (logoutState.value) {
        // console.log(
        //   'LOGOUT PROCESS IS GOING ON: DO NOT PROCESS NEW INCOMING DATA.'
        // );
        return;
      }

      // Errors
      if (data.response.errors?.length) {
        data.response.errors.forEach((error) => {
          console.error(error.message, error.event);
        });
        const message = 'Die Datenübermittlung ist beeinträchtigt.';
        $q.notify({ type: 'nFabrikWarning', message });
      }

      // Warnings
      if (data.response.warnings?.length) {
        const displayedWarning: string[] = [];
        data.response.warnings.forEach((warning) => {
          // show only one warning at the time...
          const warnignLabel = warning.event?.eventString;
          if (!warnignLabel || displayedWarning.includes(warnignLabel)) {
            return;
          }
          displayedWarning.push(warnignLabel);
          let message = warning.message;
          if (message in constants.MONITOR_WARNING_MESSAGES) {
            message = constants.MONITOR_WARNING_MESSAGES[message];
          }
          $q.notify({ type: 'nFabrikWarning', message });
        });
      }

      // dont update for public guests..
      // const IsPublicGuest = computed(
      //   (): boolean => store.getters['assemblystore/IsPubicGue']
      // );

      // // Update transmitted Data...
      if (authorized.value && !logoutState.value) {
        // Write newest data to the store!
        store.dispatch('appstore/updateStore', { data: data.response });
        // see if there are new notifications...
        store.dispatch('profilestore/checkToUpdateNotifications');
      }
    });

    /* Reset Notifications when routing...Ensure that all (error) messages disappear, when route changes.. */
    watch(currentRoute, () => {
      // console.log(
      //   'DEBUG WATCHER in CURRENT ROUTE (APP)',
      //   'syncAssembliesSync',
      //   currentRoute,
      //   currentRoute.params?.assemblyIdentifier
      // );
      notificationBanner.value = undefined;
      setLastRoute(currentRoute);
      // console.log('DEBUG WATCHER', currentRoute?.params);
      if (currentRoute.params?.assemblyIdentifier) {
        // UPDATE CURRENT ASSEMBLY...
        // TODO: redirect to Home, when assembly is invalid
        // console.log('DEBUG WATCHER 2');
        if (
          currentRoute.params?.assemblyIdentifier != assemblyIdentifier.value
        ) {
          setAssemblyIdentifier(
            currentRoute?.params?.assemblyIdentifier as string | null
          );
          syncUserAssembly();
        }

        // UPDATE STAGES...
        // TODO: redirect to asembly home, when stage is invalid
        if (
          currentRoute.params?.stageID !== null &&
          currentRoute.params?.stageID !== undefined &&
          currentRoute.params?.stageID !== `${stageID.value}`
        ) {
          setStageID(parseInt(currentRoute.params.stageID as string));
        }
      }
    });

    // see if there are new notifications...
    store.dispatch('profilestore/checkToUpdateNotifications');
  };

  /* NOTIFICATIONS 
  -------------------------*/
  const showNotification = (banner: INotificationBanner): void => {
    if (banner.settimer) {
      setTimeout(() => {
        notificationBanner.value = undefined;
        emitter.emit('notificationBannerChange', notificationBanner.value);
      }, 5000);
    }
    if (banner.type === 'error') {
      clearLoadingGif();
    }
    notificationBanner.value = banner;
    emitter.emit('notificationBannerChange', notificationBanner.value);
  };

  const showAuthorizationError = (
    config: INotificationConfig | null = null
  ) => {
    const t = getT();
    const isInsideAssembly = !!assemblyIdentifier.value;
    const banner = {
      type: 'error',
      icon: 'mdi-key-outline',
      title: t('app.error.authorization_error_title'),
      body: t('app.error.authorization_error_body'),
      buttons: [isInsideAssembly ? 'home' : 'hide'],
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_AUTHORIZATION, banner);
    showNotification(banner);
  };

  const showServiceError = (
    config: INotificationConfig | undefined = undefined
  ) => {
    const t = getT();
    const banner = {
      type: 'error',
      icon: 'mdi-alarm-light-outline',
      title: t('app.error.service_error_title'),
      body: t('app.error.service_error_body'),
      buttons: config?.nobuttons ? ['hide'] : ['reload'],
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_SERVICE, banner);
    showNotification(banner);
  };

  const showNetworkError = (config: INotificationConfig | null = null) => {
    const t = getT();
    const banner = {
      type: 'error',
      icon: 'mdi-alarm-light-outline',
      title: t('app.error.network_error_title'),
      body: t('app.error.network_error_body'),
      buttons: ['reload', 'hide'],
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_NETWORK, banner);
    showNotification(banner);
  };

  const showAuthorizationInvalidToken = (
    config: INotificationConfig | null = null
  ) => {
    const t = getT();
    const banner = {
      type: 'error',
      icon: 'mdi-key-outline',
      title: t('auth.authentication_invalid_warning_title'),
      body: t('auth.authentication_invalid_warning_body'),
      buttons: ['auth', 'home'],
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_INVALID_TOKEN, banner);
    showNotification(banner);
    const { logout } = getAuthService();
    logout(null, {}, true);
  };

  const showTooManyRequestsError = (
    config: INotificationConfig | null = null
  ) => {
    const t = getT();
    const banner = {
      type: 'error',
      icon: 'mdi-car-multiple',
      title: t('app.error.toomanyrequests_error_title'),
      body: t('app.error.toomanyrequests_error_body'),
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_TOO_MANY_REQUESTS, banner);
    showNotification(banner);
  };

  const showAuthenticationWarning = (
    config: INotificationConfig | null = null
  ) => {
    const t = getT();

    // DOES NOT WORK: assembly is empty...
    // const no_direct_login = assembly.value?.custom_data['NO_DIRECT_LOGIN'];
    // console.log('DEBUG::: assembly', no_direct_login, assembly.value);

    const isInsideAssembly = !!assemblyIdentifier.value;

    const banner = {
      type: 'warning',
      icon: 'mdi-emoticon-cool-outline',
      title: t('auth.authentication_warning_title'),
      body: t('auth.authentication_warning_body'),
      buttons: [isInsideAssembly ? 'home' : 'hide', 'auth'],
      settimer: config?.settimer ? true : false,
    };
    // if (no_direct_login) {
    banner['buttons'] = [isInsideAssembly ? 'home' : 'hide'];
    banner['body'] = t('auth.authentication_warning_body');
    // }

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_WARNING_AUTHENTICATION, banner);
    showNotification(banner);
  };

  const showAuthenticationError = (
    config: INotificationConfig | null = null
  ) => {
    const t = getT();
    const banner = {
      type: 'error',
      icon: 'mdi-alarm-light-outline',
      title: t('auth.authentication_error_title'),
      body: t('auth.authentication_error_body'),
      settimer: config?.settimer ? true : false,
    };

    const { monitorLog } = getMonitorService();
    monitorLog(constants.MONITOR_ERROR_AUTHENTICATION, banner);
    showNotification(banner);
  };

  const showLoadingGif = (label) => {
    loadingGifStack.value.push(label);
    emitter.emit('loadingGifStackChange', loadingGifStack.value);
    setTimeout(() => {
      emitter.emit('loadingGifStackChange', loadingGifStack.value);
      loadingGifStack.value = removeItem(loadingGifStack.value, label);
    }, 5000);
  };

  const hideLoadingGif = (label) => {
    loadingGifStack.value = removeItem(loadingGifStack.value, label);
    emitter.emit('loadingGifStackChange', loadingGifStack.value);
  };

  emitter.on('routeChange', () => {
    // console.log('ROUTE CHANGE IN APP:COM');
    clearLoadingGif();
  });

  emitter.on('newDaySession', (day) => {
    const store = getStore();
    console.log('NEW DAY SESSION STARTED. Day number ', day);
    store.dispatch('profilestore/startNewDaySession', { day });
  });

  /** Remove all loadingGifs (used in case of errors) */
  const clearLoadingGif = () => {
    loadingGifStack.value = [];
  };

  const scrollToAnchor = (anchor, duration = 300, lag = 0): void => {
    scrollToAnchorLibrary(anchor, headerOffset.value, duration, lag);
  };

  const apireset = (full: boolean) => {
    // TESTING: reset user data of the day or the full assembly session...
    const store = getStore();
    if (!is_in_testing_phase.value) {
      throw 'Apireset is only possible in testeing phase!!';
    }
    // Reset Api
    store.dispatch('appstore/monitorReset', { notifyBackend: true, full });
    setTimeout(() => {
      logout();
    }, 10);
  };

  return {
    headerOffset: readonly(headerOffset),
    showAuthenticationError,
    showServiceError,
    showAuthenticationWarning,
    showNetworkError,
    showTooManyRequestsError,
    showAuthorizationInvalidToken,
    showAuthorizationError,
    hideLoadingGif,
    showLoadingGif,
    apireset,
    initialize,
    setHeaderOffset,
    scrollToAnchor,
  };
}
