import constants from 'src/utils/constants';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';
import { useQuasar } from 'quasar';
import { getCurrentRoute, getStore } from 'src/services';
import useStoreService from './store.service';

export default function useMonitorService() {
  const initialize = () => {
    // Start periodic monitorLog Trigger
    // keep this interval low (much lower than the intervall number specified in env. files)
    // (e.g. 1 Min.)
    // console.log('*** START MONITOR ENGINE ***')
    const intervallString: string = process.env
      .ENV_APISERVER_MONITOR_INTERVAL_SECONDS
      ? process.env.ENV_APISERVER_MONITOR_INTERVAL_SECONDS
      : '60';
    const intervall = parseInt(intervallString);
    setInterval(() => {
      monitorFire();
    }, intervall * 1000);

    // console.log('MONITOR INIT DEBUG');

    // setTimeout(() => {
    //   monitorContextData();
    // }, 3000);
  };

  // ---------
  const monitorLog = async (
    eventString: string | null = null,
    data: Record<string, unknown> = {}
  ) => {
    const store = getStore();
    const currentRoute = getCurrentRoute();
    const { authorized } = usePKCEComposable();

    if (!authorized) {
      return null;
    }
    if (eventString === constants.MONITOR_ROUTE_CHANGE) {
      data.extra = { name: currentRoute.name };
    }
    data = { name: currentRoute.name, ...currentRoute.params, ...data };
    if (assemblyIdentifier.value && !data.assemblyIdentifier) {
      data.assemblyIdentifier = assemblyIdentifier.value;
    }
    store.dispatch('appstore/monitorLog', {
      eventString,
      data,
    });
  };

  const { assemblyIdentifier } = useStoreService();

  const monitorFire = async (
    eventString: string | null = null,
    extra = {},
    onlyWhenTokenValid = false
  ) => {
    const store = getStore();
    const currentRoute = getCurrentRoute();
    const { authorized } = usePKCEComposable();

    if (!authorized.value) {
      return null;
    }

    const data: Record<string, unknown> = {
      name: currentRoute.name,
      ...currentRoute.params,
      ...extra,
    };

    if (assemblyIdentifier.value && !data.assemblyIdentifier) {
      data.assemblyIdentifier = assemblyIdentifier.value;
    }
    await store.dispatch('appstore/monitorFire', {
      eventString,
      data,
      onlyWhenTokenValid,
    });
  };

  const monitorContextData = ($q: any = null) => {
    if (!$q) {
      $q = useQuasar();
    }
    const data: Record<string, unknown> = { extra: $q.platform.is };
    data.screenW = screen.width;
    monitorLog(constants.MONITOR_CONTEXT, data);
  };

  return {
    initialize,
    monitorLog,
    monitorFire,
    monitorContextData,
  };
}
