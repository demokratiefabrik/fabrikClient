import { deCH, deDE } from 'src/i18n';
import { createI18n } from 'vue-i18n';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
  // Setup I18n instance
  const i18n = createI18n({
    locale: 'de-ch', // default
    legacy: false, // you must set `false`, to use Composition API
    globalInjection: true,
    fullInstall: true,
    // warnHtmlInMessage: 'off', // This should disable the warning in browser console. But it doesn't
    warnHtmlMessage: false, // disable warning HTML in message
    fallbackWarn: false,
    missingWarn: false,
    messages: { 'de-ch': deCH, 'de-de': deDE },
  });

  app.use(i18n);
});
