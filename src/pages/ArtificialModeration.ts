export default {
  indexpage_left: {
    id: 'indexpage_left',
    prosa: ' Erster AM: Begrüsst die Leute auf der Webseite.',
    // loading: (ctx) =>
    //   ctx.authorized === null || (ctx.authorized && !ctx.profile?.U),
    items: [
      {
        id: 1,
        prosa: 'GÄSTE',
        // condition: (ctx) => !ctx.authorized,
        body: (ctx) => [
          'Guten Tag liebe Besucherinnen und Besucher!',
          `Wir sind ${ctx.$t('am.actor.2')} und ${ctx.$t(
            'am.actor.1'
          )} und wir freuen uns sehr, dass Sie hier sind.`,
        ],
      },
      // {
      //   id: 211,
      //   prosa: 'USER',
      //   condition: (ctx) =>
      //     ctx.authorized && !['MANAGER', 'EXPERT'].includes(ctx.profile?.ROLE),
      //   body: (ctx) => [
      //     // 'Herzlich willkommen!',
      //     'Herzlich willkommen in der Demokratiefabrik!',
      //     `In der Demokratiefabrik tragen Sie den Namen «${ctx.profile?.U}». ${ctx.currentUsernameDerivation}. So bleiben Sie anonym.`,
      //   ],
      // },
      // {
      //   id: 2112,
      //   prosa: 'MANAGER',
      //   condition: (ctx) => ctx.authorized && ctx.profile?.ROLE === 'MANAGER',
      //   body: (ctx) => [
      //     'Herzlich willkommen!',
      //     `In der Demokratiefabrik tragen Sie den Namen «${ctx.profile?.U}». So bleiben Sie anonym.`,
      //   ],
      // },
      // {
      //   id: 211,
      //   prosa: 'EXPERTS',
      //   condition: (ctx) => ctx.authorized && ctx.profile?.ROLE === 'EXPERT',
      //   body: (ctx) => [
      //     'Herzlich willkommen!',
      //     `Vielen Dank, dass Sie vorbeischauen. In der Demokratiefabrik tragen Sie den Namen «${ctx.profile?.U}». ${ctx.currentUsernameDerivation}.`,
      //   ],
      // },
    ],
  },

  indexpage_right: {
    id: 'indexpage_right',
    prosa: ' Zweite: Informiert über Aktualität.',
    items: [
      {
        id: 5,
        // condition: (ctx) => !ctx.IsThereAnAssemblyOngoing,
        body: () => [
          'Sie finden auf dieser Seite viele Informationen über die Demokratiefabrik.',
          'Zum Beispiel finden Sie unter der Rubrik «News» den Ergebnisbericht zur Könizer Demokratiefabrik und eine Möglichkeit, unseren Newsletter zu abonnieren.',
        ],
      },
      // {
      //   id: 52,
      //   condition: (ctx) => !ctx.authorized && ctx.IsThereAnAssemblyOngoing,
      //   body: () => [
      //     'Wurden Sie eingeladen in der Demokratiefabrik mitzuwirken? Dann verwenden Sie bitte den Anmeldelink im Informations-E-Mail. Falls Sie dieses nicht mehr haben, kontaktieren Sie uns bitte unter demokratiefabrik.ipw@unibe.ch.',
      //   ],
      // },
      // {
      //   id: 51,
      //   prosa:
      //     'Authenticated && assembly is ONGOING => Assuming that visitor is a delegate',
      //   condition: (ctx) => ctx.authorized && ctx.IsThereAnAssemblyOngoing,
      //   body: (ctx: any) => {
      //     return [
      //       // `Es findet in diesen Tagen die Könizer Online-Versammlung statt.`,
      //       // `Sind Sie in Köniz stimmberechtigt und wurden Sie zur Könizer Online-Versammlung eingeladen? Dann möchten wir Sie bitten sich hier einzuloggen.`
      //       // `Wurden Sie von der Gemeinde Demokratiefabrik zufällig ausgewählt,
      //       ctx.UsersDelegateAssemblies.length
      //         ? 'Sophie und ich führen Sie Schritt für Schritt durch die Demokratiefabrik. Möchten Sie gleich beginnen?'
      //         : 'Hier gelangen Sie zur Moderationsumgebung.',
      //       // TODO: proper moderation test..
      //     ];
      //   },
      //   buttons: [
      //     {
      //       condition: (ctx) =>
      //         ctx.IsThereAnAssemblyOngoing &&
      //         ctx.UsersDelegateAssemblies.length,
      //       action: (ctx) =>
      //         ctx.gotoAssemblyStart(ctx.UsersDelegateAssemblies[0]),
      //       // action: (ctx) => ctx.login(ctx.getAssemblyHomeRoute(ctx.ongoing_assemblies[0])),
      //       label: () => 'Zum Start',
      //     },
      //     {
      //       condition: (ctx) =>
      //         ctx.IsThereAnAssemblyOngoing && ctx.UsersExpertAssemblies.length,
      //       action: (ctx) =>
      //         ctx.gotoAssemblyStart(ctx.UsersExpertAssemblies[0]),
      //       label: () => 'Zum Start',
      //     },

      //     {
      //       condition: (ctx) =>
      //         ctx.IsThereAnAssemblyOngoing && ctx.UsersManagerAssemblies.length,
      //       action: (ctx) =>
      //         ctx.gotoAssemblyManage(ctx.UsersManagerAssemblies[0]),
      //       label: () => 'Moderation',
      //     },
      //   ],
      // },
    ],
  },

  // PREVIEW PAGE
  indexongoing_left: {
    id: 'indexpage_left',
    prosa: ' Erster AM: Begrüsst die Leute auf der Webseite.',
    loading: (ctx) =>
      ctx.authorized === null || (ctx.authorized && !ctx.profile),
    items: [
      {
        id: 1,
        prosa: 'GÄSTE',
        condition: (ctx) => !ctx.authorized,
        body: (ctx) => [
          'Guten Tag liebe Besucherinnen und Besucher!',
          `Wir sind ${ctx.$t('am.actor.2')} und ${ctx.$t(
            'am.actor.1'
          )} und wir freuen uns sehr, dass Sie hier sind.`,
        ],
      },

      {
        id: 4,
        prosa: 'ctx.authorized: LONGTERM!',
        condition: (ctx) =>
          ctx.authorized &&
          ctx.profile &&
          ctx.$filters.minutesSince(ctx.profile.date_created) > 60,
        body: (ctx) =>
          `Herzlich willkommen zurück, ${ctx.profile.U}! Es freut uns, dass Sie wieder hier sind!`,
      },
      {
        id: 5,
        prosa: 'ctx.authorized: FIRST HOUR!',
        condition: (ctx) =>
          ctx.authorized &&
          ctx.profile &&
          ctx.$filters.minutesSince(ctx.profile.date_created) <= 60,
        body: (ctx) => {
          return [
            'Herzlich willkommen!',
            `In der Demokratiefabrik tragen Sie den Namen ${ctx.profile.U}. ${ctx.currentUsernameDerivation}. So bleiben Sie in der Demokratiefabrik anonym.`,
          ];
        },
      },
    ],
  },

  indexongoing_right: {
    id: 'indexpage_right',
    prosa: ' Zweite: Informiert über Aktualität.',
    loading: (ctx) =>
      ctx.authorized === null || (ctx.authorized && !ctx.profile),
    items: [
      // {
      //   id: 101,
      //   prosa: 'Not authenticated && assembly is ONGOING => Assuming that visitor is a delegate',
      //   condition: (ctx) => !ctx.authorized && ctx.IsThereAnAssemblyOngoing,
      //   body: (ctx) => {
      //     return [
      //       `Am 14. Juni geht es los. Wir halten Sie hier auf dem Laufenden.`
      //     ]
      //   }
      // },

      {
        id: 1,
        prosa:
          'Not authenticated && assembly is ONGOING => Assuming that visitor is a delegate',
        condition: (ctx) => !ctx.authorized && ctx.IsThereAnAssemblyOngoing,
        body: () => {
          return [
            // `Es findet in diesen Tagen die Könizer Online-Versammlung statt.`,
            // `Sind Sie in Köniz stimmberechtigt und wurden Sie zur Könizer Online-Versammlung eingeladen? Dann möchten wir Sie bitten sich hier einzuloggen.`
            // `Wurden Sie von der Gemeinde Demokratiefabrik zufällig ausgewählt,
            'Sophie und ich führen Sie Schritt für Schritt durch die Demokratiefabrik. Möchten Sie gleich beginnen?',
          ];
        },
        buttons: [
          {
            condition: (ctx) => !ctx.authorized && ctx.IsThereAnAssemblyOngoing,
            action: (ctx) => ctx.loginToCurrentPage(),
            // action: (ctx) => ctx.login(ctx.getAssemblyHomeRoute(ctx.ongoing_assemblies[0])),
            label: () => 'Zum Login der Demokratiefabrik',
          },
        ],
      },
      {
        id: 2,
        condition: (ctx) =>
          ctx.authorized && ctx.IsUserDelegateOfOngoingAssembly,
        body: (ctx) => {
          const firstDay =
            ctx.profile &&
            ctx.profile &&
            ctx.$filters.minutesSince(ctx.profile.date_created) <= 60;
          return [
            firstDay
              ? 'Wir freuen uns sehr, dass Sie hier sind! Ist es für Sie in Ordnung, wenn wir gleich starten?'
              : 'Wir können gleich wieder fortfahren. Sind Sie bereit?',
          ];
        },
      },
      {
        id: 3,
        condition: (ctx) =>
          ctx.authorized &&
          !ctx.IsThereAnAssemblyOngoing &&
          ctx.IsThereAnAssemblyInPublicState,
        body: () =>
          'Auf dieser Webseite finden Sie spannende Ergebnisse von unserer Online-Versammlung. Schauen Sie sich bitte um!',
      },
      {
        id: 4,
        condition: (ctx) =>
          ctx.authorized &&
          !ctx.IsUserDelegateOfOngoingAssembly &&
          ctx.IsThereAnAssemblyOngoing,
        body: () =>
          'Wir sind aktuell gerade an der Durchführung der einer Online Bürgerversammlung. Sie finden auf dieser Webseite viele Informationen dazu.',
      },
      {
        id: 5,
        condition: (ctx) => ctx.IsThereNothingGoingOn,
        body: () => [
          'Sie finden auf dieser Seite viele Informationen über die Demokratiefabrik.',
          'Zum Beispiel finden Sie unter der Rubrik «News» den Ergebnisbericht zur Könizer Demokratiefabrik und eine Möglichkeit, unseren Newsletter zu abonnieren.',
        ],
      },
    ],

    buttons: [
      {
        condition: (ctx) =>
          ctx.authorized && ctx.IsUserDelegateOfOngoingAssembly,
        action: (ctx) => ctx.gotoAssemblyStart(ctx.UsersDelegateAssemblies[0]),
        label: (ctx) =>
          ctx.$t('assembly.goto_button_label_f', {
            label: ctx.UsersDelegateAssemblies[0].title,
          }),

        // label: () => 'Zur Könizer Demokratiefabrik, bitte!',
      },
      {
        condition: (ctx) =>
          ctx.authorized &&
          ctx.IsUserObserverOfOngoingAssembly &&
          !ctx.IsUserDelegateOfOngoingAssembly &&
          !ctx.UsersManagerAssemblies?.length,
        action: (ctx) => ctx.gotoAssemblyStart(ctx.UsersObserverAssemblies[0]),
        label: () => 'Eintreten als Beobachter',
      },
      {
        condition: (ctx) =>
          ctx.authorized && ctx.UsersManagerAssemblies?.length > 0,
        action: (ctx) => ctx.gotoAssemblyManage(ctx.UsersManagerAssemblies[0]),
        label: () => 'Verwaltung',
      },
    ],
  },

  background_top: {
    id: 'background_top',
    prosa: ' Hintergrund-oben',
    items: [
      {
        id: 1,
        body: (ctx) => ctx.$t('background.am.page_introduction'),
      },
    ],
  },

  // ki-demokratiefabrik@proton.me
  background_bottom: {
    id: 'background_bottom',
    prosa: ' Hintergrund: unten.',
    items: [
      {
        id: 2,
        body: (ctx) => ctx.$t('background.am.open_questions'),
        buttons: [
          {
            action: (ctx) =>
              ctx.clickSendEmail('ki-demokratiefabrik@proton.me'),
            label: (ctx) => ctx.$t('background.am.cmd_email_composer'),
          },
        ],
      },
    ],
  },

  news_top: {
    id: 'news_top',
    prosa: ' News Zuoberst.',
    items: [
      {
        id: 1,
        body: (ctx) => ctx.$t('news.am.newsletter'),
      },
    ],

    buttons: [
      {
        action: (ctx) => ctx.clickNewsletter(),
        label: (ctx) => ctx.$t('news.am.cmd_newsletter_abo'),
      },
    ],
  },
};
