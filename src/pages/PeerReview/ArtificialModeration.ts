import { IPeerreviewTuple } from 'src/models/peerreview';

export interface ICtx {
  peerreviewTuple: IPeerreviewTuple;
  peerreviewCompleted: boolean;
  peerreviewAssigned: boolean;
  peerreviewFinalized: boolean;
}

const AMs = {
  readPeerreviewCarfully: {
    id: 'readPeerreviewCarfully',
    prosa: ' Über dem ersten Review Text.',
    loading: (ctx: ICtx) => !ctx.peerreviewTuple?.peerreview,
    items: [
      {
        id: 1,
        condition: (ctx: ICtx) =>
          !ctx.peerreviewCompleted && ctx.peerreviewAssigned,
        prosa: ' ... Bitte lesen sie das Proposal.',
        body: () =>
          'Lesen Sie bitte diesen Antrag sorgfältig durch und geben Sie danach Ihr Gutachten ab. ',
        buttons: [
          {
            action: (ctx) => {
              ctx.tab = 'response';
            },
            label: () => 'Gutachten Abgeben...',
          },
        ],
      },
      {
        id: 1,
        condition: (ctx: ICtx) =>
          ctx.peerreviewCompleted && ctx.peerreviewAssigned,
        prosa: ' ...bereits abgestimmt => Zu den Resultaten.',
        body: () =>
          'Sie haben Ihr Gutachten bereits abgegeben. Schauen Sie in der Rubrik "Resultat" nach, was andere vom Vorschlag halten.',
        buttons: [
          {
            action: (ctx) => {
              ctx.tab = 'result';
            },
            label: () => 'Zu den Resultaten...',
          },
        ],
      },
      {
        id: 2,
        condition: (ctx: ICtx) => ctx.peerreviewFinalized,
        prosa: ' ... Bereits beended.',
        body: () => 'Der Beschluss zu diesem Gutachten steht schon fest.',
        buttons: [
          {
            action: (ctx) => {
              ctx.tab = 'result';
            },
            label: () => 'Zu den Resultaten...',
          },
        ],
      },
      {
        id: 3,
        condition: (ctx: ICtx) =>
          !ctx.peerreviewFinalized && !ctx.peerreviewAssigned,
        prosa: ' ... Ongoing decision.',
        body: () =>
          'Über dieses Gutachten wird gerade abgstimmt. Sehen Sie der aktuelle Stand in der Rubrik "Resultat".',
        buttons: [
          {
            action: (ctx) => {
              ctx.tab = 'result';
            },
            label: () => 'Zur Rubrik "Resultat"...',
          },
        ],
      },
    ],
  },

  completedPeerreview: {
    id: 'completedPeerreview',
    prosa: ' Am Ende des Peerreview erfassens.',
    loading: (ctx: ICtx) => !ctx.peerreviewTuple?.peerreview,
    items: [
      {
        id: 1,
        condition: (ctx: ICtx) =>
          ctx.peerreviewCompleted && ctx.peerreviewAssigned,
        prosa: ' ... Sie können das Fenster wieder schliessen.',
        body: () =>
          'Vielen herzlichen Dank! Sie können dieses Fenster nun wieder schliessen.',
        buttons: [
          {
            action: (ctx) => {
              ctx.tab = 'result';
            },
            label: () => 'Zur Rubrik "Resultat"...',
          },
          {
            action: (ctx) => {
              ctx.closeDialog();
            },
            label: () => 'Schliessen',
          },
        ],
      },
    ],
  },
};

export default AMs;
