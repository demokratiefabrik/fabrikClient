// import { INode } from "./content"

import { IAssemblyUser } from './assembly';
import { INode } from './content';

export interface IPeerreview {
  id: number;
  // content_id: number
  rejected: boolean | null;
  approved: boolean | null;
  contenttree_id: number;
  // content_id: number;
  discussion_content_id: number;
  date_created: Date | string;
  date_rejected: Date | null;
  date_approved: Date | null;
  data_to_apply_on_success: any;
  nof_responded: number;
  nof_invited: number;
  nof_approved: number;
  nof_rejected: number;
  nof_criteria_accept1: number;
  nof_criteria_accept2: number;
  nof_criteria_accept3: number;
  nof_criteria_accept4: number;
  nof_criteria_accept5: number;
  // nof_criteria_accept6: number;
  operation: 'INSERT' | 'UPDATE';
}

export interface IPeerreviewProgression {
  date_responded: Date | null;
  criteria_accept1?: boolean;
  criteria_accept2?: boolean;
  criteria_accept3?: boolean;
  criteria_accept4?: boolean;
  criteria_accept5?: boolean;
  // criteria_accept6?: boolean;
  response: boolean | null;
}

export interface IPeerreviewTuple {
  disabled: boolean | null;
  peerreview: IPeerreview;
  progression: IPeerreviewProgression;
  content: INode;
  creator: IAssemblyUser;
}
