export interface INode {
  id: number | null;
  contenttree_identifier: string | null;
  parent_id?: number | null;
  contenttree_id: number | null;
  reviewed: boolean | null; // reviewed by MANAGER
  pending_peerreview_for_insert?: number | null; // Under Review by Peers
  pending_peerreview_for_update?: number | null; // Under Review by Peers
  private_property: boolean;
  common_property: boolean;
  given_property: boolean;
  locked: boolean;
  title: string;
  type: string;
  disabled: boolean;
  text: string;
  date_created: Date | string;
  rejected?: boolean | null;
  acl: string[];
  S?: {
    SA: number;
    RA: number;
    RC: number;
    SC: number;
  };
}
export interface INodeData {
  id?: number | null;
  parent_id?: number | null;
  title?: string;
  type?: string;
  disabled?: boolean;
  text?: string;
}

export interface INodeProgression {
  content_id?: number | null;
  read: boolean | null;
  discussed: boolean | null;
  rating: null | number;
  salience: null | number;
  certainity: null | number;
  view: boolean | null;
  alerted: boolean | null;
}

export interface INodeTuple {
  content: INode | null;
  progression?: INodeProgression | null;
  id: null | number;
  path?: number[];
  creator?: any;

  // children: INodeTuple[];
  // nof_descendants?: number;
}
