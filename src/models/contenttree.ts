import { INodeTuple } from './content';

// export interface IContentTree {
//   date_last_tree_modification?: Date | string;
//   id: number;
//   type: string;
//   deleted?: boolean;
// }

export interface IContentTreeProgression {
  contenttree_id?: number;
}

export interface IContentTreeTuple {
  // contenttree: IContentTree;
  entries: Record<string, INodeTuple>;
  progression: IContentTreeProgression;
  rootElementIds: number[];
  type: string;
  access_date: Date;
  update_date: Date;
  access_sub: number;
  acl: string[];
  configuration: any;
  id: number;
  identifier: string;
}

export type IPath = number[]; //array of numbers (content_ids)
export interface ITreeNodeTuple extends INodeTuple {
  children: ITreeNodeTuple[] | null;
  level: null | number;
  key: null | string;
  nof_children: number;
  nof_children_unread: number;
  nof_children_unrated: number;
  nof_children_alerted: number;
  nof_descendants: number;
  nof_descendants_unread: number;
  nof_descendants_unrated: number;
  nof_descendants_alerted: number;
}
