import { Ref } from 'vue';

export interface IStageTuple {
  stage: IStage;
  progression: IStageProgression;
}

export interface IStage {
  contenttree_id: number;
  date_modified: Date | null;
  title: string;
  disabled: boolean;
  order_position: number;
  extended_order_position: number; // extended with random part
  group: string;
  type: string;
  info: string;
  custom_data: any;
  id: number;
}

export interface IStageProgression {
  stage_id: number;
  number_of_day_sessions: number;
  focused_content_id: number;
  completed?: boolean;
}

export interface IStageGroup {
  name: string;
  disabled?: boolean;
  exact?: boolean;
  label: string;
  toc_label?: string;
  description: string;
  actionText?: string;
  icon: string;
  tooltip?: string;
  expanded?: (item) => boolean;
  expandable?: boolean;
  manual_expanded?: boolean;
  to: () => any; // TODO. add route interface
}

export interface IMilestoneComposable {
  key: string;
  markAchieved: () => void;
  // onAchieved?: () => void;
  // onSkipped?: () => void;
  loading: Ref<boolean>;
  hidden: Ref<boolean>;
  visible: Ref<boolean>;
  achieved: Ref<boolean>;
  skipped: Ref<boolean | undefined>;
  // debugMilestone?: IMilestone;
}

export interface IMilestoneLibrary {
  isMilestoneSet: (key: string) => boolean;
  // debugMilestone?: IMilestone;
}

export interface IMilestone {
  key: string;
  initialize?: () => void; // run when initialized for the first time...
  priors?: IMilestoneComposable[]; // List of prior Milestones to be achieved
  loading?: Ref<boolean>;
  options?: Record<string, unknown>;
  skipped?: Ref<boolean | undefined>;
  achieved?: Ref<boolean>;
  final?: boolean;
}
export type IMilestones = Record<string, IMilestone>;
