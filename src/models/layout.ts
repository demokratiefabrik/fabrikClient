// import { RouteParams } from '@quasar/app';
import { IArtificialModerationGroup } from 'src/pages/components/artificial_moderation/model';
import { RouteLocationRaw } from 'vue-router';
import { IAssembly } from './assembly';

export interface INotificationBanner {
  type: string;
  // 'error' | 'warning';
  title: string;
  body: string;
  icon: string;
  buttons?: string[];
  settimer?: boolean;
  redirectRoute?: RouteLocationRaw;
}

export interface ISideMenuItem {
  label: string;
  anchor: string;
  caption?: string;
  visible?: () => boolean;
}

export type ISideMenuItems = ISideMenuItem[];

export interface IContributionLimits {
  number_of_proposals: {
    daylimit: number;
    overalllimit: number;
    overallCurrent: number;
    current: number;
  };
  number_of_comments: {
    daylimit: number;
    current: number;
  };
}

export interface IDialog {
  type: 'user' | 'profileForm' | 'content' | 'peerreview' | 'certificate';
  id: number | string;
  tab?: string;
  assembly?: IAssembly; // otpional for cross-assembly dialogs...
  contenttreeIdentifier?: string; // optional. in case it is nessacary...
  AM?: IArtificialModerationGroup;
  triggerElementId?: string | undefined;
  focusElementId?: string | undefined;
  focusElementScrollPosition?: number | undefined;
  preventCancel?: boolean;
  replaceCurrent?: boolean;
  forceBackInsteadOfCancel?: boolean;
  openedAsScheduledTask?: boolean;
  dontRouteToAssemblySection?: boolean;
}
