import { INodeTuple } from './content';

export interface IAssemblyTuple {
  access_date: Date;
  access_sub: number;
  assembly: IAssembly;
  progression: IAssemblyProgression;
  configuration: IAssemblyConfiguration;
  stage_keys?: number[];
}

export interface IAssemblyTupleByApi {
  access_date: Date;
  access_sub: number;
  assembly: IAssembly;
  progression: IAssemblyProgression;
  configuration: IAssemblyConfiguration;
  stages: Record<number, number>;
}

export interface IAssembly {
  // contenttree_id: number;
  date_modified: Date | null;
  title: string;
  disabled: boolean;
  // order_position: number;
  // group: string;
  type: string;
  info: string;
  custom_data: any;
  id: number;
  identifier: string;
}

export interface IAssemblyProgression {
  assembly_id: number;
  custom_data: Record<string, any> | null;
  number_of_day_sessions: number;
  number_of_comments_today: number;
  number_of_proposals: number;
  number_of_proposals_today: number;
}

export interface IAssemblyConfiguration {
  STAGE_TYPES: string[];
  MAX_DAILY_USER_COMMENTS: number;
  MAX_DAILY_USER_PROPOSALS: number;
  MAX_OVERALL_USER_PROPOSALS: number;
  TROTTLE_THRESHOLD_FOR_OVERALL_USER_PROPOSALS: number;
}
export interface IAssemblyUserConfiguration {
  // Not specified, yet
  t?: boolean;
}

export interface IAssemblyUserStatistic {
  CCC: number; // Number of created Content (comments and all stuff)
  CRC: number; // Number of rated Content (comments and all stuff)
  DAYS: number; // Number of active Days
  DLI: number; // Days since last interactivity...
  RSA: null; // received Salience Score (Average);
  RSC: null; // received Salience Score (Count);
}

export interface IAssemblyUser {
  id: number;
  date_modified: Date;
  date_created: Date;
  LENGTH: number; // Length of the river
  LANG: string; // lower-letter language_country iso-code: e.g. de_ch
  FN: string; // Fullname of the Pseudonmy-mountain
  U: string; // Username <letter>. <mountain-name>
  CO: string; // hex profile-color
  ROLE?: string; // e.g. Canton of the Pseudonmy-mountain (two letter)
  VAR1?: string; // e.g. Canton of the Pseudonmy-mountain (two letter)
  VAR2?: number; // e.g. ALtitude of the Pseudonmy-mountain
  VAR3?: number; // e.g. Length of the river
}

export interface IAssemblyUserTuple {
  user: IAssemblyUser;
  // topic_contenttree_id: number | null;
  // topic_content_id: number | null;
  topic_tuple?: INodeTuple | null;
  configuration?: IAssemblyUserConfiguration;
  statement_contenttree_identifier?: string | null;
  statement_contenttree_id?: number | null;
  statement_content_id?: number | null;
  statistic?: IAssemblyUserStatistic;
  arguments?: INodeTuple[]; // Rated arguments...
  access_date?: Date;
  access_sub?: number;
}
