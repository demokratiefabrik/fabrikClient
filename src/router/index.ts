import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';
import routes from './routes';
import useEmitter from 'src/utils/emitter';
const emitter = useEmitter();

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE
    ),
  });

  Router.beforeEach((to, from, next) => {
    emitter.emit('routeChange', to);

    // Under Construction
    if (to.params?.assemblyIdentifier) {
      if (parseInt(process.env.ENV_UNDER_CONSTRUCTION as string) == 1) {
        // ALL ASSEMBLY PAGES ARE UNDER CONSTRUCTION
        return next('/pause');
      }
    }

    return next();
  });

  Router.afterEach((to) => {
    emitter.emit('routeChange', to);
  });

  return Router;
});
