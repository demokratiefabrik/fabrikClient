import { RouteRecordRaw } from 'vue-router';
import plugin_routes from './plugin_routes';
import Error404 from 'src/pages/Error404.vue';

// const meta4AssemblyPages = { topmenu: 'assemblies_ongoing_list' }
// const meta4AssemblyDelegates = { assemblyAcl: ['delegate', 'observer']}
// const meta4AssemblyManagers = { assemblyAcl: 'manager' }

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('src/pages/layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/Index.vue') },
      {
        path: '/preview',
        name: 'preview',
        component: () => import('pages/Preview.vue'),
      },
      {
        path: '/authorization',
        props: true,
        name: 'authorization',
        component: () => import('pages/Empty.vue'),
      },
      {
        path: '/logout',
        name: 'logout',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/Auth/Logout.vue'),
      },

      // Encypted Pin (eg.g QC)
      {
        path: '/t/:ttoken',
        name: 'tokenlogin',
        component: () => import('src/utils/VueOAuth2PKCE/TokenLogin.vue'),
      },
      // Email Token
      {
        path: '/l/:ltoken',
        name: 'ltokenlogin',
        component: () => import('src/utils/VueOAuth2PKCE/TokenLogin.vue'),
      },

      {
        path: '/sekretariat',
        name: 'profile',
        props: true,
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/Auth/Profile.vue'),
      },
      {
        path: '/hintergrund',
        name: 'background',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/Background.vue'),
      },
      {
        path: '/news',
        name: 'news',
        component: () => import(/* webpackPrefetch: true */ 'pages/News.vue'),
      },
      {
        path: '/pause',
        name: 'pause',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/Construction.vue'),
      },
      {
        path: '/impressum',
        name: 'impressum',
        component: () =>
          import(/* webpackPrefetch: true */ 'pages/Impressum.vue'),
      },

      ...plugin_routes,

      // {
      //   path: '/:assemblyIdentifier', name: 'assembly_home',
      //   component: () => import('pages/Assembly/AssemblyHome.vue'), meta: { ...meta4AssemblyPages, ...meta4AssemblyDelegates }
      // },

      // {
      //   path: '/:assemblyIdentifier/manage', name: 'assembly_manage',
      //   component: () => import('pages/Assembly/AssemblyManage.vue'), meta: meta4AssemblyManagers
      // },
      // {
      //   path: '/:assemblyIdentifier/manage/tree/:stageID/:contenttreeID', name: 'assembly_manage_tree',
      //   component: () => import('pages/Assembly/AssemblyManageTree.vue'), meta: meta4AssemblyManagers
      // },
      // {
      //   path: '/:assemblyIdentifier/manage/summary', name: 'assembly_manage_summary',
      //   component: () => import('pages/Assembly/AssemblyManageSummary.vue'), meta: meta4AssemblyManagers
      // },

      { path: '/:pathMatch(.*)*', name: 'NotFound', component: Error404 },
    ],
  },
];

// console.log(routes, 'DEBUG ROUTES..');

export default routes;
