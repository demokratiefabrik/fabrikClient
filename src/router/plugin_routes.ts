import { RouteRecordRaw } from 'vue-router';
import cir_routes from 'src/plugins/CIR/routes';
// import ki_routes from 'app/oldPlugins/KI/routes';

const plugin_routes: RouteRecordRaw[] = [];

plugin_routes.push(...cir_routes);
// plugin_routes.push(...ki_routes);

export default plugin_routes;
