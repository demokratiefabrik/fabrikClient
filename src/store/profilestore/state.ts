export default function () {
  return {
    randomSeed: null as number | null,
    profile: {},
    surveyFinalized: false as boolean,
    notifications: {},
    oauthAcls: [],
    AMCache: {},
  };
}
