import useEmitter from 'src/utils/emitter';
const emitter = useEmitter();

export const storeProfile = (state, { data }) => {
  state.profile = data;
};

// export const storeOauthAcls = (state, oauthAcls) => {
//   state.oauthAcls = oauthAcls;
// };

export const set_random_seed = (state) => {
  // console.log('SET RANDOM SEED IF NOT YET DONE')
  if (!state.randomSeed) {
    // TODO: ???? Math.floor(99)
    const randomSeed = Math.floor(Math.random() * Math.floor(99)) + 1;
    state.randomSeed = randomSeed;
  }
};

/* Cache a currently selected AM intervention. */
export const setAMCache = (state, { cacheKey, itemId }) => {
  state.AMCache[cacheKey] = itemId;
};

export const setSurveyFinalized = (state, {}) => {
  state.surveyFinalized = true;
};

export const deleteAMCache = (state) => {
  state.AMCache = {};
};

export const storeNotifications = (state, {}) => {
  state.notifications = {};
};

export const deleteNotification = (state, { notificationID }) => {
  // console.log('deleteNotification 2/2', notificationID);
  delete state.notifications.entries[notificationID];
};

export const set_notification_update_date_to_current = (state) => {
  const now = new Date();
  state.notifications.update_date = now;
};

export const update_notifications = (state, { notifications }) => {
  // console.log('ENTRIES...');
  // NEW ENTRIES
  if (!('entries' in state.notifications)) {
    state.notifications.entries = {};
  }
  Object.values(notifications).forEach((notification: any) => {
    state.notifications.entries[notification.id] = notification;
    // IN CASE WE HAVE CERTIFICATE NOTIFICATIONS
    if (notification.action == 'CERTIFICATE_ACCOMPLISHED') {
      if (!state.profile.user.CERTIFICATES) {
        state.profile.user.CERTIFICATES = [];
      }
      if (!state.profile.user.CERTIFICATES.includes(notification.value)) {
        // console.log('DEBUG: ADD NOTIFICATION', notification.value);
        state.profile.user.CERTIFICATES.push(notification.value);

        if (!notification.is_read) {
          // launch notification action, in case its the first notification of this kind...
          // if (notification.call_action_when_initialized) {
          // TODO: remove this line!!!!
          // console.log('DEBUG - NOTIFICATION ACTION !!!!!!', notification);
          emitter.emit('notificationAction', notification);
        }
        // }
      }
    }
  });
};
