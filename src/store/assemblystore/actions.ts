import useEmitter from 'src/utils/emitter';
const emitter = useEmitter();
import api from 'src/utils/api';
import { IAssemblyTupleByApi } from 'src/models/assembly';

export const syncAssembly = (
  { state, dispatch },
  { oauthUserID, assemblyIdentifier, forceSyncDate }
) => {
  // console.log(` sync assembly ${assemblyIdentifier}`);
  if (!state.assemblydata[assemblyIdentifier]) {
    // no cached version exists: load the data from resource server...
    dispatch('retrieveAssembly', { assemblyIdentifier: assemblyIdentifier });
    // console.log(' not yet fetched...')
    return null;
  }

  // wrong user? and renew cache all x- minutes!
  const wrongUser =
    oauthUserID != state.assemblydata[assemblyIdentifier].access_sub;
  const expired =
    !state.assemblydata[assemblyIdentifier] ||
    api.expiredCacheDate(
      state.assemblydata[assemblyIdentifier].access_date,
      forceSyncDate
    );
  // console.log()
  if (expired || wrongUser) {
    // console.log(
    //   ' Assembly not in sync  or wrong user...',
    //   expired,
    //   state.assemblydata[assemblyIdentifier].access_date
    // );
    dispatch('retrieveAssembly', { assemblyIdentifier: assemblyIdentifier });
    return null;
  }

  // console.log("AssemblyLoaded: Assembly retrieved from localStorage")

  emitter.emit('AssemblyLoaded');
  return null;
};

export const deleteAssemblyStore = ({ commit }) => {
  commit('deleteAssemblyStore');
};

export const deleteSessionData = ({ commit }) => {
  commit('deleteSessionData');
};

export const retrieveAssembly = ({ commit }, { assemblyIdentifier }) => {
  if (!assemblyIdentifier) {
    return null;
  }

  // console.log('Retrieve assembly from resource server', assemblyIdentifier);
  api
    .retrieveAssembly(assemblyIdentifier)
    .then((response) => {
      const data = response.data as IAssemblyTupleByApi;
      // console.log('DEBUG after retreieveAssembly,', data, 'emitter error?');
      console.assert(data);
      commit('storeAssembly', { assemblyIdentifier, data });
      emitter.emit('AssemblyLoaded');
    })
    .catch((error) => {
      console.warn(error);
      // Error Handling is done in Axios Interceptor
      console.warn('Request Error');
    });
};

export const retrieveAssemblyUser = (
  { state, commit },
  { assemblyIdentifier, userID }
): Promise<boolean> => {
  if (!assemblyIdentifier) {
    return Promise.resolve(false);
  }

  if (userID in state.users) {
    const expired = api.expiredCacheDate(state.users[userID].access_date);
    if (expired) {
      return Promise.resolve(true);
    }
  }

  // console.log('Retrieve assembly from resource server', assemblyIdentifier);
  return api
    .retrieveAssemblyUser(assemblyIdentifier, userID)
    .then((response) => {
      const ok = !!response.data.user;
      commit('storeAssemblyUserObject', {
        assemblyIdentifier,
        userTuple: response.data,
      });
      return Promise.resolve(ok);
    })
    .catch(() => {
      // this.showNetworkError('Da ist ein Fehler aufgetreten')
      return Promise.reject();
    });
};

export const addOrUpdateStage = ({ commit }, { assemblyIdentifier, stage }) => {
  // console.log('stage: ', stage, assemblyIdentifier);
  api
    .addOrUpdateStage(assemblyIdentifier, stage)
    .then((response) => {
      // console.log(response.data);
      const data = response.data as IAssemblyTupleByApi;

      // Store result
      console.assert(!!data.assembly);
      console.assert(!!data.stages);
      commit('storeAssemblyStage', { assemblyIdentifier, data });
    })
    .catch((error) => {
      console.warn(error);
      // Error Handling is done in Axios Interceptor
      console.warn('Request Error');
    });
};

export const updateAssembly = ({ commit }, { assembly }) => {
  const assemblyIdentifier: string = assembly.identifier;
  api
    .updateAssembly(assemblyIdentifier, assembly)
    .then((response) => {
      // console.log(response.data);
      const data = response.data;

      // Store result
      console.assert(!!data.assembly);
      commit('storeAssemblyObject', {
        assemblyIdentifier,
        assembly: data.assembly,
      });
    })
    .catch((error) => {
      console.warn(error);
      // Error Handling is done in Axios Interceptor
      console.warn('Request Error');
    });
};

export const addMilestone = ({ getters, commit }, { stageID, key }) => {
  console.assert(key);
  // console.log('ADD MILESTONE', ' KEY:', key);

  const day = getters.assemblyProgression?.number_of_day_sessions;
  const milestones = getters.stageMilestones;
  console.assert(milestones !== undefined);
  if (milestones) {
    if (key in milestones) {
      // milestone has been already archieved before
      return false;
    }
  }

  // INSERT NEW MILESTONE
  commit('addMilestone', { key, day, stageID });
  return true;
};

export const addSessionData = ({ getters, commit }, { stageID, key, data }) => {
  console.assert(key);
  // console.log('ADD sessionData', ' KEY:', key);
  const day = getters.assemblyProgression?.number_of_day_sessions;
  // console.log(data, 'data 9039232222');

  // Upsert SessionData
  commit('addSessionData', { key, day, stageID, data });
  return true;
};

export const incrementAssemblyActivityCounter = (
  { commit },
  { assemblyIdentifier, counterName }
) => {
  console.assert(
    ['number_of_proposals_today', 'number_of_comments_today'].includes(
      counterName
    )
  );
  commit('incrementAssemblyActivityCounter', {
    assemblyIdentifier,
    counterName,
  });
};

export const updateFocusedContent = ({ commit }, { stageID, contentID }) => {
  console.assert(stageID);
  console.assert(contentID);
  commit('updateFocusedContent', { stageID, contentID });
};

export const storeStageProgressionAlertFlag = (
  { commit },
  { stageID, alerted }
) => {
  console.assert(stageID);
  commit('storeStageProgressionAlertFlag', { stageID, alerted });
};

export const storeStageProgressionCompleted = (
  { commit },
  { stageID, completed }
) => {
  console.assert(stageID);
  commit('storeStageProgressionCompleted', { stageID, completed });
};
