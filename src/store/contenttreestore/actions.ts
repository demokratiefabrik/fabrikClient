import api from 'src/utils/api';

/* Retrieve new version of the contenttree <assemblyIdentifier>.<contenttreeIdentifier>
  If timelag is set to true, the method will wait a few seconds to give priority the other 
  API-Requests. (e.g. RetrieveAssembly)
  TODO: is there a better way to queue API Requests. 
  (Consider also the problem of oAuth2 Token refreshs, that are then issued twice..)
  */
export const retrieveContenttree = (
  { commit },
  { assemblyIdentifier, contenttreeIdentifier, timelag }
) => {
  // console.log('with timelag', timelag);
  const timeout = timelag ? 1000 : 0;
  setTimeout(() => {
    // console.log('Retrieve contenttree from resource server' + contenttreeIdentifier)
    console.assert(contenttreeIdentifier);
    api
      .retrieveContenttree(assemblyIdentifier, contenttreeIdentifier)
      .then((response) => {
        // update
        // console.log('save full contenttree to cache.')
        console.assert(response.data);
        console.assert('OK' in response.data);
        console.assert('contenttree' in response.data);
        const configuration =
          'configuration' in response.data ? response.data.configuration : null;

        commit('add_or_update_contenttree', {
          contenttreeIdentifier: contenttreeIdentifier,
          contenttree: response.data.contenttree,
          configuration: configuration,
        });
      })
      .catch((error) => {
        console.warn('Request Error', error);
      });
  }, timeout);
};

/* Retrieve modifed content of the contenttree <assemblyIdentifier>.<contenttreeIdentifier>
Timelag: postpone update call by one second...
  */
export const updateContenttree = (
  { commit, dispatch },
  { assemblyIdentifier, contenttreeIdentifier, update_date, timelag }
) => {
  commit('set_update_date_to_current', { contenttreeIdentifier });

  const timeout = timelag ? 1000 : 0;
  setTimeout(() => {
    console.assert(contenttreeIdentifier);
    // assemblyIdentifier, contenttreeIdentifier, update_date
    api
      .updateContenttree(assemblyIdentifier, contenttreeIdentifier, update_date)
      .then((response) => {
        console.assert(response?.data);
        console.assert('OK' in response.data);
        const modifiedContents = response.data.contents;
        if (modifiedContents && Object.values(modifiedContents).length) {
          // console.log('STORE MODIFIED CONTENT TO DISK', modifiedContents);
          dispatch('update_contents', {
            modifiedContents,
          });
        }
      })
      .catch((error) => {
        console.warn('Request Error', error);
      });
  }, timeout);
};

export const add_or_update_contenttree = (
  { commit },
  { contenttreeIdentifier, contenttree, configuration }
) => {
  commit('add_or_update_contenttree', {
    contenttreeIdentifier,
    contenttree,
    configuration,
  });
};

export const update_contents = ({ commit, getters }, { modifiedContents }) => {
  // in case content or progression changes (without changing hirarchy...)
  // console.log('DEBUG: update_contents MODIFIED CONTENTS (2)', modifiedContents);
  console.assert(modifiedContents !== undefined);
  console.assert(modifiedContents !== null);
  const contentIdsToUpdateStructure = {};
  for (const contentTuple of Object.values(modifiedContents)) {
    console.assert(contentTuple !== undefined);
    console.assert(contentTuple !== null);

    // collect all Contents with parent_id modifications (grouped by contenttree)
    const contenttreeIdentifier = (contentTuple as any).content
      .contenttree_identifier;
    const pathChanged = getters.monitorPathChange({ contentTuple });
    // console.log(
    //   'DEBUG;;;; path changed',
    //   pathChanged,
    //   contentTuple,
    //   (contentTuple as any)?.content?.parent_id,
    //   (contentTuple as any)?.path
    // );
    if (pathChanged) {
      if (!contentIdsToUpdateStructure[contenttreeIdentifier]) {
        contentIdsToUpdateStructure[contenttreeIdentifier] = [];
      }
      contentIdsToUpdateStructure[contenttreeIdentifier].push(
        (contentTuple as any).content.id
      );
    }
    // console.log("contentIdsToUpdateStructure", contentIdsToUpdateStructure, "llllllllllll")
    commit('update_content', { contentTuple });
  }

  // UPDATE TREE STRUCTURE of parentID modified contents (grouped by contenttree)
  if (Object.values(contentIdsToUpdateStructure).length) {
    Object.entries(contentIdsToUpdateStructure).forEach(
      ([contenttreeIdentifier, contentIdsToUpdate]) => {
        // console.log(
        //   'PATH STRUCTURE IS TO UPDATE FOR ',
        //   contentIdsToUpdate,
        //   ' contents'
        // );
        console.assert(contenttreeIdentifier);
        // dispatch('updateTreeStructure', { contenttreeIdentifier, contentIdsToUpdate });
        commit('updateTreeStructure', {
          contenttreeIdentifier,
          contentIdsToUpdate,
        });
      }
    );
  }
};

export const update_content = ({ commit, getters }, { contentTuple }) => {
  // in case content or progression changes (without changing hirarchy...)
  console.assert(contentTuple !== undefined);
  console.assert(contentTuple !== null);
  // console.log("SEND TO mutate", contentTuple);

  commit('update_content', { contentTuple });

  const pathChanged = getters.monitorPathChange({ contentTuple });
  // console.log(
  //   'DEBUG;;;; path changed',
  //   pathChanged,
  //   contentTuple,
  //   contentTuple.content.parent_id,
  //   contentTuple.path
  // );
  if (pathChanged) {
    const contenttreeIdentifier = contentTuple.content.contenttree_identifier;
    console.assert(contenttreeIdentifier);
    commit('updateTreeStructure', {
      contenttreeIdentifier,
      contentIdsToUpdate: [contentTuple.content.id],
    });
  }
};
export const delete_contenttree = ({ commit }, { contenttreeIdentifier }) => {
  commit('delete_contenttree', { contenttreeIdentifier });
};
export const delete_all_subtree = ({ commit }, {}) => {
  commit('delete_all_subtree', {});
};

export const update_rating = (
  { commit },
  { contenttreeIdentifier, contentID, topicID, rating }
) => {
  commit('update_rating', { contenttreeIdentifier, contentID, rating });
  if (topicID && topicID !== contentID) {
    // console.log('update_discussed...topicID', topicID);
    commit('update_discussed', { contenttreeIdentifier, contentID: topicID });
  }
};

export const update_review = (
  { commit },
  { contenttreeIdentifier, contentID, reviewed }
) => {
  commit('update_review', { contenttreeIdentifier, contentID, reviewed });
};

export const update_salience = (
  { commit },
  { contenttreeIdentifier, contentID, topicID, salience }
) => {
  commit('update_salience', { contenttreeIdentifier, contentID, salience });
  if (topicID && topicID !== contentID) {
    commit('update_discussed', { contenttreeIdentifier, contentID: topicID });
  }
};

export const update_certainity = (
  { commit },
  { contenttreeIdentifier, contentID, topicID, certainity }
) => {
  commit('update_certainity', { contenttreeIdentifier, contentID, certainity });
  if (topicID && topicID !== contentID) {
    commit('update_discussed', { contenttreeIdentifier, contentID: topicID });
  }
};

export const update_read = (
  { commit },
  { contenttreeIdentifier, contentID }
) => {
  commit('update_read', { contenttreeIdentifier, contentID });
};

export const set_flag_for_peerreview_updates = (
  { commit },
  { contenttreeIdentifier, contentID, peerreviewID }
) => {
  commit('set_flag_for_peerreview_updates', {
    contenttreeIdentifier,
    contentID,
    peerreviewID,
  });
};

export const update_discussed = (
    { getters, commit },
    { contenttreeIdentifier, contentID }
  ) => {
    console.assert(contentID);
    // console.log(
    //   'first step in updat_discussion',
    //   contenttreeIdentifier,
    //   contentID
    // );
    // while (contentID) {

    const content = getters.get_content({ contenttreeIdentifier, contentID });
    if (content?.progression?.discussed) {
      // already discussed...
      return;
    }
    commit('update_discussed', { contenttreeIdentifier, contentID });
    // }
  },
  update_expanded_branches = (
    { commit },
    { contenttreeIdentifier, contenttreeRootNodeID, expanded }
  ) => {
    // console.log(expanded)
    commit('update_expanded_branches', {
      contenttreeIdentifier,
      contenttreeRootNodeID,
      expanded: expanded as string[],
    });
  };

export const deleteContentStore = ({ commit }) => {
  commit('deleteContentStore');
};

export const syncContenttree = (
  { commit, dispatch, state },
  { assemblyIdentifier, contenttreeIdentifier, oauthUserID, forceSyncDate }
) => {
  // empty contenttree?
  const emptyContenttree = !state.contenttree[contenttreeIdentifier];

  // wrong user? and renew cache all x- minutes!
  const wrongUser =
    !emptyContenttree &&
    oauthUserID != state.contenttree[contenttreeIdentifier]?.access_sub;

  if (wrongUser) {
    // DELETE CACHE IMMEDIATELY...
    commit('add_or_update_contenttree', {
      contenttreeIdentifier,
      contenttree: {},
      configuration: {},
    });
  }
  // console.log(
  //   'EXPIRED DATE',
  //   state.contenttree[contenttreeIdentifier]?.access_date
  // );
  const expired =
    !emptyContenttree &&
    !wrongUser &&
    api.expiredCacheDate(
      state.contenttree[contenttreeIdentifier]?.access_date,
      forceSyncDate
    );

  if (!expired && !emptyContenttree && !wrongUser) {
    // CACHE IS UP TO DATE!
    dispatch('checkToUpdateContenttree', {
      assemblyIdentifier,
      contenttreeIdentifier,
    });
    return true;
  }

  dispatch('retrieveContenttree', {
    assemblyIdentifier: assemblyIdentifier,
    contenttreeIdentifier: contenttreeIdentifier,
    timelag: !emptyContenttree && !wrongUser,
  });
};

export const checkToUpdateContenttree = (
  { state, dispatch },
  { assemblyIdentifier, contenttreeIdentifier }
) => {
  // Check for new content / Modified content, from time to time...
  // console.log(
  //   'DEBUG: update date?..',
  //   contenttreeIdentifier,
  //   state.contenttree[contenttreeIdentifier],
  //   state.contenttree[contenttreeIdentifier]?.update_date
  // );
  // console.log('DEBUG', contenttreeIdentifier);
  const update_date = state.contenttree[contenttreeIdentifier]?.update_date;

  if (update_date) {
    const expiredUpdateTime = api.expiredUpdateDate(update_date);
    if (!expiredUpdateTime) {
      return true;
    }
  }

  console.assert(!!update_date);
  // if (!update_date) {
  //   // update_date = "2000-00-00";
  //   update_date = new Date(new Date().setDate(new Date().getDate() - 1)); // minus one day
  // }
  // EMPTY UPDATE_DATE
  // if (!update_date) {
  //   // retrieveContenttree;
  //   dispatch('retrieveContenttree', {
  //     assemblyIdentifier: assemblyIdentifier,
  //     contenttreeIdentifier: contenttreeIdentifier,
  //     timelag: true,
  //   });
  // } else {
  // too old or missing cache: load the data from resource server...
  // console.log('Its update time: check for modified / new contents');
  dispatch('updateContenttree', {
    assemblyIdentifier: assemblyIdentifier,
    contenttreeIdentifier: contenttreeIdentifier,
    update_date,
    timelag: true,
  });
  // }
};
