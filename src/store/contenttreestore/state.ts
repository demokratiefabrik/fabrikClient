import { IContentTreeTuple } from 'src/models/contenttree';

export default function () {
  return {
    contenttree: {} as Record<string, IContentTreeTuple>,
    expanded_branches: {} as Record<string, string[]>,
  };
}
