export const add_or_update_contenttree = (
  state,
  { contenttreeIdentifier, contenttree, configuration }
) => {
  // keep list of opened contents (if previously available)
  // console.log('update contenttree')
  let configuration_old = null;
  // const expanded_old = null;

  if (contenttreeIdentifier in state.contenttree) {
    // TODO: is this intentionally disabled?
    // expanded_old = state.contenttree[contenttreeIdentifier].expanded_by_default
    configuration_old = state.contenttree[contenttreeIdentifier].configuration;
  }
  // console.log(configuration)

  contenttree.configuration = configuration ? configuration : configuration_old;
  // if (expanded_old) {
  //   contenttree.expanded = expanded_old;
  // }
  // console.log(contenttree)
  // console.log('new copy saved...')
  state.contenttree[contenttreeIdentifier] = contenttree;
};

export const update_content = (state, { contentTuple }) => {
  // in case content or progression changes (without changing hirarchy...)
  console.assert(contentTuple !== undefined);
  console.assert(contentTuple !== null);

  const contenttreeIdentifier = contentTuple.content.contenttree_identifier;
  const contentID = contentTuple.content.id;

  // NEW ENTRIES
  // console.log('UPDATE CONTENT STORE', contentTuple);

  if (!(contentID in state.contenttree[contenttreeIdentifier].entries)) {
    console.assert(contentTuple.content);
    console.assert(contentTuple.creator);
    state.contenttree[contenttreeIdentifier].entries[contentTuple.content.id] =
      {
        progression: contentTuple.progression,
        content: contentTuple.content,
        creator: contentTuple.creator,
        path: contentTuple.path,
      };
  } else {
    if (contentTuple.progression) {
      state.contenttree[contenttreeIdentifier].entries[contentID].progression =
        contentTuple.progression;
    }
    if (contentTuple.content) {
      state.contenttree[contenttreeIdentifier].entries[contentID].content =
        contentTuple.content;
    }
    if (contentTuple.creator) {
      state.contenttree[contenttreeIdentifier].entries[contentID].creator =
        contentTuple.creator;
    }
    if (contentTuple.path) {
      state.contenttree[contenttreeIdentifier].entries[contentID].path =
        contentTuple.path;
    }
  }

  // console.log('DEBUG 33:', contentTuple?.path);
};

export const updateTreeStructure = (
  state,
  { contenttreeIdentifier, contentIdsToUpdate }
) => {
  // console.log(
  //   'DEBUG: content that have changed their parent object!!!',
  //   contentIdsToUpdate
  // );
  const values = Object.values(
    state.contenttree[contenttreeIdentifier].entries
  );
  const contentsToUpdate = values.filter((contentTuple: any) => {
    return (
      !contentTuple.path ||
      contentTuple.path.some((r) => contentIdsToUpdate.includes(r))
    );
  });

  // console.log(
  //   'content that is affected by parent_id modifications',
  //   contentsToUpdate
  // );
  contentsToUpdate.forEach((contentTuple: any) => {
    // console.log('UPDATE PATH FOR FOLLOGING CONTENTS', contentIdsToUpdate);
    let antecesdentID = contentTuple.content.id;
    let antecesdent =
      state.contenttree[contenttreeIdentifier].entries[antecesdentID];
    let rootElementIds =
      state.contenttree[contenttreeIdentifier].rootElementIds;
    const isAmongRootElementIds = rootElementIds.includes(
      contentTuple.content.id
    );
    console.assert(!antecesdentID || antecesdent);
    const path: number[] = [];
    while (antecesdent) {
      path.unshift(antecesdentID);
      // GET new Ancestore
      antecesdentID = antecesdent.content.parent_id;
      if (antecesdentID) {
        antecesdent =
          state.contenttree[contenttreeIdentifier].entries[antecesdentID];
      } else {
        antecesdent = null;
      }
      console.assert(!antecesdentID || antecesdent);
    }
    state.contenttree[contenttreeIdentifier].entries[
      contentTuple.content.id
    ].path = path;

    // console.log('DEBUG: new path', path);

    // Update RootNode Ids...
    if (contentTuple.content.parent_id && isAmongRootElementIds) {
      // REMOVE
      rootElementIds = rootElementIds.filter(
        (x) => x !== contentTuple.content.id
      );
      state.contenttree[contenttreeIdentifier].rootElementIds = rootElementIds;
    }
    if (!contentTuple.content.parent_id && !isAmongRootElementIds) {
      // ADD
      rootElementIds.push(contentTuple.content.id);
      state.contenttree[contenttreeIdentifier].rootElementIds = rootElementIds;
      // console.log('add to root id');
    }
    // console.log('CURRENT ROOT ELEMENTS:, ', rootElementIds);
  });
};

export const update_expanded_branches = (
  state,
  { contenttreeIdentifier, contenttreeRootNodeID, expanded }
) => {
  // console.log(contenttreeIdentifier, 'contenttreeIdentifier in expa branch');
  const key = contenttreeIdentifier + '-' + contenttreeRootNodeID;
  state.expanded_branches[key] = expanded as string[];
};
export const delete_all_subtree = (state, {}) => {
  const keys = Object.keys(state.contenttree);
  console.log('ddd');
  const subtree_keys = keys.filter((k) => k.includes('-'));
  if (subtree_keys?.length) {
    subtree_keys.forEach((sk) => {
      delete state.contenttree[sk];
    });
  }
};
export const delete_contenttree = (state, { contenttreeIdentifier }) => {
  if (state.contenttree[contenttreeIdentifier]) {
    delete state.contenttree[contenttreeIdentifier];
  }
};

export const set_update_date_to_current = (
  state,
  { contenttreeIdentifier }
) => {
  const now = new Date();
  state.contenttree[contenttreeIdentifier].update_date = now;
};

export const update_rating = (
  state,
  { contenttreeIdentifier, contentID, rating }
) => {
  // in case content or progression changes (without changing hierarchy...)
  if (rating === null || rating === undefined) {
    return null;
  }

  let progression =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.progression;
  if (!progression) {
    progression = {
      read: true,
      discussed: true,
    };
  }

  // store value
  progression.rating = rating;

  state.contenttree[contenttreeIdentifier].entries[contentID].progression =
    progression;
};

export const update_review = (
  state,
  { contenttreeIdentifier, contentID, reviewed }
) => {
  // in case content or progression changes (without changing hierarchy...)
  if (reviewed === undefined) {
    return null;
  }

  const content =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.content;
  console.assert(content);

  // store value
  content.reviewed = reviewed;

  state.contenttree[contenttreeIdentifier].entries[contentID].content = content;
};

export const update_read = (state, { contenttreeIdentifier, contentID }) => {
  let progression =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.progression;
  if (!progression) {
    progression = {
      read: false,
      discussed: false,
      rating: null,
      salience: null,
      certainity: null,
    };
  }

  // store value
  progression.read = true;

  state.contenttree[contenttreeIdentifier].entries[contentID].progression =
    progression;
};

export const set_flag_for_peerreview_updates = (
  state,
  { contenttreeIdentifier, contentID, peerreviewID }
) => {
  const content =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.content;
  if (!content) {
    throw 'Invalid content';
  }

  // store value
  content.pending_peerreview_for_update = peerreviewID;
  state.contenttree[contenttreeIdentifier].entries[contentID].content = content;
};

export const update_discussed = (
  state,
  { contenttreeIdentifier, contentID }
) => {
  // console.log("update_discussed ############", contentID, contenttreeIdentifier)
  let progression =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.progression;
  if (!progression) {
    progression = {
      read: true,
      discussed: true,
      rating: null,
      salience: null,
      certainity: null,
    };
    // console.log('add progressison', progression);
    state.contenttree[contenttreeIdentifier].entries[contentID].progression =
      progression;
  } else {
    // console.log('update.. progressison discussed', contentID);
    state.contenttree[contenttreeIdentifier].entries[
      contentID
    ].progression.discussed = true;
  }
};

export const update_salience = (
  state,
  { contenttreeIdentifier, contentID, salience }
) => {
  if (salience === undefined) {
    return null;
  }

  // TARGET
  let progression =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.progression;
  if (!progression) {
    progression = {
      read: true,
      salience: salience,
      discussed: true,
      view: false,
    };
    state.contenttree[contenttreeIdentifier].entries[contentID].progression =
      progression;
  } else {
    // console.log('update.. progressison salienced', contentID, salience);

    state.contenttree[contenttreeIdentifier].entries[
      contentID
    ].progression.salience = salience;
  }
};

export const update_certainity = (
  state,
  { contenttreeIdentifier, contentID, certainity }
) => {
  if (certainity === undefined) {
    return null;
  }

  // TARGET
  let progression =
    state.contenttree[contenttreeIdentifier]?.entries[contentID]?.progression;
  if (!progression) {
    progression = {
      read: true,
      certainity,
      discussed: true,
      view: false,
    };
    state.contenttree[contenttreeIdentifier].entries[contentID].progression =
      progression;
  } else {
    // console.log(
    //   'update.. progressison indicate certainity',
    //   contentID,
    // certainity
    // );

    state.contenttree[contenttreeIdentifier].entries[
      contentID
    ].progression.certainity = certainity;
  }
};

export const deleteContentStore = (state) => {
  state.contenttree = {};
  state.expanded_branches = {};
};
