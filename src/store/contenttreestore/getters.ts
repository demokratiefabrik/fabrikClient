import { INodeTuple } from 'src/models/content';

export const get_available_contenttree_identifiers = (state) => {
  if (!Object.keys(state.contenttree)) {
    return null;
  }

  return Object.keys(state.contenttree);
};

export const get_contenttree =
  (state) =>
  ({ contenttreeIdentifier }) => {
    if (!Object.keys(state.contenttree).includes(contenttreeIdentifier)) {
      return null;
    }

    return state.contenttree[contenttreeIdentifier];
  };

export const get_content =
  (state) =>
  ({ contenttreeIdentifier, contentID }) => {
    // console.log('DEBUG 98', contenttreeIdentifier, contentID);
    const nodeTuple: INodeTuple =
      state.contenttree[contenttreeIdentifier]?.entries[contentID];
    return nodeTuple;
  };

export const get_content_text_max_length =
  (state) =>
  ({ contenttreeIdentifier, type }) => {
    const MAX_LENGTHS =
      state.contenttree[contenttreeIdentifier].configuration
        .CONTENT_TEXT_MAX_LENGTH_BY_TYPES;
    if (type in MAX_LENGTHS) {
      return MAX_LENGTHS[type];
    }
    return state.contenttree[contenttreeIdentifier].configuration
      .DEFAULT_CONTENT_TEXT_MAX_LENGTH;
  };

export const get_content_title_max_length =
  (state) =>
  ({ contenttreeIdentifier, type }) => {
    const MAX_LENGTHS =
      state.contenttree[contenttreeIdentifier].configuration
        .CONTENT_TITLE_MAX_LENGTH_BY_TYPES;
    if (type in MAX_LENGTHS) {
      return MAX_LENGTHS[type];
    }
    return state.contenttree[contenttreeIdentifier].configuration
      .DEFAULT_CONTENT_TITLE_MAX_LENGTH;
  };

export const get_allowed_node_types =
  (state) =>
  ({ contenttreeIdentifier, parentType }) => {
    const contenttree = state.contenttree[contenttreeIdentifier];
    if (!contenttree) {
      throw 'could not load contenttree';
    }

    const parentTypeString: string = parentType === null ? 'null' : parentType;
    // console.log(contenttree.configuration);
    if (!contenttree.configuration.ONTOLOGY) {
      // console.log(contenttree.configuratio);
      throw 'unknown contenttree ONTOLOGY';
    }
    if (
      !Object.keys(contenttree.configuration.ONTOLOGY).includes(
        parentTypeString
      )
    ) {
      throw `incompatible parentType for this contenttree ontology. Type: ${parentTypeString}`;
    }

    // Allowed types by ontology
    let types = [];
    if (parentType === undefined) {
      types = contenttree.configuration.CONTENTTYPES;
    } else {
      types = contenttree.configuration.ONTOLOGY[parentTypeString];
    }

    return types;
  };

export const get_allowed_node_types_to_add =
  (state, getters) =>
  ({ contenttreeIdentifier, parentType }) => {
    // Content Types for this position
    const context_node_types = getters.get_allowed_node_types({
      contenttreeIdentifier,
      parentType,
    });

    // FILTER THE TYPES, THE USER HAS ADD PERMISSION
    // get types (considering current permissions)
    const allowedContentTypesToAdd = getters.get_node_types_with_add_permission(
      {
        contenttreeIdentifier,
      }
    );

    // Reduce to overlapp
    return context_node_types.filter((value: string) => {
      // console.log(allowedContentTypesToAdd, 'allowedContentTypesToAdd');
      return allowedContentTypesToAdd.includes(value);
    });
  };

export const get_node_types_with_add_permission =
  (state) =>
  ({ contenttreeIdentifier }) => {
    return state.contenttree[contenttreeIdentifier].configuration
      .ALLOWED_CONTENT_TYPES_TO_ADD;
  };

export const getExpandedBranches =
  (state) =>
  ({ contenttreeIdentifier, contenttreeRootNodeID }): string[] | null => {
    const key = contenttreeIdentifier + '-' + contenttreeRootNodeID;
    if (!(key in state.expanded_branches)) {
      return null;
    }
    return state.expanded_branches[key];
  };

export const isCommonPropertyByConentType =
  (state) =>
  ({ contenttreeIdentifier, type }) => {
    if (
      !state.contenttree[
        contenttreeIdentifier
      ].configuration.CONTENTTYPES.includes(type)
    ) {
      return null;
    }

    return state.contenttree[
      contenttreeIdentifier
    ].configuration.COMMON_PROPERTY_CONTENT.includes(type);
  };

export const monitorPathChange =
  (state) =>
  ({ contentTuple }) => {
    // return yes, if parent_id changed or if path is not yet set.
    console.assert(
      contentTuple && contentTuple.content && contentTuple.content.id
    );
    const contenttreeIdentifier = contentTuple.content.contenttree_identifier;
    // console.log(contenttreeIdentifier);
    console.assert(contenttreeIdentifier);
    const previousParentID =
      state.contenttree[contenttreeIdentifier].entries[contentTuple.content.id]
        ?.content?.parent_id;
    return previousParentID !== contentTuple.content.parent_id;
  };
