// import { IStageGroup } from 'src/composables/stage.composable';
// import { Ref} from 'vue';
import { INodeTuple } from 'src/models/content';
// import { IStageTuple } from 'src/models/stage';

export interface ICtx {
  // groupsScheduled: IStageGroup[];
  // assemblyMenuItems: IStageGroup[];
  unratedStatement: boolean;
  statementContentTuple: INodeTuple;
  // nextScheduledStageGroup: IStageGroup;
  // gotoStage: (IStageTuple) => void;
  // [x: string]: any,
}

const AMs = {
  statementRating: {
    id: 'statementRating',
    prosa: ' Direkt unter dem Statement.',
    loading: (ctx: ICtx) => !ctx.statementContentTuple?.content,
    items: [
      {
        id: 1,
        prosa: ' ... Bitte bewerten sie das statment.',
        condition: (ctx: ICtx) => ctx.unratedStatement,
        body: () =>
          'Was denken Sie über diese Stellungsnahme? Bitte bewerten Sie sie.',
      },
    ],
  },
};

export default AMs;
