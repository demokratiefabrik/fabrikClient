/** DEMOKRATIFABRIK RUNTIME VARIABLES */

import useAssemblyComposable from 'src/composables/assembly.composable';
// import constants from 'src/utils/constants';
import useStageComposable from 'src/composables/stage.composable';
import { IStageGroup } from 'src/models/stage';

export default function useKIComposable() {
  const { getStageRoute } = useAssemblyComposable('');
  const { getFirstOrRoutedStageIDByGroup } = useStageComposable();

  const assemblyMenuData: Record<string, IStageGroup> = {
    // preparation: {
    //   name: 'preparation',
    //   label: '',
    //   exact: true,
    //   toc_label: 'Vorbereitung',
    //   description:
    //     'Sie erhalten hier die wichtigsten Informationen zur Demokratiefabrik. Umgekehrt benötigen wir auch einige Angaben von Ihnen.',
    //   icon: 'mdi-forwardburger',
    //   tooltip: 'Sie finden hier das heutige Demokratiefabrik-Programm.',
    //   expanded: (item) =>
    //     nextScheduledStage?.value?.stage.group == 'preparation' ||
    //     item.manual_expanded,
    //   expandable: true,
    //   manual_expanded: false,
    //   to: () => {
    //     return {
    //       name: 'KI',
    //       params: {
    //         assemblyIdentifier: assemblyIdentifier.value,
    //       },
    //     };
    //   },
    // },

    // information: {
    //   name: 'information',
    //   label: 'Vorlage',
    //   icon: 'mdi-sign-direction',
    //   // constants.ICONS.VAA_TOPIC,
    //   description: 'Erfahren Sie alles über das Beteiligungsprojekt',
    //   tooltip: 'Erfahren Sie alles über das Beteiligungsprojekt',
    //   expanded: () => false,
    //   expandable: false,
    //   to: () => {
    //     const firstStageTuple = getFirstOrRoutedStageIDByGroup('information');
    //     return getStageRoute(firstStageTuple);
    //   },
    // },

    // TODO: random ordering: either with dynamic Caption, or with dynamic ordering...
    context1: {
      name: 'context1',
      label: 'KI in der Pflege',
      icon: 'mdi-hospital-box-outline',
      description:
        'Diskutieren Sie über die Verwendung von künstlicher Intelligenz in der Pflege',
      tooltip:
        'Diskutieren Sie über die Verwendung von künstlicher Intelligenz in der Pflege',
      expanded: () => false,
      // nextScheduledStage?.value?.stage.group == 'voice' ||
      // item.manual_expanded,
      expandable: false,
      // manual_expanded: false,
      to: () => {
        // is this group scheduled?
        // if (nextScheduledStage?.value?.stage.group === 'voice') {
        //   // console.log(getStageRoute(nextScheduledStage.value));
        //   // return getStageRoute(nextScheduledStage.value);
        // } else {
        //   const firstStageTuple = getFirstOrRoutedStageIDByGroup('voice');
        //   console.log(getStageRoute(firstStageTuple), 'llllllllllllllll');
        //   // return getStageRoute(firstStageTuple);
        // }
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('context1');
        return getStageRoute(firstStageTuple);
      },
    },

    context2: {
      name: 'context2',
      label: 'KI bei Bewerbungen',
      icon: 'mdi-folder-account-outline',
      description:
        'Diskutieren Sie über die Verwendung von künstlicher Intelligenz bei der Beurteilung von Bewerbungen',
      tooltip:
        'Diskutieren Sie über die Verwendung von künstlicher Intelligenz bei der Beurteilung von Bewerbungen',
      expanded: () => false,
      // nextScheduledStage?.value?.stage.group == 'voice' ||
      // item.manual_expanded,
      expandable: false,
      // manual_expanded: false,
      to: () => {
        // is this group scheduled?
        // if (nextScheduledStage?.value?.stage.group === 'voice') {
        //   // console.log(getStageRoute(nextScheduledStage.value));
        //   // return getStageRoute(nextScheduledStage.value);
        // } else {
        //   const firstStageTuple = getFirstOrRoutedStageIDByGroup('voice');
        //   console.log(getStageRoute(firstStageTuple), 'llllllllllllllll');
        //   // return getStageRoute(firstStageTuple);
        // }
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('context2');
        return getStageRoute(firstStageTuple);
      },
    },

    conclusion: {
      name: 'conclusion',
      label: 'Zwischenstand',
      description:
        'Hier stellen wir Ihnen erste, provisorische Erkenntnisse zusammen. Was ist bisher passiert im BürgerInnen-Rat?',
      tooltip:
        'Hier stellen wir Ihnen erste, provisorische Erkenntnisse zusammen. Was ist bisher passiert im BürgerInnen-Rat?',
      // disabled: this.groupsAccessible?.includes("conclusion"),
      icon: 'mdi-flag-checkered',
      expanded: () => false,
      to: () => {
        const firstStageTuple = getFirstOrRoutedStageIDByGroup('conclusion');
        return getStageRoute(firstStageTuple);
        // return {
        //   name: 'CONCLUSION',
        //   params: {
        //     assemblyIdentifier: assemblyIdentifier.value,
        //     stageID: getFirstOrRoutedStageIDByGroup('conclusion'),
        //   },
        // };
      },
    },
  };

  return {
    // ...toRefs(storedPolarBeePlot),
    assemblyMenuData,
  };
}
