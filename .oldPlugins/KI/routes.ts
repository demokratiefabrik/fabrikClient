// const meta4AssemblyPages = { topmenu: 'assemblies_ongoing_list' };
const meta4AssemblyAuthenticated = {
  assemblyAcl: ['delegate', 'observer', 'manager', 'expert'],
};
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    name: 'AssemblyHome_KI',
    path: '/:assemblyIdentifier/KI/',
    component: () => import('/src/plugins/KI/Layout.vue'),
    children: [
      {
        path: '',
        name: 'KI',
        component: () => import('pages/Index.vue'),
      },
      {
        path: 'start',
        name: 'start__KI',
        component: () => import('/src/pages/Assembly/TOC/TOC.vue'),
        meta: {
          hideAssemblyMenu: true,
          ...meta4AssemblyAuthenticated,
        },
      },
      {
        // TODO: do we need contenttreeID in path?
        path: 'textsheet/:stageID/:contenttreeID/',
        name: 'TEXTSHEET__KI',
        component: () => import('/src/plugins/KI/stages/TEXTSHEET/Index.vue'),
        meta: { ...meta4AssemblyAuthenticated },
      },

      {
        // TODO: do we need contenttreeid in path?
        path: '/:assemblyIdentifier/:assemblyType/textsheet/:stageID/:contenttreeID/',
        name: 'TEXTSHEET2__KI',
        component: () => import('/src/plugins/CIR/stages/TEXTSHEET/Index.vue'),
        meta: meta4AssemblyAuthenticated,
      },
      {
        path: '/:assemblyIdentifier/textsheet/:stageID/:contenttreeID/',
        name: 'TEXTSHEET_DEFAULT_LAYOUT',
        component: () => import('/src/plugins/CIR/stages/TEXTSHEET/Index.vue'),
        meta: meta4AssemblyAuthenticated,
      },
      {
        path: ':stageID/information',
        name: 'INFORMATION__KI',
        component: () => import('./stages/Information.vue'),
        meta: meta4AssemblyAuthenticated,
      },
      {
        path: ':stageID/context',
        name: 'EXPERT_STATEMENT__KI',
        component: () =>
          import('/src/plugins/KI/stages/EXPERT_STATEMENT/Index.vue'),
        meta: meta4AssemblyAuthenticated,
      },

      // Note: Define a route for each stageGroup/=> /stageID/<stage.group>
      {
        path: ':stageID/conclusion',
        name: 'CONCLUSION__KI',
        component: () => import('./stages/Conclusion.vue'),
        meta: meta4AssemblyAuthenticated,
      },
      // { path: ':stageID/arguments', name: 'KI_ARGUMENTS', component: () => import('./Arguments'), meta: meta4AssemblyAuthenticated },
    ],
  },
];

export default routes;
