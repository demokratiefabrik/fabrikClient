import { RouteRecordRaw } from 'vue-router';

const meta4AssemblyPages = { topmenu: 'assemblies_ongoing_list' };
// , acls: ['observe']  // NOT IMPLEMENTED/ NOT NECESSARY=> VUEX
const routes: RouteRecordRaw[] = [
  {
    // TODO: do we need contenttreeid in path?
    path: '/:assemblyIdentifier/:assemblyType/textsheet/:stageID/:contenttreeID/',
    name: 'TEXTSHEET',
    component: () => import('/src/plugins/KI/stages/TEXTSHEET/Index.vue'),
    meta: meta4AssemblyPages,
  },
  {
    path: '/:assemblyIdentifier/textsheet/:stageID/:contenttreeID/',
    name: 'TEXTSHEET_DEFAULT_LAYOUT',
    component: () => import('/src/plugins/KI/stages/TEXTSHEET/Index.vue'),
    meta: meta4AssemblyPages,
  },
];

export default routes;
