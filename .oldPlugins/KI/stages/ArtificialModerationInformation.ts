import { IAssemblyTuple } from 'src/models/assembly';
import { INodeTuple } from 'src/models/content';
import { IStageTuple } from 'src/models/stage';
import {
  IArtificialModeration,
  IArtificialModerationFile,
  IArtificialModerationGroup,
} from 'src/pages/components/artificial_moderation/model';
import { Ref } from 'vue';

export interface ICtx {
  routedStageTuple: IStageTuple | null;
  nextScheduledStage: Ref<IStageTuple>;
  isRoutedStageAlerted: boolean;
  assembly: IAssemblyTuple;
  isFirstText: boolean;
  gotoStage: (IStageTuple) => void;
  markUnAlert: () => void;
  scrollToTop: () => void;
  loaded: (any) => boolean;
  node: INodeTuple;
}

export default {
  index_top: {
    id: 'index_top',
    prosa: ' Leitet eine Text-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  index_bottom: {
    id: 'index_bottom',
    prosa: ' Schliesst die Text-Stage ab.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 10,
        prosa: 'Irgendwann, nachdem Rundgang zu Ende ist.',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && !ctx.nextScheduledStage,
        body: () => 'Hier geht es zurück zum Programm.',
        buttons: [
          {
            action: (ctx) => {
              ctx.scrollToTop();
            },
            label: () => 'Nach oben, bitte!',
          },
        ],
      },
      {
        id: 1,
        prosa: ' Erster Besuch am ersten Tag!.',
        condition: (ctx: ICtx) =>
          ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () => 'Sie haben den Text gelesen? Dann folgen Sie mir bitte.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },
      {
        id: 2,
        prosa: ' Zweiter Besuch',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && ctx.nextScheduledStage,
        body: () => 'Wollen wir fortfahren? Dann folgen Sie mir bitte.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },
    ] as IArtificialModeration[],
  },

  faq: {
    id: 'discussion_index_top',
    loading: (ctx: ICtx) => !ctx.loaded(ctx.node),
    items: [
      {
        id: 2,
        body: () =>
          'Sie können hier eine Frage stellen. Die anderen Teilnehmenden oder die Organisatoren werden sie sehr bald beantworten.',
      },
    ] as IArtificialModeration[],
  } as IArtificialModerationGroup,
} as IArtificialModerationFile;
