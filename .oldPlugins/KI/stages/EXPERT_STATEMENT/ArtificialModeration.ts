import { IAssemblyTuple } from 'src/models/assembly';
import { INodeTuple } from 'src/models/content';
import { IStageTuple } from 'src/models/stage';
import {
  IArtificialModeration,
  IArtificialModerationFile,
  IArtificialModerationGroup,
} from 'src/pages/components/artificial_moderation/model';
import { Ref } from 'vue';

export interface ICtx {
  $q: any;
  routedStageTuple: IStageTuple | null;
  nextScheduledStage: Ref<IStageTuple>;
  isRoutedStageAlerted: boolean;
  unreadExpertArguments: number | undefined;
  assembly: IAssemblyTuple;
  isFirstText: boolean;
  atFirstKITopic: boolean;
  gotoStage: (IStageTuple) => void;
  markUnAlert: () => void;
  markCompleted: () => void;
  loaded: (any) => boolean;
  node: INodeTuple;
}

export default {
  index_top: {
    id: 'index_top',
    prosa: ' Leitet eine Text-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [] as IArtificialModeration[],
  } as IArtificialModerationGroup,

  arguments_bottom: {
    id: 'index_bottom',
    prosa: ' Schliesst die Text-Stage ab.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 1,
        prosa: ' Erster Besuch am ersten Tag!.',
        condition: (ctx: ICtx) =>
          ctx.isRoutedStageAlerted &&
          ctx.nextScheduledStage &&
          ctx.unreadExpertArguments,
        body: (ctx) => [
          'Es folgen nun sechs Pro- und Kontraargumente zu diesem Einsatzgebiet der KI. Sie können diese Argumente lesen und darüber mit anderen Teilnehmenden des BürgerInnen-Rates diskutieren.',
          ctx.unreadExpertArguments === 1
            ? 'Da ist noch ein ungelesenes Argument vorhanden. Möchten Sie das auch noch lesen?'
            : ctx.unreadExpertArguments < 6
            ? `Es gibt insgesamt noch  ${ctx.unreadExpertArguments} ungelesene Argumente.`
            : '',
        ],
        // buttons: [
        //   {
        //     action: (ctx: ICtx) => {
        //       ctx.markUnAlert();
        //       ctx.gotoStage(ctx.nextScheduledStage);
        //     },
        //     label: () => 'Vielleicht später!',
        //   },
        // ],
      },
    ] as IArtificialModeration[],
  },

  index_bottom: {
    id: 'index_bottom',
    prosa: ' Schliesst die Text-Stage ab.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple?.stage.id,
    items: [
      {
        id: 10,
        prosa: 'Irgendwann, nachdem Rundgang zu Ende ist.',
        condition: (ctx: ICtx) =>
          !ctx.isRoutedStageAlerted && !ctx.nextScheduledStage,
        body: () => 'Möchten Sie zurück nach oben?',
        buttons: [
          {
            action: (ctx) => {
              ctx.scrollToTop();
            },
            label: () => 'Ja bitte!',
          },
        ],
      },
      {
        id: 1,
        prosa: ' Erster Besuch am ersten Tag!.',
        condition: (ctx: ICtx) =>
          ctx.isRoutedStageAlerted &&
          ctx.nextScheduledStage &&
          ctx.unreadExpertArguments,
        body: () =>
          'Haben Sie vorest genug zum Thema gelesen? Es gäbe noch ungelesene Argumente dazu.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Gerne fortfahren',
          },
        ],
      },
      //
      {
        id: 11,
        prosa: 'Alle Argumente gelesen.. (erstes Thema)',
        condition: (ctx: ICtx) =>
          ctx.atFirstKITopic &&
          ctx.nextScheduledStage &&
          !ctx.unreadExpertArguments,
        body: () =>
          'Schön. Sie haben alle Argumente zum Thema gelesen. Wenn Sie möchten, können wir fortfahren.',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },
      {
        id: 12,
        prosa: ' Alle Argumente gelesen: Zweites Thema.',
        condition: (ctx: ICtx) =>
          ctx.nextScheduledStage &&
          !ctx.atFirstKITopic &&
          !ctx.unreadExpertArguments,
        body: () =>
          'Schauen Sie sich die Zusammenfassung der bisherigen Diskussion im Bereich “Zwischenstand” an!',
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte!',
          },
        ],
      },
    ] as IArtificialModeration[],
  },
} as IArtificialModerationFile;
