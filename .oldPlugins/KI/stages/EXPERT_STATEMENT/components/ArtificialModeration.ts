// import { INodeTuple } from 'src/models/content';
import { INodeTuple } from 'src/models/content';
// import library from 'src/utils/library';
import { ITreeNodeTuple } from 'src/models/contenttree';
import {
  IArtificialModeration,
  IArtificialModerationFile,
} from 'src/pages/components/artificial_moderation/model';

// const { getRandomInt } = library;

export interface ICtx {
  tree: ITreeNodeTuple | null;
  customEvent: string | null;
  ownEntries: INodeTuple[] | undefined;
  getDailyContributionLimits: () => any;
  limitForAddingCommentsReached: boolean;
  // routedStageTuple: IStageTuple | null;
  // nextScheduledStage: Ref<IStageTuple>;
  // isRoutedStageAlerted: boolean;
  // unreadExpertArguments: number | undefined;
  // assembly: IAssemblyTuple;
  // isFirstText: boolean;
  // atFirstKITopic: boolean;
  // gotoStage: (IStageTuple) => void;
  // markUnAlert: () => void;
  // markCompleted: () => void;
  // loaded: (any) => boolean;
  // node: INodeTuple;
}

export default {
  argumentDiscussion: {
    // id: 10,
    id: (ctx: ICtx) => `argumentDiscussion${ctx?.tree?.content?.id}`,
    noReplace: true,
    prosa: ' Aufforderung was zu schreiben.',
    loading: (ctx: ICtx) => !ctx.tree,
    items: [
      {
        id: 3,
        condition: (ctx) =>
          ctx.tree?.nof_descendants % 2 == 1 &&
          !ctx.ownEntries?.length &&
          ctx.tree.nof_descendants >= 7,
        prosa: 'Schon alles gesagt?',
        body: () =>
          'Hier wurde schon einiges geschrieben. Möchten Sie die Diskussionen ergänzen?',
      },
      {
        id: 4,
        prosa: 'Mehr als drei ungelesene Beiträge',
        condition: (ctx) =>
          ctx.tree?.nof_descendants % 2 == 1 &&
          ctx.tree.nof_descendants_unread > 3,
        body: () =>
          'Da gibt es viele neue Beiträge in dieser Discussion. Wollen Sie sich mal umsehen?',
      },
      {
        id: 5,
        prosa: 'Ein ungelesener Beitrag',
        condition: (ctx) =>
          ctx.tree?.nof_descendants % 2 == 1 &&
          ctx.tree.nof_descendants_unread === 1,
        body: () =>
          'Hier gibt es ein ungelesener Beitrag. Möchten Sie den lesen?',
      },
      {
        id: 6,
        prosa: 'Es gibt zwei, drei ungelesene Beiträge',
        condition: (ctx) =>
          ctx.tree?.nof_descendants % 2 == 0 &&
          ctx.tree.nof_descendants_unread > 1 &&
          ctx.tree.nof_descendants_unread <= 3,
        body: () => 'Einige Beiträge sind neu hier. Sehen Sie sich nur um.',
      },
      {
        id: 10,
        // noReplace: true,
        prosa: 'Empty Discussion',
        condition: (ctx: ICtx) => ctx.tree?.nof_descendants === 0,
        body: () =>
          'Zu diesem Argument hat noch niemand etwas geschrieben. Möchten Sie der Erste sein?',
      },
      {
        id: 11,
        // noReplace: true,
        prosa: 'EXPERTQUESTION',
        condition: (ctx: ICtx) => {
          return (
            !!ctx.tree &&
            ctx.tree?.nof_descendants % 2 == 1 &&
            !ctx.limitForAddingCommentsReached
          );
        },
        body: () =>
          'Tipp: Wenn Sie eine sachliche Frage haben, können Sie über den Button jemanden aus unserem ExpertInnen-Kreis zur Hilfe rufen. Dieser befindet sich auf dieser Seite ganz unten.',
      },
      {
        id: 12,
        prosa: 'RESPECT (show only at every third entry..',
        // noReplace: true,
        condition: (ctx: ICtx) =>
          ctx.tree &&
          !ctx.ownEntries?.length &&
          ctx?.tree?.content?.id &&
          (ctx?.tree?.content?.id + ctx.tree?.nof_descendants) % 3 == 0,
        body: () =>
          'Tipp: In der Demokratiefabrik gehen wir respektvoll miteinander um und achten auf einen angemessenen Tonfall. Verzeihen Sie uns, wenn wir Verstöße nicht sofort entdecken und ahnden können.',
      },
      {
        id: 13,
        // noReplace: true,
        prosa:
          'FIRST UNREAD CHILDREN (only when number of unread children is even...',
        condition: (ctx: ICtx) =>
          ctx.tree &&
          ctx.tree?.nof_descendants_unrated &&
          ctx.tree?.nof_descendants % 2 == 1,
        body: (ctx) => {
          const unread = ctx.tree.children.find(
            (x: ITreeNodeTuple) => !x.progression?.read
          );
          return `Was halten Sie von dem Argument, das ${unread.creator.U} gepostet hat? Zeigen Sie es durch einen Smiley oder eine Antwort.`;
        },
      },
      {
        id: 14,
        prosa: 'FIRST UNRATED ARGUMENT',
        // noReplace: true,
        condition: (ctx: ICtx) =>
          ctx.tree &&
          ctx.tree?.nof_descendants_unread > 0 &&
          !ctx.ownEntries?.length,
        body: () =>
          'Überzeugen Sie die Kommentare? Dann können Sie sie mit einem Smiley bewerten.',
      },
      {
        id: 15,
        prosa: 'ADD COMMENT',
        condition: (ctx: ICtx) => {
          const limits = ctx.getDailyContributionLimits();
          const frequentPoster =
            ctx.limitForAddingCommentsReached ||
            limits.number_of_comments.current >= 5;
          return (
            ctx.tree &&
            ctx.tree?.nof_children > 0 &&
            !ctx.ownEntries?.length &&
            !frequentPoster
          );
        },
        body: (ctx) => {
          const first = ctx.tree.children[0];
          if (first) {
            return `Gibt es dem Argument von ${first.creator.U} noch was hinzuzufügen?`;
          }
          return 'Gibt es hier noch was hinzuzufügen?';
        },
      },
      {
        id: 16,
        prosa: 'GOTO ANOTHER ARGUMENT (only when number of entires are uneven',
        // noReplace: true,
        condition: (ctx: ICtx) => {
          const limits = ctx.getDailyContributionLimits();
          const frequentPoster =
            ctx.limitForAddingCommentsReached ||
            limits.number_of_comments.current >= 5;
          return (
            ctx.tree && frequentPoster && ctx.tree.nof_descendants % 2 === 1
          );
        },
        body: () =>
          'Toll, dass Sie so viel beitragen. Wollen Sie nun den anderen Teilnehmenden etwas Zeit zum Reagieren einräumen?',
      },
      {
        id: 17,
        // noReplace: true,
        prosa:
          'GOTO ANOTHER ARGUMENT (only when user already discussed this argument',
        condition: (ctx: ICtx) => {
          return ctx.tree && ctx.ownEntries && ctx.ownEntries.length > 2;
        },
        body: () =>
          'Schön, dass Sie hier aktiv mitdiskutieren. Es gibt freilich noch viel andere spannende Argumente. Schauen Sie doch auch dort nochmal vorbei!',
      },
    ] as IArtificialModeration[],
  },
} as IArtificialModerationFile;
