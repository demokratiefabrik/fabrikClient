/**
 * Requires contenttreeID (takes routedStageContenttreeID per default)
 */
import { computed, readonly, watch } from 'vue';
import { useStore } from 'vuex';
import { INodeTuple } from 'src/models/content';
import useStageComposable from 'src/composables/stage.composable';
import { IContentTreeTuple } from 'src/models/contenttree';
import usePKCEComposable from 'src/utils/VueOAuth2PKCE/pkce.composable';

export default function useFAQComposable(addWatcher = false) {
  const store = useStore();
  const { userid } = usePKCEComposable();
  const { assembly, routedStageTuple } = useStageComposable();
  const get_contenttree = store.getters['contenttreestore/get_contenttree'];

  const faqContenttreeIdentifier = computed((): string | null => {
    // console.log(
    //   'get FAQ CONTENTTREE_ID.lll',
    //   routedStageTuple.value?.stage.custom_data.FAQ_CONTENTTREE_ID
    // );
    if (routedStageTuple.value?.stage?.custom_data?.FAQ_CONTENTTREE_ID) {
      return `${routedStageTuple.value?.stage.custom_data.FAQ_CONTENTTREE_ID}`;
    }
    return null;
  });

  const faqContenttreeTuple = computed((): IContentTreeTuple | null => {
    if (!faqContenttreeIdentifier.value) {
      return null;
    }
    const contenttree = get_contenttree({
      contenttreeIdentifier: faqContenttreeIdentifier.value,
    });

    if (contenttree && !contenttree.id) {
      throw 'contenttree not correctly loaded...';
    }
    // console.log('DEBUG: new faqContenttreeTuple (111)', contenttree);

    return contenttree;
  });

  const faqContenttreeEntries = computed(
    (): Record<string, INodeTuple> | null => {
      if (!faqContenttreeTuple.value) {
        return null;
      }
      return faqContenttreeTuple.value?.entries;
    }
  );

  // const statementContentTuple = computed((): INodeTuple | null => {
  //   if (!faqContenttreeTuple.value) {
  //     // not yet ready
  //     return null;
  //   }

  //   if (!contenttreeEntries.value) {
  //     return null;
  //     // throw 'invalid contenttree entries';
  //   }
  //   const id = faqContenttreeTuple.value?.rootElementIds[0];

  //   // console.log('sssssssssssssssssssssssss', id, contenttreeEntries.value);
  //   return contenttreeEntries.value[`${id}`];
  // });

  if (addWatcher) {
    watch(
      () => faqContenttreeIdentifier.value,
      () => {
        if (faqContenttreeIdentifier.value) {
          store.dispatch('contenttreestore/syncContenttree', {
            assemblyIdentifier: assembly.value.identifier,
            contenttreeIdentifier: faqContenttreeIdentifier.value,
            oauthUserID: userid.value,
          });
        }
      },
      { immediate: true }
    );
  }

  // if (statementMarkRead) {
  //   watch(
  //     () => statementContentTuple.value,
  //     () => {
  //       if (statementContentTuple.value) {
  //         markRead(statementContentTuple.value);
  //       }
  //     },
  //     { immediate: true }
  //   );
  // }

  return {
    // statementContentId: readonly(statementContentId),
    // statementContentTuple: readonly(statementContentTuple),
    faqContenttreeEntries,
    faqContenttreeTuple: readonly(faqContenttreeTuple),
    faqContenttreeIdentifier: readonly(faqContenttreeIdentifier),
  };
}
