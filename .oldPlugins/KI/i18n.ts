// import { i18n } from 'src/boot/i18n';
export default {
  'de-ch': {},
  'de-de': {
    auth: {
      manager_name_derivation:
        'Sie gehören zu einem studentischen Team der Hochschule der Medien und der Universität Stuttgart, das mit der Demokratiefabrik zusammenarbeitet.',
      manager_name_derivation_3rd_party:
        'Dieser User gehört zu einem studentischen Team der Hochschule der Medien und der Universität Stuttgart, das mit der Demokratiefabrik zusammenarbeitet.',
      expert_name_derivation:
        'Sie wurden als Expertin respektie Experte der Fachrichtung «{var2}» zu dieser Demokratiefabrik-Veranstaltung eingeladen.',
      expert_name_derivation_3rd_party:
        'Der User wurde als Expertin respektive Experte der Fachrichtung «{var2}» zu dieser Demokratiefabrik-Veranstaltung eingeladen.',
    },
    common_properties: {
      insert: {
        criteria_accept: {
          '1': 'Der vorgeschlagene Beitrag ist - so oder ähnlich - noch nicht vorhanden.',
          '2': 'Der vorgeschlagene Beitrag macht inhaltlich Sinn.', // {topic}
          '3': 'Der vorgeschlagene Beitrag entspricht unseren Kriterien.',
        },
      },
      update: {
        criteria_accept: {
          '1': 'Die ursprüngliche Idee des Beitrags bleibt erhalten.',
          '2': 'Die Änderung stellt nach bestem Wissen und Gewissen eine Verbesserung des Beitrags dar.',
          '3': 'Der geänderte Beitrag entspricht noch immer unseren Kriterien.',
        },
      },

      contribution_text_hint: '',
      contribution_title_hint: '',
      contribution_title_label: 'Einen kurzen Titel',
      contribution_criteria_html: `<p class="text-caption">
            Sie finden nachfolgend die Erläuterungen, wie die Argumentarium-Beiträge zu
            formulieren sind.
          </p>
          <ol>
            <li>
              Formulieren Sie Fragen, die nach Möglichkeit einen
              <b>Bezug zur Gemeinde Köniz</b> aufweisen.
            </li>
            <li>
              Bitte formulieren Sie lediglich neutrale Fragen und verzichten Sie
              auf <b>Aussagen und/oder Statements</b>.
            </li>
            <li>
              Die Fragen müssen <b>mit ja/nein beantwortet</b> werden können.
            </li>
            <li>
              Mehrere, mit 'und' oder 'oder'
              <b>verbundene Fragen in einem Satz sind nicht erlaubt.</b>
            </li>
            <li>
              Die smartvote-Fragen sollen die <b>politischen Positionen</b> der
              Kandidierenden abfragen. Sie zielen nicht darauf ab, etwas über das
              persönliche Verhalten oder Eigenschaften der Kandidierenden zu
              erfahren.
            </li>
          </ol>`,
    },

    contenttree: {
      types: {
        COMMENT: 'Kommentar',
        INVITED_COMMENT: 'Kommentar*',
        QUESTION: 'Frage',
        ANSWER: 'Antwort',
        PARAGRAPH: 'Absatz',
        FOLDER: 'Ordner ',
        SECTION: 'Kapitel',
        SUBSECTION: 'Unterkapitel',
        CIR_TOPIC: 'Abstimmungsvorlage',
        PRO: 'Vorteil',
        CONTRA: 'Nachteil',
      },
      discussion_block_intro: {
        FAQ: {
          QUESTION:
            'Hier werden die Antworten des Expertenteams aufgelistet. Sie haben danach die Möglichkeit über diese Antworten zu diskutieren.',
          ANSWER:
            'Sie können hier über diese Antwort eines Experten diskutieren. Bitte bleiben Sie dabei jederzeit freundlich.',
        },
      },
      editor: {
        contribution_limit_reached:
          'Sie haben heute schon {current} Posts veröffentlicht. Sie können nun noch Posts mit Smileys kommentieren. Aber morgen ist ein neuer Tag - loggen Sie sich gerne wieder ein und diskutieren Sie weiter!',
      },
    },
  },
};
