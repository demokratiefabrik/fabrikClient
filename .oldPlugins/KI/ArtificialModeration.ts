// import { IStageGroup } from 'src/composables/stage.composable';
import { IStageGroup, IStageTuple } from 'src/models/stage';
// import { Ref} from 'vue';

export interface IAmToc {
  groupsScheduled: IStageGroup[];
  assemblyMenuItems: IStageGroup[];
  stages_by_groups: Record<string, IStageTuple[]>;
  nextScheduledStage: IStageTuple;
  nextScheduledStageGroup: IStageGroup;
  gotoStage: (IStageTuple) => void;
  // [x: string]: any,
}

const AMs = {
  toc: {
    id: 'toc',
    prosa: ' Wird bei der Tagesübersicht angzeigt.',
    loading: (ctx: IAmToc) =>
      !ctx.assemblyMenuItems?.length ||
      !ctx.stages_by_groups ||
      ctx.groupsScheduled === undefined,
    items: [
      {
        id: 1,
        prosa: ' ... gibt nichts mehr zu tun',
        condition: (ctx: IAmToc) => !ctx.groupsScheduled.length,
        body: () =>
          'Sie haben heute bereits alle wichtigsten Traktanden erledigt. Erst morgen früh halten wir hier wieder neue Aufgaben für Sie bereit.',
      },
      {
        id: 3,
        prosa:
          ' ... VORBEREITUNG: die bitte dort weiterzufarhen wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStage.stage.group == 'preparation',
        body: (ctx) => {
          const chapter = ctx.nextScheduledStage.stage.title;
          return `Wir möchten, dass Sie sich nun das Kapitel «${chapter}» ansehen. `;
        },
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ich komme mit!',
          },
        ],
      },
      {
        id: 21,
        prosa:
          ' ... TOPICS: Day X: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStageGroup?.name == 'context1' &&
          ctx.nextScheduledStage.progression &&
          ctx.nextScheduledStage.progression.number_of_day_sessions > 1,
        body: () => {
          // const chapter = ctx.nextScheduledStageGroup.toc_label ? ctx.nextScheduledStageGroup.toc_label : ctx.nextScheduledStageGroup.label
          return 'Kommen Sie doch nochmals mit zur Übersicht über die Teilnehmerschaft.'; // MEINUNGSRAUM
        },
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ja, gern.',
          },
        ],
      },
      {
        id: 212,
        prosa:
          ' ... TOPICS: Day 1: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStageGroup?.name == 'context1' &&
          (!ctx.nextScheduledStage.progression ||
            ctx.nextScheduledStage.progression.number_of_day_sessions == 1),
        body: () => {
          // const chapter = ctx.nextScheduledStageGroup.toc_label ? ctx.nextScheduledStageGroup.toc_label : ctx.nextScheduledStageGroup.label
          return 'Sie haben nun alle Vorbereitungen beendet und es kann richtig los gehen.';
        },
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Fortfahren',
          },
        ],
      },
      {
        id: 22,
        prosa:
          ' ... ARGUMENTARIUM: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStage &&
          ctx.nextScheduledStageGroup?.name == 'context2',
        body: () => 'Es geht weiter mit dem zweiten Fallbeispiel.',
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Ja, ich komme mit!',
          },
        ],
      },
      {
        id: 2,
        prosa:
          ' ... CONCLUSION: die bitte dort weiterzufahren wo, es was zu tun gibt.',
        condition: (ctx: IAmToc) =>
          ctx.nextScheduledStageGroup?.name == 'conclusion',
        body: () =>
          'Sehr schön! Sie haben unsere Fragen alle beantwortet. Vielen Dank. Sie können sich nun noch den Zwischenstand ansehen.',
        buttons: [
          {
            action: (ctx: IAmToc) => ctx.gotoStage(ctx.nextScheduledStage),
            label: () => 'Zum Zwischenstand',
          },
        ],
      },
    ],
  },

  indexTopPeerReview: {
    id: 'indexTopBasic',
    loading: (ctx) => !ctx.node.children,
    items: [
      {
        id: 1,
        condition: (ctx) => ctx.node.nof_descendants === 0,
        priority: 1,
        prosa: 'Nichts bisher',
        body: () =>
          'Die anderen würden sich freuen, wenn Sie hier Ihre Überlegungen teilen.',
      },
      {
        id: 1,
        condition: (ctx) =>
          ctx.node.nof_descendants > 0 && ctx.node.nof_descendants < 5,
        priority: 1,
        prosa: 'Nichts bisher',
        body: () => 'Was sind Ihre Überlegungen zu diesem Vorschlag?',
      },
      {
        id: 3,
        priority: 1,
        condition: (ctx) => ctx.node.nof_descendants >= 5,
        prosa: 'Schon alles gesagt?',
        body: () => 'Was denken Sie zu den Überlegungen der anderen?',
      },
    ],
  },

  // msChallengeByOthers: {
  //   id: 'msChallengeByOthers',
  //   items: [
  //     {
  //       id: 1,
  //       prosa: 'no feedbacks, User must click the button to progress.',
  //       body: () => [
  //         'Nun denn, wir können uns nun anschauen, wie sich die anderen Teilnehmenden positioniert haben.',
  //       ],
  //       buttons: [
  //         {
  //           action: (ctx) => ctx.msChallengeByOthers.markAchieved(),
  //           label: () => 'Gerne fortfahren...',
  //         },
  //       ],
  //     },
  //   ],
  // },

  // msChallengeOfOthers: {
  //   id: 'msChallengeOfOthers',
  //   items: [
  //     {
  //       id: 1,
  //       prosa: 'give feedback to other users.',
  //       body: () => [
  //         'Sind mit diesen Positionen einverstanden. Welchen Aspekt der Vorlage sollen sich die Teilnehmenden nochmals durch den Kopf gehen lassen.',
  //       ],
  //       // buttons: [
  //       //   {
  //       //     action: (ctx) => ctx.msChallengeOfOthers.markAchieved(),
  //       //     label: () => 'Gerne fortfahren...',
  //       //   },
  //       // ],
  //     },
  //   ],
  // },
  conclusionA: {
    id: 'conclusionA',
    items: [
      {
        id: 1,
        body: () => [
          'Wir danken Ihnen im Namen des ganzen Demokratiefabrik-Teams für Ihre wertvolle Mitarbeit!',
          // `${actorPartnerReference} und ich machen nun Pause. Wir stehen Ihnen ab morgen wieder zur Verfügung!`,
        ],
      },
    ],
  },

  conclusionB: {
    id: 'conclusionB',
    items: [
      {
        id: 2,
        body: () => [
          'Es freut uns, wenn Sie uns in den nächsten Tagen nochmals besuchen kommen. Verwenden Sie dazu den Link in der E-Mail-Einladung von mingle',
          // 'Unten folgt noch eine kleine Übersicht über den aktuellen Stand des Argumentariums.',
        ],
      },
    ],
  },
};

export default AMs;
