import { RouteRecordRaw } from 'vue-router';
const routes: RouteRecordRaw[] = [
  {
    path: '/:assemblyIdentifier/:assemblyType/survey/:stageID',
    name: 'SURVEY',
    component: () => import('/src/plugins/stages/SURVEY/Index.vue'),
    meta: { assemblyAcl: 'delegate' },
  },
  {
    path: '/:assemblyIdentifier/survey/:stageID',
    name: 'SURVEY_DEFAULT_LAYOUT',
    component: () => import('/src/plugins/stages/SURVEY/Index.vue'),
    meta: { assemblyAcl: 'delegate' },
  },
];

export default routes;
