import { IStageTuple } from 'src/models/stage';
import { Ref } from 'vue';

interface ICtx {
  routedStageTuple: IStageTuple | null;
  redirecting: boolean;
  nextScheduledStage: Ref<IStageTuple>;
  isRoutedStageCompleted: boolean;
  markUnAlert: () => void;
  markCompleted: () => void;
  gotoStage: (IStageTuple) => void;
  $t: (string) => void;
}

export default {
  survey: {
    id: 'survey',
    prosa: ' Leitet eine Text-Stage ein.',
    loading: (ctx: ICtx) => !ctx.routedStageTuple || ctx.redirecting,
    items: [
      {
        id: 1,
        prosa: ' Redirect zum Survey....',
        condition: (ctx: ICtx) => !ctx.isRoutedStageCompleted,
        body: (ctx: ICtx) => ctx.$t('survey.redirect_to_survey'),
      },
      {
        id: 2,
        prosa: ' Bereits Survey beendet',
        condition: (ctx: ICtx) => ctx.isRoutedStageCompleted,
        body: (ctx: ICtx) => ctx.$t('survey.survey_completed'),
        buttons: [
          {
            action: (ctx: ICtx) => {
              ctx.markUnAlert();
              // console.log(ctx.nextScheduledStage, 'ddddddddddd ROUTE');
              ctx.gotoStage(ctx.nextScheduledStage);
            },
            label: () => 'Ja, bitte weiterfahren',
          },
        ],
      },
    ],
  },
};
