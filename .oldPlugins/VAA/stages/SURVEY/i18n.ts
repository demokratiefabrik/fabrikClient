const localI18n = {
  'de-ch': {
    survey: {
      misconfiguration_error: 'Es ist ein Fehler aufgeteten für den wir um Entschuldiung bitten. Momentan ist es nicht möglich die Befragung fortzusetzen.',
      redirect_to_survey: 'Einen kleinen Moment...',
      survey_completed: 'Grossartig! Sie haben diesen Fragebogen fertig ausgefüllt. Wir können nun weiterfahren.'
    }
  }
}

export default localI18n