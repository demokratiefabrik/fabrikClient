/** DEMOKRATIFABRIK RUNTIME VARIABLES */

import useAssemblyComposable from 'src/composables/assembly.composable';
import constants from 'src/utils/constants';
import useStageComposable from 'src/composables/stage.composable';
import { IStageGroup } from 'src/models/stage';
// import { stage } from 'src/store/assemblystore/getters';

export default function useCIRComposable() {
  const { assemblyIdentifier } = useAssemblyComposable('');
  const { nextScheduledStage, getFirstOrRoutedStageIDByGroup } =
    useStageComposable();

  const assemblyMenuData: Record<string, IStageGroup> = {
    preparation: {
      name: 'preparation',
      label: 'Programm',
      toc_label: 'Vorbereitung',
      description:
        'Sie erhalten hier die wichtigsten Informationen zur Könizer Demokratiefabrik. Umgekehrt benötigen wir auch einige Angaben von Ihnen.',
      icon: 'mdi-information-outline',
      tooltip: 'Sie finden hier das Demokratiefabrik-Programm.',
      expanded: (item) =>
        nextScheduledStage.value?.stage.group == 'preparation' ||
        item.manual_expanded,
      expandable: true,
      manual_expanded: false,
      to: () => {
        return {
          name: 'VAA',
          params: { assemblyIdentifier: assemblyIdentifier.value },
        };
      },
    },

    topics: {
      name: 'topics',
      label: 'Themengebiete',
      icon: constants.ICONS.VAA_TOPIC,
      description:
        'Welche Themen sind Ihnen wichtig? Wie viel Gewicht soll den einzelnen politischen Themen im smartvote-Fragebogen beigemessen werden?',
      tooltip:
        'Welche Themen sind Ihnen wichtig? Wie viel Gewicht soll den einzelnen politischen Themen im smartvote-Fragebogen beigemessen werden?',
      // disabled: this.groupsAccessible?.includes("topics"),
      // tooltip: "Setzen Sie die Themen des Wahlkampfs.",
      expanded: () => false,
      to: () => {
        const stageTuple = getFirstOrRoutedStageIDByGroup('topics');
        return {
          name: 'VAA_TOPICS',
          params: {
            assemblyIdentifier: assemblyIdentifier.value,
            stageID: stageTuple.stage.id,
          },
        };
      },
    },

    questions: {
      name: 'questions',
      label: 'Fragenkatalog',
      description:
        'Nun wird es konkret: Was möchten Sie von den Kandidatinnen und Kandidaten der Könizer Gemeindewahlen wissen, bevor Sie sie wählen?',
      icon: constants.ICONS.VAA_QUESTION,
      expanded: () => false,
      tooltip:
        'Bei welchen Sachfragen müssen die Kandidatinnen und Kandidaten mit Ihnen übereinstimmen, damit Sie sie wählen würden.',
      // tooltip:
      //   "Entscheiden, Sie über welche konkreten Fragen im Wahlkampf diskutiert werden soll.",
      to: () => {
        const stageTuple = getFirstOrRoutedStageIDByGroup('questions');
        return {
          name: 'VAA_QUESTIONS',
          params: {
            assemblyIdentifier: assemblyIdentifier.value,
            stageID: stageTuple.stage.id,
          },
        };
      },
    },

    conclusion: {
      name: 'conclusion',
      label: 'Zwischenstand',
      description:
        'Hier stellen wir Ihnen erste, provisorische Resultate zusammen. Welche Fragen liegen vorn? Welche Themen sind den bisherigen Mitgliedern der Demokratiefabrik besonders wichtig?',
      tooltip:
        'Hier stellen wir Ihnen erste, provisorische Resultate zusammen. Welche Fragen liegen vorn? Welche Themen sind den bisherigen Mitgliedern der Demokratiefabrik besonders wichtig?',
      // disabled: this.groupsAccessible?.includes("conclusion"),
      icon: 'mdi-flag-checkered',
      expanded: () => false,
      // tooltip:
      //   "Sie finden eine Übersicht über den aktuellen Stand der Online-Versammlung",
      to: () => {
        const stageTuple = getFirstOrRoutedStageIDByGroup('conclusion');
        return {
          name: 'VAA_CONCLUSION',
          params: {
            assemblyIdentifier: assemblyIdentifier.value,
            stageID: stageTuple.stage.id,
          },
        };
      },
    },
  };

  return {
    // ...toRefs(storedPolarBeePlot),
    assemblyMenuData,
  };
}
