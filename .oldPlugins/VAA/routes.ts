const meta4AssemblyDelegates = { assemblyAcl: ['delegate', 'observer'] };
import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    name: 'AssemblyHome_VAA',
    path: '/:assemblyIdentifier/vaa/',
    component: () => import('/src/plugins/assemblies/VAA/Layout.vue'),
    children: [
      {
        path: 'textsheet/:stageID/:contenttreeID/',
        name: 'TEXTSHEET__VAA',
        component: () => import('/src/plugins/stages/TEXTSHEET/Index.vue'),
        meta: { ...meta4AssemblyDelegates },
      },
      {
        path: 'survey/:stageID',
        name: 'SURVEY__VAA',
        component: () => import('/src/plugins/stages/SURVEY/Index.vue'),
        meta: { ...meta4AssemblyDelegates },
      },
      {
        path: ':stageID/topics',
        name: 'TOPICS__VAA',
        component: () => import('./TOPICS/Topics.vue'),
        meta: meta4AssemblyDelegates,
      },
      {
        path: ':stageID/questions',
        name: 'QUESTIONS__VAA',
        component: () => import('./QUESTIONS/Questions.vue'),
        meta: meta4AssemblyDelegates,
      },
      {
        path: ':stageID/questions/:contentID',
        name: 'QUESTIONS_ENTRY__VAA',
        component: () => import('./questions.vue'),
        meta: meta4AssemblyDelegates,
      },
      {
        path: '',
        name: 'VAA',
        component: () => import('/src/pages/Assembly/TOC/TOC.vue'),
        meta: { hideAssemblyMenu: true, ...meta4AssemblyDelegates },
      },
    ],
  },
];

export default routes;
