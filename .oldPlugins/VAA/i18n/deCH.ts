// import { i18n } from 'src/boot/i18n';
export default {
  common_properties: {
    insert: {
      criteria_accept: {
        '1': 'Der vorgeschlagene Beitrag ist - so oder ähnlich - noch nicht vorhanden.',
        '2': 'Der vorgeschlagene Beitrag macht inhaltlich Sinn.', // {topic}
        '3': 'Der vorgeschlagene Beitrag entspricht unseren Kriterien.',
      },
    },
    update: {
      criteria_accept: {
        '1': 'Die ursprüngliche Idee des Beitrags bleibt erhalten.',
        '2': 'Die Änderung stellt nach bestem Wissen und Gewissen eine Verbesserung des Beitrags dar.',
        '3': 'Der geänderte Beitrag entspricht noch immer unseren Kriterien.',
      },
    },
    contribution_title_label: 'Die Frage an die Kandidierenden *',
    contribution_title_hint:
      "'Häufige Formulierungen lauten etwa: Soll das Gesetz XY geändert werden?' oder 'Unterstützen Sie die Einführung von XY?'",
    contribution_criteria_html: `  <p class="text-caption">
          Sie finden nachfolgend die Erläuterungen, wie die smartvote-Fragen zu
          formulieren sind.
        </p>
        <ol>
          <li>
            Formulieren Sie Fragen, die nach Möglichkeit einen
            <b>Bezug zur Gemeinde Köniz</b> aufweisen.
          </li>
          <li>
            Bitte formulieren Sie lediglich neutrale Fragen und verzichten Sie
            auf <b>Aussagen und/oder Statements</b>.
          </li>
          <li>
            Die Fragen müssen <b>mit ja/nein beantwortet</b> werden können.
          </li>
          <li>
            Mehrere, mit 'und' oder 'oder'
            <b>verbundene Fragen in einem Satz sind nicht erlaubt.</b>
          </li>
          <li>
            Die smartvote-Fragen sollen die <b>politischen Positionen</b> der
            Kandidierenden abfragen. Sie zielen nicht darauf ab, etwas über das
            persönliche Verhalten oder Eigenschaften der Kandidierenden zu
            erfahren.
          </li>
        </ol>`,
  },

  disclaimer: {
    contenttree: {
      basic:
        'Um die Diskussion übersichtlich zu halten, behalten wir uns vor, die Inhalte innerhalb des Forums neu zu strukturieren und auch ohne Angaben von Gründen (insbesondere bei Verstössen gegen unseren Verhaltenskodex) zu löschen. Die Inhalte in diesem Forum werden nicht in chronologischer Reihenfolge aufgelistet. Die Reihenfolge der Inhalte auf gleicher Hierarchiestufe ist zufällig und variiert von Benutzer zu Benutzer.',
      extensionExtraLarge:
        'Die Diskussion ist schon recht umfassend. Welche übersichtlich bleibt, wurden nur wenige zufällig ausgewählte Beiträge vollständig aufgeklappt. Sie können die restlichen Beiträge selbst per Mausklick öffnen.',
      peerreview:
        'Alle Teilnehmende können auf dieser Seite neue Vorschläge einbringen, wie der smartvote-Fragebogen besser gemacht werden kann. Sie können also vorschlagen, smartvote- Fragen zu erstellen, zu bearbeiten oder auch zu löschen. Zufällig ausgewählte Teilnehmende werden hier dann angefragt zu diesen Vorschlägen Stellung zu nehmen. Nach 24 Stunden wird aufgelöst: Bei einer Mehrheit an positiven Antworten, werden die Vorschläge automatisch umgesetzt. ',
    },
  },

  contenttree: {
    types: {
      COMMENT: 'Kommentar',
      INVITED_COMMENT: 'Kommentar*',
      QUESTION: 'Frage',
      ANSWER: 'Antwort',
      PARAGRAPH: 'Absatz',
      FOLDER: 'Ordner ',
      SECTION: 'Kapitel',
      SUBSECTION: 'Unterkapitel',
      VAA_QUESTION: 'smartvote-Frage',
      VAA_TOPIC: 'smartvote-Thema',
    },
  },

  notifications: {
    PEERREVIEW_ASSIGNED:
      'Sie dürfen sich heute an {value} Gutachten zur Änderung/Ergänzung des Fragenkatalogs beteiligen.',
    PEERREVIEW_INIT_INSERT:
      'Der von Ihnen erstellte Fragenvorschlag «{value}» wird nun von anderen Teilnehmenden begutachtet. Wir halten Sie hier auf dem Laufenden.',
    PEERREVIEW_INIT_UPDATE:
      'Ihr Änderungsvorschlag zur Frage «{value}» wird nun von anderen Teilnehmenden begutachtet. Wir halten Sie hier auf dem Laufenden.',
    PEERREVIEW_REJECTED:
      'Der von Ihnen eingereichte Antrag (Frage: «{value}») wurde abgelehnt.',
    PEERREVIEW_APPROVED:
      'Der von Ihnen erstellte Antrag (Frage: «{value}») wurde angenommen.',
  },
};
