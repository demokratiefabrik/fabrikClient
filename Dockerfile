# 1) develop stage
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
COPY quasar.conf.js ./
RUN yarn --silent
RUN yarn --silent global add @quasar/cli
COPY . .
COPY .env.production .env
# RUN quasar build
# Uii, breathtaking hack: build twice, since first run installs all missing dependency (TODO: fix this.)
# RUN quasar build
RUN quasar build -d

# 2) production
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist/spa /usr/share/nginx/html

# Replace NGINX CONF
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

EXPOSE 80
COPY start-server-nginx.sh /
RUN chmod +x ./start-server-nginx.sh
CMD ["./start-server-nginx.sh"]
